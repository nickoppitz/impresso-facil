<?php 
$namePage = "produtos-categoria";
include("header.php"); 
$infoMarca = fillMarcas($parametros[1]);
?>

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black"><?= $infoMarca['MARMARCA'] ?></h1>
			
			<ul class="breadcrumb">
				<li><a href="/marcas/">Marcas</a></li>
				<li><?= $infoMarca['MARMARCA'] ?></li>
			</ul><!-- breadcrumb -->

			<ul class="category-catalog">
				<?php 
					$categorias = categoriasByMarca($parametros[1]);
					if($categorias['quantidadeRegistros'] > 0){
						foreach($categorias['dados'] as $categoria){
							$imagem = file_exists('admin/uploads/categorias/thumbnail/'.$categoria['CATIMAGEM']) ? 'admin/uploads/categorias/thumbnail/'.$categoria['CATIMAGEM'] : 'admin/uploads/categorias/'.$categoria['CATIMAGEM'];

							$imagem = file_exists($imagem) ? "<img src=\"{$imagem}\" alt=\"{$categoria[CATCATEGORIA]}\" title=\"{$categoria[CATCATEGORIA]}\">" : null;
							echo "<li>\n
									\t<article class=\"category-itm\">\n
										\t\t<a href=\"/".$parametros[0].'/'.$parametros[1].'/'.$parametros[2].'/'.$categoria['CATCODIGO'].'/'.gerarUrlAmigavel($categoria['CATCATEGORIA'])."/\">\n
											\t\t\t<figure class=\"category-pic\">\n
												\t\t\t\t{$imagem}\n
											\t\t\t</figure>\n
										\t\t</a>\n

										\t\t<h2 class=\"category-ttl\"><a href=\"/".$parametros[0].'/'.$parametros[1].'/'.$parametros[2].'/'.$categoria['CATCODIGO'].'/'.gerarUrlAmigavel($categoria['CATCATEGORIA'])."/\">{$categoria[CATCATEGORIA]}</a></h2>\n
									\t</article>\n
								</li>\n";
						}
					}else{
						echo "\t\t<li>Não há categorias para esta marca!</li>\n";
					}
				?>
			</ul><!-- category catalog -->
		</div><!-- ctn -->
	</main>
<?php 
 if(!isset($_SESSION['CLICODIGO']) || !is_numeric($_SESSION['CLICODIGO'])){
		include("modal-login-lg.php");
 }
 ?>	
<?php include("footer.php"); ?>