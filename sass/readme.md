Base
	- Onde todas as bibliotecas(libs) são invocadas;
	- Onde é feito o setup padrão, como tipografia, tamanho do texto, cores padrões, correções de outras libs (como o font-weight da tag <b> e display none da modal gerada pelo addthis).

Components
	- Onde os componentes são divididos em partials, categorizados pelo template padrão de sites institucionais;
	- alerts;
	- buttons;
	- catalogs;
	- contents;
	- forms;
	- grid especial para forms;
	- institutionals;
	- paginations;
	- sliders;
	- titles.

Layout
	- Onde ficam divididas em partials partes essenciais do layout;
	- main header;
	- main nav;
	- main footer;
	- sidebars;

Pages
	- Onde ficam os partials com os nomes correspondentes às páginas do site/aplicação. Geralmente utilizados para configurar o alinhamento, margens e paggings de componentes dentro de cada página, mas o ideal é que seja pouco usado.

Themes
	- Quando o layout possui mais de um tipo de cor por exemplo, podemos criar sub templates dentro dele.

Utils
	- Onde ficam mixins globais e seletores de ajuda.

Vendors
	- Onde ficam as terceiras partes da aplicação como, bibliotecas e frameworks.