module.exports = function (gulp, plugins) {
    return function () {

        // ****************************************************************************
        // 1. Email Dev Default - Watch Style, HTML includes e deploy dos emails de dev
        // ****************************************************************************
        gulp.task('email-default',function() {
            gulp.watch('email/src/sass/**/*.scss',['styles-email']);
            gulp.watch('email/src/html/*.php',['email-fileinclude']);
            gulp.watch([
                // PHP files
                'email/src/temp/*.php',

                // CSS files
                'email/src/temp/css/main.css',

                // IMG files
                'email/img/*.jpg','email/img/*.png', 'email/img/*.gif'

            ],['email-dev-deploy']);
        });


        // ******************************************************
        // 2. Email file include antecessor do processo de inline
        // ******************************************************
        var fileinclude = require('gulp-file-include');
         
        gulp.task('email-fileinclude', function() {
          gulp.src([
                    'email/src/html/*.php',

                    // Não pegar
                    '!./email/src/html/header.php',
                    '!./email/src/html/footer.php'
                ])
            .pipe(fileinclude({
              prefix: '@@',
              basepath: '@file'
            }))
            .pipe(gulp.dest('email/src/temp/'));
        });


        // *******************
        // 3. Email inline CSS
        // *******************
        inlineCss = require('gulp-inline-css');

        gulp.task('email-inline', function() {
            return gulp.src('email/src/temp/*.php')
                .pipe(inlineCss())
                .pipe(gulp.dest('email/'));
        });


        // *******************
        // E-mail deploy final
        // *******************

        var runSequence = require('run-sequence');

        gulp.task('email-deploy-default', function(done) {
            runSequence('email-inline', 'email-final-deploy', function() {
                console.log('Arquivos finais de e-mails "inlineados" e "deployados" :)');
                done();
            });
        });


        // *******************
        // Vinyl FTP (Deploys)
        // *******************
        var gutil = require( 'gulp-util' );
        var ftp = require( 'vinyl-ftp' );

        // Email Development Deploy
        gulp.task( 'email-dev-deploy', function () {

            var conn = ftp.create( {
                host:     '',
                user:     '',
                password: '',
                parallel: 10,
                log:      gutil.log
            } );

            var globs = [
                // Email Assets
                'email/src/html/*.php',
                'email/src/temp/*.php',
                'email/src/temp/css/main.css',
                'email/img/*.jpg',
                'email/img/*.png',
                'email/img/*.gif'
            ];

            // using base = '.' will transfer everything to /public_html correctly
            // turn off buffering in gulp.src for best performance

            return gulp.src( globs, { base: '.', buffer: false } )
                .pipe( conn.newer( '/' ) ) // only upload newer files
                .pipe( conn.dest( '/' ) );

        } );

        // E-mail Final Deploy
        gulp.task( 'email-final-deploy', function () {

            var conn = ftp.create( {
                host:     '',
                user:     '',
                password: '',

                parallel: 10,
                log:      gutil.log
            } );

            var globs = [
                // Email Final Inline
                'email/*.php'
            ];

            // using base = '.' will transfer everything to /public_html correctly
            // turn off buffering in gulp.src for best performance

            return gulp.src( globs, { base: '.', buffer: false } )
                .pipe( conn.newer( '/' ) ) // only upload newer files
                .pipe( conn.dest( '/' ) );

        } );

        // *****************
        // Sass (task email)
        // *****************
        var sass = require('gulp-sass');
        
        gulp.task('styles-email', function() {
            gulp.src('email/src/sass/**/*.scss')
                .pipe(sass().on('error', sass.logError))
                .pipe(sass({ outputStyle: 'compressed' }))
                .pipe(gulp.dest('./email/src/temp/css'))

        });

    };
};