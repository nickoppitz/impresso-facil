module.exports = function (gulp, plugins) {
    return function () {

        // ***************************************************
        // UI Dev Default - Watch Style e deploy da UI do site
        // ***************************************************
        gulp.task('default',function() {
            gulp.watch('sass/**/*.scss',['styles']);
            gulp.watch([
                // CSS files
                'css/main.css',

                // IMG files
                'img/*.jpg','img/*.png', 'img/*.gif'

            ],['ui-deploy']);
        });


        // *******************
        // Vinyl FTP (Deploys)
        // *******************
        var gutil = require( 'gulp-util' );
        var ftp = require( 'vinyl-ftp' );

        // UI Deploy
        gulp.task( 'ui-deploy', function () {

            var conn = ftp.create( {
                host:     '',
                user:     '',
                password: '',
                parallel: 10,
                log:      gutil.log
            } );

            var globs = [
                // UI assets
                'css/main.css',
                'img/*.jpg',
                'img/*.png',
                'img/*.gif'
            ];

            // using base = '.' will transfer everything to /public_html correctly
            // turn off buffering in gulp.src for best performance

            return gulp.src( globs, { base: '.', buffer: false } )
                .pipe( conn.newer( '/' ) ) // only upload newer files
                .pipe( conn.dest( '/' ) );

        } );


        // ********************
        // Sass (task default)
        // ********************
        var sass = require('gulp-sass');

        gulp.task('styles', function() {
            gulp.src('sass/**/*.scss')
                .pipe(sass().on('error', sass.logError))
                .pipe(sass({ outputStyle: 'compressed' }))
                .pipe(gulp.dest('./css'))

        });


        // ******************
        // Clean/Minify CSS
        // ******************
        var cleanCSS = require('gulp-clean-css');

        gulp.task('cssmin', function() {
          return gulp.src('css/*.css')
            .pipe(cleanCSS({compatibility: 'ie8'}))
            .pipe(gulp.dest('dist/css'));
        });


        // *******************
        // Mininfy HTML e PHP
        // *******************
        var htmlmin = require('gulp-htmlmin');
         
        gulp.task('min', function() {
          return gulp.src(['**/*.html', '**/*.php', '!./libs/**', '!./php/**', '!./googlede54d636ea67bfa9.html', '!./dist/**', '!./arquivos/**', '!./node_modules/**','!./exemplo/**', '!./email/**'])
            .pipe(htmlmin({
            	collapseWhitespace: true,
            	preserveLineBreaks : true,
            	removeComments : true
            }))
            .pipe(gulp.dest('dist'))
        });

        // Se rolar tretas na minificação de html/php tente colocar antes e depois da tag que está com o problema isso:
        // <!-- htmlmin:ignore -->


        // **********
        // Minify js
        // **********
        var uglify = require('gulp-uglify');
         
        gulp.task('jsmin', function() {
          return gulp.src('js/*.js')
            .pipe(uglify())
            .pipe(gulp.dest('dist/js'));
        });


        // **************
        // Minify images
        // **************
        const imagemin = require('gulp-imagemin');
         
        gulp.task('imgmin', () =>
        	gulp.src(['**/*.{jpg,png,gif,ico}', '!./libs/**', '!./php/**', '!./dist/**', '!./arquivos/**', '!./node_modules/**'])
        		.pipe(imagemin())
        		.pipe(gulp.dest('dist/'))
        );

    };
};