<?php 
$namePage = "entrar";
include("header.php"); ?>

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Entrar</h1>
			
			<div class="getin">
				<div class="login">
					<h2 class="ttl-sm-black-2">Já tenho cadastro</h2>

					<form class="form-1" name="form-login">
						<fieldset class="full">
							<input type="email" id="emailLogin" name="emailLogin" class="error">
							<label class="form-1-placeholder" for="emailLogin">Seu e-mail</label>
						</fieldset>
						<fieldset class="full">
							<input type="password" id="pwd" name="pwd">
							<label class="form-1-placeholder" for="pwd">Sua senha</label>
						</fieldset>

						<footer class="form-1-footer">
							<a href="minha-senha.php" class="pwd-settings">Esqueci minha senha</a>

							<button class="form-1-btn-submit">CONTINUAR</button>
						</footer><!-- form 1 footer -->
					</form><!-- form-1 -->
				</div><!-- login -->

				<div class="sign-up">
					<h2 class="ttl-sm-black-2">Não possuo cadastro</h2>

					<form class="form-1" name="form-sign-up">
						<fieldset class="full">
							<input type="email" id="emailSignUp" name="emailSignUp">
							<label class="form-1-placeholder" for="emailSignUp">Seu e-mail</label>
						</fieldset>

						<footer class="form-1-footer">
							<button class="form-1-btn-submit">CRIAR CADASTRO</button>
						</footer><!-- form 1 footer -->
				</div><!-- sign up -->
			</div><!-- getin -->
		</div><!-- ctn -->
	</main>

<?php include("footer.php"); ?>