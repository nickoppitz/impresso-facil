<?php 
$namePage = "quem-somos";
include("header.php"); ?>

	<main>
		<div class="ctn">


		<section class="institucional">

			<h1 class="ttl-md-black">Quem somos</h1>

			<div class="content">

				<br>
				<br>
				<p>
						O jeito Algo Mais de ser, começou a nascer quando a Algo Mais Gráfica e Editora iniciou as atividades em 2004 com uma proposta bem definida de atuação: ofereccer a mais alta qualidade gráfica em produtos e serviços.
					
					<br>
					<br>

						Através de modernos equipamentos de impressão, juntamente com a experiência e a competência dos nossos profissionais, conseguimos dar mais vida aos seus impressos, com cores consistentes e fiéis, dentro do prazo programado.
					
					<br>
					<br>

						O jeito de ser Algo Mais, está e cada etapa do nosso trabalho, desde o atendimento, passando pela produção, até a entrega, mais do que satisfazer, buscamos sempre surpreender.

					<br>
					<br>

						Estamos cada vez mais renovados e originais. Tudo para oferecer a você uma empresa mais completa, que valoriza cada vez mais o cliente.

					<br>
					<br>

						Esperamos que possamos fazer uma grande parceria. Para maiores informações, entre em contato conosco:
					
					<br>
					
					<b>Tel: </b> (51) 3222-2339
					
					<br>
					
					<b>E-mail: </b> atendimento@algomais.art.br
				</p>
			
			</div><!-- content -->

		</section><!-- institucional -->

		</div><!-- ctn -->
	</main>
<?php include("footer.php"); ?>