<?php 
$namePage = "produtos-marca";
include("header.php"); 
?>

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Linhas de Produtos</h1>
			
			<ul class="breadcrumb">
				<li>Escolha uma marca</li>
			</ul><!-- breadcrumb -->

			<ul class="brands-catalog">
				<?php
					foreach($marcas as $marca){
						$imagem = file_exists('admin/uploads/marcas/thumbnail/'.$marca['MARIMAGEM']) ? 'admin/uploads/marcas/thumbnail/'.$marca['MARIMAGEM'] : 'admin/uploads/marcas/'.$marca['MARIMAGEM'];

						$imagem = file_exists($imagem) ? "<img src=\"{$imagem}\" alt=\"{$marca[MARNOME]}\" title=\"{$marca[MARNOME]}\">" : null;
						echo "<li>\n
								\t<article class=\"brand-itm\">\n
									\t\t<a href=\"marcas/{$marca[MARCODIGO]}/".gerarUrlAmigavel($marca['MARMARCA'])."\">\n
										\t\t\t<figure class=\"brand-logo\">\n
											\t\t\t\t{$imagem}\n
										\t\t\t</figure>\n
									\t\t</a>\n
									\t\t<h2 class=\"brand-ttl\"><a href=\"marcas/{$marca[MARCODIGO]}/".gerarUrlAmigavel($marca['MARMARCA'])."\">{$marca[MARMARCA]}</a></h2>\n
								\t</article>\n
							</li>\n";
					}
				?>
			</ul><!-- brands catalog -->
		</div><!-- ctn -->
	</main>
<?php 
 if(!isset($_SESSION['CLICODIGO']) || !is_numeric($_SESSION['CLICODIGO'])){
		include("modal-login-lg.php");
 }
 ?>	
<?php include("footer.php"); ?>