<?php
 $url = trim($_SERVER['SCRIPT_URI'], '/');
 $arrUrl = explode('/', $url);
 $redirect = (end($arrUrl) == 'pagamento') ? 'pagamento/' : '';
?>
<div class="modal-login-lg">
	<div class="modal-inner">
		<div class="ttl ttl-lg-black-2">Faça seu login</div>
		<p class="description">Área exclusiva para clientes cadastrados</p>

		<form action="/login/" method="POST" class="form-1">
			<fieldset class="full">
				<input type="text" name="cpfCnpj" id="cpfCnpj" class="cpfCnpj" required autofocus autocomplete="off">
				<label class="form-1-placeholder" for="cpfCnpj">Seu CPF/CNPJ</label>
			</fieldset>

			<fieldset class="full">
				<input type="password" name="senha" id="senha" required minlength="8">
				<label class="form-1-placeholder" for="senha">Sua senha</label>
			</fieldset>

			<ul class="support">
			    <li><a href="/cadastro/<?= $redirect; ?>">Cadastre-se</a></li>
			    <li><a href="/esqueci-minha-senha/">Esqueci minha senha</a></li>
			</ul>
			<input type="hidden" name="urlAtual" value="<?= $_SERVER['SCRIPT_URI'] ?>">
			<button class="btn-login btn-md-red-sq" type="submit">ENTRAR</button>
		</form>
	</div>
	<div class="modal-overlay"></div>
</div><!-- modal login lg -->