<?php 
$namePage = "meus-dados";
include("header.php"); 
require_once 'admin/transaction/transactionClientes.php';
require_once 'admin/transaction/transactionMarcas.php';
require_once 'admin/transaction/transactionTabelasDinamicas.php';
$fill = fillClientes($_SESSION['CLICODIGO']);
?>

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Meus dados</h1>
			
			<div class="panel">
				<!-- panel nav -->
				<?php include("panel-nav.php"); ?>
				<!-- panel nav -->

				<div class="panel-data-list">
					<form class="form-my-dates form-1 form-1-grey" name="form-register" method="POST" action="/meus-dados/alterar/">
						<div class="row">
							<fieldset class="half">
								<label class="select-arrow"></label>
								<select name="vSCLITIPO" id="vSCLITIPO" required>
									<option disabled selected></option>
								<?php
									foreach(comboTipoCliente() as $tipoCliente){
										if($tipoCliente['TABCODIGO'] == $fill['CLITIPO'])
											echo "<option value=\"{$tipoCliente[TABCODIGO]}\" selected>{$tipoCliente[TABDESCRICAO]}</option>\n";
										else	
											echo "<option value=\"{$tipoCliente[TABCODIGO]}\" >{$tipoCliente[TABDESCRICAO]}</option>\n";
									}
								?>
								</select>
								<label class="form-1-placeholder" for="assunto">Tipo de cliente</label>
							</fieldset>

							<fieldset class="half">
								<label class="select-arrow"></label>
								<select name="vSCLITIPOPESSOA" id="vSCLITIPOPESSOA" required>
									<option disabled selected></option>
									<option value="F" <?php if($fill['CLITIPOPESSOA'] == 'F') echo 'selected' ?>>Pessoa fisíca</option>
									<option value="J" <?php if($fill['CLITIPOPESSOA'] == 'J') echo 'selected' ?>>Pessoa jurídica</option>
								</select>
								<label class="form-1-placeholder" for="assunto">Tipo de pessoa</label>
							</fieldset>
						</div><!-- row -->	

						<div class="row">
							<fieldset class="full inputCpfCnpj">
								<input type="text" class="cpf" id="vSCLICPFCNPJ" name="vSCLICPFCNPJ" value="<?= $fill['CLICPFCNPJ']; ?>" required>
								<label class="form-1-placeholder" for="vSCLICPFCNPJ">CPF</label>
							</fieldset>
						</div><!-- row -->	
							
						<div class="row">	
							<fieldset class="half inputNome">
								<input type="text" id="vSCLINOME" name="vSCLINOME" value="<?= $fill['CLINOME']; ?>" required>
								<label class="form-1-placeholder" for="vSCLINOME">Nome</label>
							</fieldset>

							<fieldset class="half pessoaJuridica">
								<input type="text" id="vSCLIRAZAOSOCIAL" name="vSCLIRAZAOSOCIAL" value="<?= $fill['CLIRAZAOSOCIAL']; ?>" required>
								<label class="form-1-placeholder" for="vSCLIRAZAOSOCIAL">Razão social</label>
							</fieldset>
							
							<fieldset class="half pessoaFisica">
								<label class="select-arrow"></label>
								<select name="vSCLISEXO" id="vSCLISEXO" required>
									<option disabled selected></option>
									<option value="M" <?php if($fill['CLISEXO'] == 'M') echo 'selected' ?>>Masculino</option>
									<option value="F" <?php if($fill['CLISEXO'] == 'F') echo 'selected' ?>>Feminino</option>
									<option value="O" <?php if($fill['CLISEXO'] == 'O') echo 'selected' ?>>Outro</option>
								</select>
								<label class="form-1-placeholder" for="vSCLISEXO">Sexo</label>
							</fieldset>
						</div><!-- row -->	
						
						<div class="row">	
							<fieldset class="half">
								<input type="email" id="vSCLIEMAIL" name="vSCLIEMAIL" value="<?= $fill['CLIEMAIL']; ?>" required>
								<label class="form-1-placeholder" for="vSCLIEMAIL">E-mail</label>
							</fieldset>

							<fieldset class="half pessoaFisica">
								<input type="text" class="datepicker" id="vDCLIDATANASCIMENTO" name="vDCLIDATANASCIMENTO" value="<?= $fill['CLIDATANASCIMENTO']; ?>" required>
								<label class="form-1-placeholder" for="vDCLIDATANASCIMENTO">Data de nascimento</label>
							</fieldset>
						</div><!-- row -->	

						<div class="row">						
							<fieldset class="full pessoaJuridica">
								<input type="text" id="vSCLIINSCRICAOESTADUAL" name="vSCLIINSCRICAOESTADUAL" maxlength="30" value="<?= $fill['CLIINSCRICAOESTADUAL']; ?>" required>
								<label class="form-1-placeholder" for="vSCLIINSCRICAOESTADUAL">Inscrição Estadual</label>

								<input type="checkbox" id="checkIsencaoIE" name="checkIsencaoIE">
								<label class="form-1-addon" for="checkIsencaoIE">Isento</label>
							</fieldset>
						</div><!-- row -->	

						<div class="row">
							<fieldset class="half">
								<input type="text" class="telefone" id="vSCLITELEFONE" name="vSCLITELEFONE" value="<?= $fill['CLITELEFONE']; ?>" required>
								<label class="form-1-placeholder" for="vSCLITELEFONE">Telefone</label>
							</fieldset>

							<fieldset class="half">
								<input type="text" class="telefone" id="vSCLICELULAR" name="vSCLICELULAR" value="<?= $fill['CLICELULAR']; ?>" required>
								<label class="form-1-placeholder" for="vSCLICELULAR">Celular</label>
							</fieldset>
						</div><!-- row -->

						<div class="ttl-sm-red">Trocar senha</div>

						<div class="row">	
							<fieldset class="full">
								<input type="password" id="vSCLISENHAANTIGA" name="vSCLISENHAANTIGA" required minlength="8">
								<label class="form-1-placeholder" for="vSCLISENHAANTIGA">Senha atual</label>
							</fieldset>
						</div><!-- row -->

						<div class="row">					
							<fieldset class="half">
								<input type="password" id="vSCLISENHA" name="vSCLISENHA" minlength="8">
								<label class="form-1-placeholder" for="vSCLISENHA">Digite a nova senha</label>
							</fieldset>

							<fieldset class="half">
								<input type="password" id="vSCLISENHACONFIRMACAO" name="vSCLISENHACONFIRMACAO" data-rule-equalTo="#vSCLISENHA" minlength="8">
								<label class="form-1-placeholder" for="vSCLISENHACONFIRMACAO">Confirme a nova senha</label>
							</fieldset>
						</div><!-- row -->
						<div class="ttl-sm-red">Selecionar linhas de produtos</div>
						<div class="row">
							<?php 
								foreach(comboMarcas('ADMIN') as $marca){
									if (!empty($_SESSION['MARCAS']) && in_array($marca['MARCODIGO'], $_SESSION['MARCAS']))
										echo "<input type=\"checkbox\" id=\"{$marca[MARCODIGO]}\" name=\"vAMARCODIGO[]\" value=\"{$marca[MARCODIGO]}\" checked><label for=\"{$marca[MARCODIGO]}\" class=\"checkbox\">{$marca[MARMARCA]}\n</label>";
									else
										echo "<input type=\"checkbox\" id=\"{$marca[MARCODIGO]}\" name=\"vAMARCODIGO[]\" value=\"{$marca[MARCODIGO]}\"><label for=\"{$marca[MARCODIGO]}\" class=\"checkbox\">{$marca[MARMARCA]}\n</label>";
								}
							?>
						</div>
						<input type="hidden" name="vICLICODIGO" value="<?= $fill['CLICODIGO'] ?>">
						<footer class="form-1-footer">
							<button type="submit" class="btn-mbl-cnt btn-md-red">Alterar dados</button>
						</footer>
					</form><!-- form-1 -->
				</div><!-- panel-data-list -->
			</div><!-- panel -->			
		</div><!-- ctn -->
	</main>
<?php 
	if(!isset($_SESSION['CLICODIGO']) || !is_numeric($_SESSION['CLICODIGO'])){
		include("modal-login-lg.php");
	}
?>
<?php include("footer.php"); ?>