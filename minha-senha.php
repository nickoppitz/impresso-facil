<?php 
$namePage = "minha-senha";
include("header.php"); ?>

<main>
	<div class="ctn">
		<h1 class="ttl-md-black">Minha senha</h1>

		<div class="my-password">
			<h2 class="ttl-sm-black-2">Esqueci minha senha</h2>

			<form class="my-password-form form-1" name="form-recuperar-senha" action="/esqueci-minha-senha/recuperar/" method="POST">
				<fieldset class="full">
					<input type="text" name="vSCLICPFCNPJ" id="vSCLICPFCNPJ" class="cpfCnpj" required autofocus autocomplete="off">
					<label class="form-1-placeholder" for="vSCLICPFCNPJ">Seu CPF/CNPJ</label>
				</fieldset>

				<footer class="form-1-footer">
					<button class="btn-md-blue">RECEBER SENHA POR E-MAIL</button>
				</footer><!-- form 1 footer -->
			</form>
		</div><!-- my-password -->
	</div><!-- ctn -->
</main>

<?php include("footer.php"); ?>