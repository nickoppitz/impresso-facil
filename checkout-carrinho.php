<?php 
$namePage = "checkout-carrinho";
include("header.php"); ?>

<main>
	<div class="ctn">
		<h1 class="ttl-md-black">Checkout</h1>

		<ul class="step-by-step">
			<li>
				<div class="itm isActive">
					<div class="number cart"></div>
					<div class="description">Carrinho</div>
				</div><!-- itm -->				
			</li>

			<li>
				<div class="itm">
					<div class="number payment"></div>
					<div class="description">Pagamento</div>
				</div><!-- itm -->
			</li>

			<li>
				<div class="itm">
					<div class="number finish"></div>
					<div class="description">Concluído</div>
				</div><!-- itm -->
			</li>
		</ul><!-- step-by-step -->

		<div class="list-cart-product">
			<?php
			$valorTotal = 0;
			if(!empty($_SESSION['CARRINHO']) && is_array($_SESSION['CARRINHO'])){
				include_once 'admin/transaction/transactionProdutos.php';
				if(count($_SESSION['CARRINHO']) > 0){
					foreach ($_SESSION['CARRINHO'] as $carrinho){
						$valorTotal += $carrinho['VALORUNITARIO']*$carrinho['QUANTIDADE'];
						$produtoCarrinho = fillProdutos($carrinho['PRODUTO']);
						$imagem = is_file('../admin/uploads/produtos/'.$produtoCarrinho['PROCODIGO'].'/thumbnail/'.$produtoCarrinho['PROIMAGEMPRINCIPAL']) ? 'admin/uploads/produtos/'.$produtoCarrinho['PROCODIGO'].'/thumbnail/'.$produtoCarrinho['PROIMAGEMPRINCIPAL'] : 'admin/uploads/produtos/'.$produtoCarrinho['PROCODIGO'].'/'.$produtoCarrinho['PROIMAGEMPRINCIPAL'];
						$link = "/produtos/".gerarUrlAmigavel($produtoCarrinho['MARMARCA'])."/".gerarUrlAmigavel($produtoCarrinho['CATCATEGORIA'])."/{$produtoCarrinho[PROCODIGO]}/".gerarUrlAmigavel($produtoCarrinho['PRONOME'])."/";
						echo "<div class=\"itm-cart-product\" data-token=\"{$carrinho[TOKEN]}\">\n
						<div class=\"product\" data-title=\"PRODUTO\">\n
							<img class=\"product-pic\" src=\"{$imagem}\">\n
							<div class=\"product-name\">
								<a href=\"{$link}\">{$produtoCarrinho[PRONOME]}</a>\n
														
								<button class=\"btn-sm-blue btn-attachment\" id=\"{$produtoCarrinho['PROCODIGO']}\">Anexar arte</button>
								
							</div>\n
						</div>\n

						
						<div class=\"amount\">
							<div class=\"amount-box\" data-title=\"QUANTIDADE\">\n
								<input type=\"number\" class=\"input-amount\" name=\"quantidade\" id=\"quantidade\" value=\"{$carrinho[QUANTIDADE]}\" min=\"{$produtoCarrinho[PROQUANTIDADEMINIMAPORVENDA]}\" readonly data-token=\"{$carrinho[TOKEN]}\" max=\"{$produtoCarrinho[PROESTOQUE]}\">\n
								
								<div class=\"amount-max\">{$produtoCarrinho[PROESTOQUE]} unidades disponíveis</div>\n
								<div class=\"amount-min\"><b>Quantidade mínima por compra:</b> {$produtoCarrinho[PROQUANTIDADEMINIMAPORVENDA]}</div>\n
								
								<div class=\"value\" data-title=\"PREÇO\">\n
									<div class=\"price\">".formatar_moeda($carrinho['VALORUNITARIO']*$carrinho['QUANTIDADE'])."</div>\n
									<div class=\"value-unit\">(".formatar_moeda($carrinho['VALORUNITARIO'])." unid.)</div>\n";
									if($produtoCarrinho['PROTIPOFRETE'] == 1) echo "<div class=\"free-shipping\">Frete grátis</div>\n";
									echo "</div>\n
								</div>\n

							</div>\n

							<div class=\"actions\">\n
								<button class=\"btn-remove\" data-token=\"{$carrinho[TOKEN]}\"></button>\n
							</div>\n

						</div>\n";
					}
				}else{
					echo "<div class=\"cart-empty\">
							Ainda não há produtos no carrinho! <br>\n
							Clique em <span class=\"btn-buy btn-buy-sm\">Comprar</span> para adicionar produtos aqui.
						  </div>\n";
				}
			}else{
				echo "<div class=\"cart-empty\">
						Ainda não há produtos no carrinho! <br>\n
						Clique em <span class=\"btn-buy btn-buy-sm\">Comprar</span> para adicionar produtos aqui.
					  </div>\n";
			}
			?>
			<!--
			<div class="itm-cart-product">
				<div class="product">
					<img class="product-pic" src="http://www.algomais.teraware.info/admin/uploads/produtos/20/557563153c967bf5bc85839331017bfc.jpg">
					<div class="product-name">
						<a href="#">Nome do produto</a>

						<!-- #ATENÇÃO: leia comentário lá em modal-attachment.php antes de implementar aqui 
						<button class="btn-sm-blue btn-attachment" id="1">Anexar arte</button>
						<div class="obs-attachment">
							<span>*</span>
							Envio obrigatório.
						</div>
					</div>
				</div>
		
				<div class="amount">
					<div class="amount-box">
						<input type="number" class="input-amount" name="quantidade" id="quantidade" value="4" min="2" max="5">
						
						<div class="amount-max">5 unidades disponíveis</div>
						<div class="amount-min"><b>Quantidade mínima por compra:</b> 2</div>
						
						<div class="value">
							<div class="price">R$ 10,00</div>
							<div class="value-unit">(R$ 15,00 unid.)</div>
						</div>
					</div>
				</div>

				<div class="actions">
					<button class="btn-remove"></button>
				</div>
			</div>
-->
		</div><!-- list-cart-product -->

		<div class="cart-product-freight">
			<form class="cart-product-freight-form form-1" id="formFrete">
				<fieldset class="half">
					<input type="text" id="cep" name="cep" class="cep" required autocomplete="off">
					<label class="form-1-placeholder" for="cep">Seu CEP</label>
				</fieldset><!-- cart-product-cep -->

				<fieldset class="half">
					<button class="btn-calculate btn-md-black" type="submit">CALCULAR FRETE</button> <!-- btn-cart-product-cep -->
				</fieldset>
			</form>
			<img src="img/loader.svg" class="loader">
			<div class="result" style="display: none;">
				<div class="result-address"></div>
				<form class="form-1">
					<div class="info-value">
						<div class="row frete">
						</div>
					</div>
				</form>

				<p>Obs.: Previsão de entrega sujeita a alteração no fechamento do pedido em função da forma de pagamento.</p>
			</div><!-- result -->
		</div><!-- cart-product-freight -->

		<div class="cart-product-value">
			<div class="value-subtotal">Subtotal: <?= formatar_moeda($valorTotal) ?></div>
			<div class="value-freight">+ Frete: À Calcular</div>
			<div class="value-total">TOTAL: À Calcular</div>

			<div class="action">
				<form action="/checkout/pagamento/" method="POST" id="formPagamento" enctype="multipart/form-data">
					<a href="/marcas/" class="btn-continue">Continuar comprando</a>
					<input type="hidden" name="freteModalidade" value="0">
					<?php include("modal-attachment.php"); ?>
					<button type="submit" class="btn-finish btn-md-red">FINALIZAR COMPRA</button>
				</form>
			</div><!-- action -->
		</div><!-- cart-product-value -->

	</div><!-- ctn -->
</main>


<script>
	var subtotal = <?= $valorTotal ?>	
</script>

<?php include("footer.php"); ?>
