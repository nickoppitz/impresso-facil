<nav class="panel-nav">
	<header class="header-panel-nav">
		<img src="img/user-photo.jpg" class="pic-panel-nav">
		<div class="user-name-panel-nav"><?= $_SESSION['CLINOME']; ?></div>
	</header>
	<ul class="itms-panel-nav">
		<li <?php if ($namePage == "meus-pedidos") echo "class='isActive'"; ?>><a href="/meus-pedidos/">Meus pedidos</a></li>
		<li <?php if ($namePage == "meus-dados") echo "class='isActive'"; ?>><a href="/meus-dados/">Meus dados</a></li>
		<li <?php if ($namePage == "meus-enderecos-editar" || $namePage == "meus-enderecos") echo "class='isActive'"; ?>><a href="/meus-enderecos/">Meus endereços</a></li>
		<li><a href="logout.php" class="logout">Sair</a></li>
	</ul>
</nav><!-- panel-nav -->