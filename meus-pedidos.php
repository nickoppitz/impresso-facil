<?php 
	$namePage = "meus-pedidos";
	include("header.php");
	require_once 'admin/transaction/transactionPedidos.php';
	require_once 'admin/transaction/transactionPedidosxProdutos.php';
	require_once 'admin/transaction/transactionPedidosxProducao.php';
	require_once 'admin/transaction/transactionEnderecos.php';
	require_once 'admin/transaction/transactionClientes.php';
	require_once 'admin/transaction/transactionConfig.php';
	require_once "admin/transaction/transactionPedidosxAnexos.php";
	$endereco = fillEnderecosByCliente($_SESSION['CLICODIGO']);
	$cliente  = fillClientes($_SESSION['CLICODIGO']);
	$bancario = getDadosBancarios();

	if(!empty($_FILES['vFPXAANEXO'])){
		$anexos = reArrayFiles($_FILES['vFPXAANEXO']);
		
		$vIPROCODIGO = is_array($_POST['vIPROCODIGO']) ? $_POST['vIPROCODIGO'][0] : $_POST['vIPROCODIGO'];
		$diretorio = $_SERVER['DOCUMENT_ROOT']."/anexos/{$_POST['vIPEDCODIGO']}";
		if(!is_dir($diretorio))	mkdir($diretorio, 0755, true);

		foreach($anexos as $anexo){
			if(is_file($anexo['tmp_name'])){
				$nomeArquivo = removerAcentoEspacoCaracter($anexo['name']);
				uploadArquivo($anexo, $diretorio, $nomeArquivo);

				$arrAnexo = array(
					'vSPXAANEXO'   => $nomeArquivo,
					'vIPROCODIGO'  => $vIPROCODIGO,
					'vIPEDCODIGO'  => $_POST['vIPEDCODIGO'],
					'vIPXATAMANHO' => $anexo['size'],
					'vSPXATIPO'    => $anexo['type'],
				);
				insertPedidoAnexos($arrAnexo);
			}
		}
	}
?>
<main>
	<div class="ctn">
		<h1 class="ttl-md-black">Checkout</h1>
		<div class="panel">
			<?php include("panel-nav.php"); ?>
			<div class="panel-data-list">
				<div class="request-table table">	
					<div class="request-header table-header">
						<div class="lst-request">	
							<div class="itm-request-number">
								<div class="ttl ttl-md-black-2">N° DO PEDIDO</div>
							</div>
							<div class="itm-date">
								<div class="ttl ttl-md-black-2">DATA</div>
							</div>
							<div class="itm-payment">
								<div class="ttl ttl-md-black-2">FORMA DE PAGAMENTO</div>
							</div>
							<div class="itm-status">
								<div class="ttl ttl-md-black-2">STATUS</div>
							</div>
							<div class="itm-status-payment">
								<div class="ttl ttl-md-black-2">STATUS DE PAGAMENTO</div>
							</div>
							<div class="itm-actions"></div>
						</div>
					</div>
					<?php
						foreach(listPedidosByCliente($_SESSION['CLICODIGO']) as $pedido): 
							switch ($pedido['PEDFORMAPAGAMENTO']) {
								case 0:
									$formaPagamento = "Boleto Itaú";
									$imagemPagamento = 'itau.png';
									break;
								case 1:
									$formaPagamento = "Depósito em conta Itaú";
									$imagemPagamento = 'itau.png';
									break;
							}
					?>
					<div class="request-content table-content" data-ref="<?= str_pad($pedido['PEDCODIGO'], 11, '0', STR_PAD_LEFT); ?>">
						<div class="lst-request">
							<div class="itm itm-request-number" data-ttl-itm="N° DO PEDIDO"><?= str_pad($pedido['PEDCODIGO'], 11, '0', STR_PAD_LEFT); ?></div>
							<div class="itm itm-date" data-ttl-itm="DATA"><?= $pedido['PEDDATA_INC'] ?></div>
							<div class="itm itm-payment" data-ttl-itm="FORMA DE PAGAMENTO"><?= $formaPagamento ?></div>
							<div class="itm itm-status" data-ttl-itm="STATUS"><?= $pedido['POSICAO'] ?></div>
							<div class="itm itm-status-payment" data-ttl-itm="STATUS DE PAGAMENTO"><?= $pedido['STATUSPAGAMENTO'] ?></div>
							<div class="itm itm-actions">
								<div class="btn-dropdown"></div>
							</div>
						</div>
						<div class="request-detail table table-grey">							
							<section>
								<header class="table-header">
									<div class="ttl ttl-md-black-2">DETALHES DO PEDIDO</div> 
								</header>
								<div class="table-content">	
									<div class="lst-request-detail">
										<ul>
											<li class="<?= ($pedido['PEDPOSICAO'] >= 0) ? 'success' : 'waiting'; ?> itm-request-detail">1) PEDIDO REALIZADO</li>
											<li class="<?= ($pedido['PEDPOSICAO'] >= 1) ? 'success' : 'waiting'; ?> itm-request-detail">2) CONFIRMAÇÃO DE PAGAMENTO</li>
											<li class="<?= ($pedido['PEDPOSICAO'] >= 2) ? 'success' : 'waiting'; ?> itm-request-detail">3) EM PRODUÇÂO</li>
											<li class="<?= ($pedido['PEDPOSICAO'] >= 3) ? 'success' : 'waiting'; ?> itm-request-detail">4) PEDIDO ENVIADO</li>
											<li class="<?= ($pedido['PEDPOSICAO'] >= 4) ? 'success' : 'waiting'; ?> itm-request-detail">5) PEDIDO ENTREGUE</li>
										</ul>
									</div><!-- lst-request-detail -->	
								</div><!-- table-content request-detail-content -->	
							</section>
							<section>
								<header class="table-header">
									<div class="ttl ttl-md-black-2">RESUMO DO PEDIDO</div>
								</header>
								<div class="table-content">
									<ul>
										<div class="lst-request-resume">	
											<li class="itm-request-resume"><b>Status do pedido</b><?= $pedido['POSICAO'] ?></li>
											<li class="itm-request-resume"><b>Status do pagamento</b><?= $pedido['STATUSPAGAMENTO'] ?></li>
											<li class="itm-request-resume"><b>Código do pedido</b><?= str_pad($pedido['PEDCODIGO'], 11, '0', STR_PAD_LEFT); ?></li>
											<li class="itm-request-resume"><b>Data do pedido</b><?= $pedido['PEDDATA_INC'] ?></li>
											<li class="itm-request-resume"><b>Subtotal</b><?= $pedido['SUBTOTAL'] ?></li>
											<li class="itm-request-resume"><b>Frete</b><?= $pedido['FRETE'] ?></li>
											<li class="itm-request-resume"><b>Descontos (-)</b><?= formatar_moeda($pedido['PEDDESCONTOS']) ?></li>
											<li class="itm-request-resume"><b>Acréscimos (+)</b><?= formatar_moeda($pedido['PEDACRESCIMOS']) ?></li>
											<li class="itm-request-resume"><b>Total</b><?= $pedido['VALORTOTAL'] ?></li>
										</div>
									</ul>
								</div>
							</section>
							<section>
								<header class="address-payment-header table-header">
									<div class="lst-address-payment">
										<ul>
											<li class="ttl ttl-md-black-2 itm-address-payment">ENDEREÇO DE ENTREGA</li>
											<li class="ttl ttl-md-black-2 itm-address-payment">FORMAS DE PAGAMENTO</li>
										</ul>
									</div>
								</header>
								<div class="address-payment-content table-content">	
									<div class="delivery-address" data-ttl-itm="ENDEREÇO DE ENTREGA">	
										<ul>
											<li class="itm-address-payment">
												<?= $endereco['ENDLOGRADOURO'].', '.$endereco['ENDNUMERO'].' / '.$endereco['ENDCOMPLEMENTO'].'<br>'.$endereco['ENDBAIRRO'].'<br>'.$endereco['CIDDESCRICAO'].' - '.$endereco['ESTSIGLA'].'<br> CEP:'.$endereco['ENDCEP'] ?>
											</li>
										</ul>
									</div>
									<div class="payment-form" data-ttl-itm="FORMAS DE PAGAMENTO">
										<div class="payment-form-detail">
											<img class="pic" src="img/<?= $imagemPagamento; ?>">
											<div class="txt">
												<p>
											<?php 
												if ($pedido['PEDFORMAPAGAMENTO'] == 0) {
													echo $formaPagamento. "<br>";
													echo "<a target=\"_blank\" href=\"/checkout/boleto/{$pedido[PEDCODIGO]}/\">Efetuar pagamento</a>";							
												}
												if ($pedido['PEDFORMAPAGAMENTO'] == 1) {
													echo $formaPagamento. "<br>";
													echo "<div>Agência: <span>".$bancario['CFGAGENCIA']."</span></div>";
													echo "<div>Conta: <span>".$bancario['CFGCONTA']."</span></div>";
													echo "<div>Razão social: <span>".$bancario['CFGCEDENTE']."</span></div>";
													echo "<div>CNPJ: <span>".$bancario['CFGCNPJ']."</span></div>";
													echo "<div><br></div>";
													echo "<div>Enviar um E-mail com o comprovante de depósito para: <span>".getConfig('CFGEMAILRECEBIMENTO')."</span></div>";
												}
											?>
												</p>
											</div>
										</div>
									</div>	
								</div>
							</section>
							<section>
								<header class="table-header">
									<div class="ttl ttl-md-black-2">ITENS DO PEDIDO</div>
								</header>
								<div class="table-content">
									<?php 
										$listProdutos = gridProdutosPedido($pedido['PEDCODIGO']);
										foreach($listProdutos['dados'] as $produto):
									?>
									<div class="lst-order-dtls">
										<ul>
											<li class="itm-order">
												<b>PRODUTO</b>
												<?= $produto['PRONOME']; ?>
											</li>
											<li class="itm-order">
												<b>QUANTIDADE</b>
												<?= $produto['PXPQUANTIDADE']; ?>
											</li>
											<li class="itm-order">
												<b>VALOR UNITÁRIO</b>
												<?= formatar_moeda($produto['PXPVALOR']) ?>
											</li>
											<li class="itm-order">
												<b>VALOR TOTAL</b>
												<?= formatar_moeda($produto['PXPVALOR']*$produto['PXPQUANTIDADE']) ?>
											</li>
											<li class="itm-order">
												<b>ARQUIVOS ANEXADOS</b>

												<ul class="lst-request-files"> 
													<?php 
														$listAnexos = gridPedidoAnexos($pedido['PEDCODIGO'], $produto['PROCODIGO']);
													    foreach ($listAnexos['dados'] as $anexo):
															if ($anexo['PXACARRINHO'] != '')
																$vSCaminho = 'anexos\\'.$anexo['PXACARRINHO'].'\\'.$anexo['PXAANEXO'];
															else
																$vSCaminho = 'anexos\\'.$anexo['PEDCODIGO'].'\\'.$anexo['PXAANEXO'];
													?>	
														<li class="itm-request-files">
															<a target="_blank" href="<?= $vSCaminho;?>"><?= $anexo['PXAANEXO'];?></a>
														</li>
													<?php  
														endforeach;
													?>				
												</ul><!-- lst-request-files -->

												<?php if ($pedido['PEDPOSICAO'] < 2) { ?>
													<button class="btn-sm-blue btn-attachment btn-art-attachment" id="<?= $pedido['PEDCODIGO'];?>-<?= $produto['PROCODIGO'];?>" >Anexar arte</button>
												<?php } ?>
											</li>
											<?php
												$producoes = gridPedidoProducao($pedido['PEDCODIGO'],$produto['PROCODIGO']);
												if($producoes['quantidadeRegistros'] > 0):
											?>	
											<li class="itm-order">
												<b>PROVA VIRTUAL</b>

												<ul class="lst-request-files"> 
													<?php
														foreach($producoes['dados'] as $producao):
															$arquivo = "admin/uploads/pedidoProducao/".$producao['PPRANEXO'];
															$infoArquivoProducao = pathinfo($arquivo);
															if(is_file($arquivo)):
													?>
																<li class="itm-request-files">
																	<a target="_blank" href="<?= $arquivo;?>" class="link-request-file"><?= $infoArquivoProducao['basename'];?></a>

																	<br>
																	<?php if($producao['PPRSITUACAO'] == 'E'): ?>
																		<button class="btn-xsm-green btn-approved" data-producao="<?= $producao['PPRCODIGO'] ?>">Aprovar</button>
																		<button class="btn-xsm-red btn-disapproved" data-producao="<?= $producao['PPRCODIGO'] ?>">Reprovar</button>
																	<?php elseif($producao['PPRSITUACAO'] == 'A'): ?>
																		<button class="btn-xsm-green btn-approved btn-disabled">Aprovado</button>
																	<?php elseif($producao['PPRSITUACAO'] == 'R'): ?>
																		<button class="btn-xsm-red btn-disapproved btn-disabled">Reprovado</button>
																	<?php endif; ?>
																</li>
													<?php
															endif; 
														endforeach; 
													?>
												</ul><!-- lst-request-files -->
											</li>
										<?php endif; ?>
										</ul>
									</div>
									<?php
									endforeach; ?>
								</div>
							</section>
							
							<button class="btn-close-detail btn-md-red-sq">FECHAR DETALHES</button>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</main>
<?php 
	if(!isset($_SESSION['CLICODIGO']) || !is_numeric($_SESSION['CLICODIGO'])){
		include("modal-login-lg.php");
	}
?>

<?php include("modal-attachment-pedidos.php"); ?>

<?php include("footer.php"); ?>