<?php 
$namePage = "cadastro";
include("header.php"); 
require_once 'admin/transaction/transactionEstados.php';
require_once 'admin/transaction/transactionMarcas.php';
require_once 'admin/transaction/transactionTabelasDinamicas.php';
?>

<main>
	<div class="ctn">
		<h1 class="ttl ttl-md-black">Cadastre-se</h1>

		<form class="form-1 form-1-grey" name="form-register" method="POST" action="/cadastro/salvar/">

			<h3 class="ttl-sm-red"><div class="ttl-number">1</div> Dados gerais </h3>

			<div class="row">
				<fieldset class="half">
					<label class="select-arrow"></label>
					<select name="vSCLITIPO" id="vSCLITIPO" required>
						<option disabled selected></option>
					<?php 
						foreach(comboTipoCliente() as $tipoCliente){
							echo "<option value=\"{$tipoCliente[TABCODIGO]}\" >{$tipoCliente[TABDESCRICAO]}</option>\n";
						}
					?>
					</select>
					<label class="form-1-placeholder" for="assunto">Tipo de cliente</label>
				</fieldset>
				<fieldset class="half">
					<label class="select-arrow"></label>
					<select name="vSCLITIPOPESSOA" id="vSCLITIPOPESSOA" required>
						<option disabled selected></option>
						<option value="F" >Pessoa fisíca</option>
						<option value="J" >Pessoa jurídica</option>
					</select>
					<label class="form-1-placeholder" for="assunto">Tipo de pessoa</label>
				</fieldset>
			</div><!-- row -->	

			<div class="row">
				<fieldset class="full inputCpfCnpj">
					<input type="text" class="cpf" id="vSCLICPFCNPJ" name="vSCLICPFCNPJ" required>
					<label class="form-1-placeholder" for="vSCLICPFCNPJ">CPF</label>
				</fieldset>
			</div><!-- row -->	

			<div class="row">	
				<fieldset class="half inputNome">
					<input type="text" id="vSCLINOME" name="vSCLINOME" required>
					<label class="form-1-placeholder" for="vSCLINOME">Nome</label>
				</fieldset>

				<fieldset class="half pessoaJuridica">
					<input type="text" id="vSCLIRAZAOSOCIAL" name="vSCLIRAZAOSOCIAL" required>
					<label class="form-1-placeholder" for="vSCLIRAZAOSOCIAL">Razão social</label>
				</fieldset>

				<fieldset class="half pessoaFisica">
					<label class="select-arrow"></label>
					<select name="vSCLISEXO" id="vSCLISEXO" required>
						<option disabled selected></option>
						<option value="M">Masculino</option>
						<option value="F">Feminino</option>
						<option value="O">Outro</option>
					</select>
					<label class="form-1-placeholder" for="vSCLISEXO">Sexo</label>
				</fieldset>
			</div><!-- row -->	

			<div class="row">	
				<fieldset class="half">
					<input type="email" id="vSCLIEMAIL" name="vSCLIEMAIL" required>
					<label class="form-1-placeholder" for="vSCLIEMAIL">E-mail</label>
				</fieldset>

				<fieldset class="half pessoaFisica">
					<input type="text" class="datepicker" id="vDCLIDATANASCIMENTO" name="vDCLIDATANASCIMENTO" required>
					<label class="form-1-placeholder" for="vDCLIDATANASCIMENTO">Data de nascimento</label>
				</fieldset>
			</div><!-- row -->	

			<div class="row">						
				<fieldset class="full pessoaJuridica">
					<input type="text" id="vSCLIINSCRICAOESTADUAL" name="vSCLIINSCRICAOESTADUAL" maxlength="30" required>
					<label class="form-1-placeholder" for="vSCLIINSCRICAOESTADUAL">Inscrição Estadual</label>

					<input type="checkbox" id="checkIsencaoIE" name="checkIsencaoIE">
					<label class="form-1-addon" for="checkIsencaoIE">Isento</label>
				</fieldset>
			</div><!-- row -->	

			<div class="row">
				<fieldset class="half">
					<input type="text" class="telefone" id="vSCLITELEFONE" name="vSCLITELEFONE" required>
					<label class="form-1-placeholder" for="vSCLITELEFONE">Telefone</label>
				</fieldset>

				<fieldset class="half">
					<input type="text" class="telefone" id="vSCLICELULAR" name="vSCLICELULAR" required>
					<label class="form-1-placeholder" for="vSCLICELULAR">Celular</label>
				</fieldset>
			</div><!-- row -->

			<div class="ttl-sm-red">Senha</div>

			<div class="row">					
				<fieldset class="half">
					<input type="password" id="vSCLISENHA" name="vSCLISENHA" minlength="8">
					<label class="form-1-placeholder" for="vSCLISENHA">Digite a nova senha</label>
				</fieldset>

				<fieldset class="half">
					<input type="password" id="vSCLISENHACONFIRMACAO" name="vSCLISENHACONFIRMACAO" data-rule-equalTo="#vSCLISENHA" minlength="8">
					<label class="form-1-placeholder" for="vSCLISENHACONFIRMACAO">Confirme a nova senha</label>
				</fieldset>
			</div><!-- row -->
			<h3 class="ttl-sm-red"><div class="ttl-number">2</div> Seu endereço </h3>

			<div class="row">
				<fieldset class="full">
					<input type="text" id="vSENDCEP" name="vSENDCEP" value="<?= $fill['ENDCEP']; ?>" required>
					<label class="form-1-placeholder" for="vSENDCEP">CEP:</label>
				</fieldset>
			</div><!-- row -->

			<div class="row">
				<fieldset class="full">
					<input type="text" id="vSENDLOGRADOURO" name="vSENDLOGRADOURO" value="<?= $fill['ENDLOGRADOURO']; ?>" required>
					<label class="form-1-placeholder" for="vSENDLOGRADOURO">Endereço:</label>
				</fieldset>
			</div><!-- row -->

			<div class="row">
				<fieldset class="half">
					<input type="text" id="vIENDNUMERO" name="vIENDNUMERO" value="<?= $fill['ENDNUMERO']; ?>" required>
					<label class="form-1-placeholder" for="vIENDNUMERO">Número:</label>
				</fieldset>

				<fieldset class="half">
					<input type="text" id="vSENDCOMPLEMENTO" name="vSENDCOMPLEMENTO" value="<?= $fill['ENDCOMPLEMENTO']; ?>">
					<label class="form-1-placeholder" for="vSENDCOMPLEMENTO">Complemento</label>
				</fieldset>
			</div><!-- row -->

			<div class="row">
				<fieldset class="full">
					<input type="text" id="vSENDBAIRRO" name="vSENDBAIRRO" value="<?= $fill['ENDBAIRRO']; ?>" required>
					<label class="form-1-placeholder" for="vSENDBAIRRO">Bairro:</label>
				</fieldset>
			</div><!-- row -->	

			<div class="row">
				<fieldset class="half">
					<label class="select-arrow"></label>
					<select id="vIESTCODIGO" name="vIESTCODIGO" required>
						<option disabled selected></option>
						<?php 
						foreach(comboEstados() as $estado){
							echo "<option value=\"{$estado[ESTCODIGO]}\">{$estado[ESTDESCRICAO]}</option>\n";
						}
						?>	
					</select>
					<label class="form-1-placeholder" for="vIESTCODIGO">Estado</label>
				</fieldset>

				<fieldset class="half">
					<label class="select-arrow"></label>
					<select class="form-control select" id="vICIDCODIGO" name="vICIDCODIGO" disabled>

					</select>
					<label class="form-1-placeholder" for="vICIDCODIGO">Cidade</label>
				</fieldset>
			</div><!-- row -->
			<div class="ttl-sm-red">Selecionar Linhas de produtos</div>
 			<div class="row">
 				<?php 
 					foreach(comboMarcas('ADMIN') as $marca){
 						echo "<input type=\"checkbox\" id=\"{$marca[MARCODIGO]}\" name=\"vAMARCODIGO[]\" value=\"{$marca[MARCODIGO]}\"><label for=\"{$marca[MARCODIGO]}\" class=\"checkbox\">{$marca[MARMARCA]}\n</label>";
 					}
 				?>
 			</div>
			<footer class="form-1-footer">
				<input type="hidden" name="vSREDIRECT" value="<?= $parametros[1] ?>">
				<button type="submit" class="btn-mbl-cnt btn-md-red"> CADASTRAR </button>
			</footer>
		</form><!-- form-1 -->	

	</div><!-- ctn -->
</main>
<?php include("footer.php"); ?>