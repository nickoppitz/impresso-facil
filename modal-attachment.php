<?php
if(count($_SESSION['CARRINHO']) > 0):
	foreach ($_SESSION['CARRINHO'] as $carrinho):
		?>
		<div class="modal-attachment" id="modal-attachment-<?=$carrinho['PRODUTO']?>">
			<div class="modal-inner">
				<div class="btn-close" data-dismiss="modal"></div>
				
				<div class="attachment-lst">
					<div class="anexar">
						<div class="title-anexar">ANEXAR ARQUIVOS</div>
						<div class="anexo">
							<label class="label-upload">Adicionar um arquivo</label>
							<input type="file" class="input-upload" id="vFPXAANEXO[]" name="vFPXAANEXO[]">
							<input type="hidden" class="hidden-upload" name="vIPROCODIGO[]" id="vIPROCODIGO[]" value="<?=$carrinho['PRODUTO']?>">
							<div class="status-upload"><b>Escolha um arquivo</b></div>								
							<div class="remover"></div>
						</div><!-- anexo -->
					</div><!-- anexar -->
					<div class="anexar-mais-arquivos" id="<?=$carrinho['PRODUTO']?>"></div>

				</div><!-- attachment-lst -->
				<div class="btn-md-red btn-concluido" data-dismiss="modal" value="Concluído">Concluído</div>
			</div>
			<div class="modal-overlay"></div>
		</div>
		<?php
	endforeach;
endif;