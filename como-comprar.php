<?php 
$namePage = "como-comprar";
include("header.php"); ?>

	<main>
		<div class="ctn">


		<section class="institucional">
			<h1 class="ttl-md-black">Como comprar</h1>

			<div class="content">
				<p>
				
					<br>

					1. Para efetuar a compra, primeiro você deve se cadastrar na nossa loja.
					<br>
					2. Escolha o produto que está disponível ou faça a sua encomenda.
					<br>
					3. Daremos opções de envios e seus valores.
					<br>
					4. Escolha o frete.
					<br>
					5. Escolha a forma de pagamento.
					<br>
					6. Após a confirmação do pagamento pela instituição financeira, o produto será produzido e posteriormente enviado, conforme orientações enviadas por e-mail.

					<br>
					<br>
					<b>
						Condições de Pagamento
					</b>
					 <br> 
					- Depósito Bancário
					<br>
					- Boleto Bancário			
					<br>
					<br>

					<b>
						Frete 
					</b> 
					<br> 
					- Após o envio do produto, enviamos o código de rastreamento para que o cliente possa acompanhar a entrega.
				</p>
			</div><!-- content -->
			
		</section><!-- institucional -->

		</div><!-- ctn -->
	</main>
<?php include("footer.php"); ?>