<?php 
$namePage = "produtos-pesquisa";
include("header.php"); ?>

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Resultados para: "<?= $_POST['pesquisa'] ?>"</h1>

			<ul class="breadcrumb">
				<li><a href="/marcas/">Produtos</a></li>
				<li>Pesquisa</li>
			</ul><!-- breadcrumb -->

			<ul class="products-catalog">
				<!-- products catalog -->
			</ul>

			<img src="img/loader.svg" class="loader">
		</div><!-- ctn -->
	</main>
<script type="text/javascript">
	var pesquisa = "<?= $_POST['pesquisa'] ?>";
</script>
<?php include("footer.php"); ?>