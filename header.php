<?php 
require_once 'admin/includes/constantes.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<base href="<?=$_SERVER['SCRIPT_NAME']?>" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- viewport -->

	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="fav/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="fav/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="fav/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="fav/manifest.json">
	<link rel="mask-icon" href="fav/safari-pinned-tab.svg" color="#ec1b23">
	<link rel="shortcut icon" href="fav/favicon.ico">
	<meta name="msapplication-config" content="fav/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="admin/libs/sweetalert/dist/sweetalert.css">
	<script type="text/javascript" src="admin/libs/sweetalert/dist/sweetalert.min.js"></script>
	<!-- favicon -->

	<!-- libs css -->
	<link rel="stylesheet" type="text/css" href="admin/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
	<!-- libs css -->

	<!-- main css -->
	<link rel="stylesheet" href="css/main.css">
	<!-- main css -->

	<?php include("scripts_header.php"); ?>

	<title>Loja da Algo Mais Gráfica e Editora</title>
</head>
<body <?php echo "class='$namePage'"; ?>>
	<header class="main-header">
		<div class="main-header-top">
			<div class="ctn">
				<div class="message">
					Bem-vindo à Algo Mais Gráfica e Editora! Conheça os melhores produtos do mercado.
				</div><!-- message -->

				<div class="actions">
					<ul class="contact-itms">
						<li>
							<div class="phone">(51) 3222-2339</div>
						</li>
						<li>
							<a href="/contato/" class="customer-service">Atendimento</a>
						</li>
					</ul><!-- contact itms -->

					<ul class="user-area">
						<li>
							<button class="btn-login"></button>
							
							<?php if(!isset($_SESSION['CLICODIGO']) || !is_numeric($_SESSION['CLICODIGO'])): ?>
								<div class="modal-login">
									<button class="btn-close"></button>

									<div class="ttl">Faça seu login</div>
									<form action="/login/" method="POST">
										<fieldset>
											<input type="text" name="cpfCnpj" class="cpfCnpj" required autofocus autocomplete="off" placeholder="Seu CPF/CNPJ">
										</fieldset>
										<fieldset>
											<input type="password" name="senha" required placeholder="Sua senha" minlength="8">
										</fieldset>
										<input type="hidden" name="urlAtual" value="<?= $_SERVER['SCRIPT_URI'] ?>">
										<button class="btn-login btn-md-red-sq" type="submit">ENTRAR</button>
										
										<ul class="support">
										    <li><a href="/cadastro/">Cadastre-se</a></li>
										    <li><a href="/esqueci-minha-senha/">Esqueci minha senha</a></li>
										</ul>
									</form>

								</div> <!-- modal login NOT LOGGED  -->
							<?php else: ?>
								<div class="modal-login logged">
									<button class="btn-close"></button>

									<header>
										<img src="img/user-photo.jpg" class="pic-user">

										<div class="name-user"><?= $_SESSION['CLINOME'] ?></div>
									</header>

									<ul class="nav-user">
										<li><a href="/meus-pedidos/">Meus pedidos</a></li>
										<li><a href="/meus-dados/">Meus dados</a></li>
										<li><a href="/meus-enderecos/">Meus endereços</a></li>
										<li><a href="/logout/"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a></li>
									</ul>
								</div><!-- modal login LOGGED -->
							<?php endif; ?>
						</li>
						<li>
							<button class="btn-shopping-cart"></button>

							<div class="modal-shopping-cart">
								<button class="btn-close"></button>

								<div class="ttl">Meu carrinho</div>

								<div class="product-lst">
								<?php
									$valorTotal = 0;
									if(!empty($_SESSION['CARRINHO']) && is_array($_SESSION['CARRINHO'])){
										include_once 'admin/transaction/transactionProdutos.php';
										if(count($_SESSION['CARRINHO']) > 0){
											foreach ($_SESSION['CARRINHO'] as $carrinho){
												$valorTotal += $carrinho['VALORUNITARIO']*$carrinho['QUANTIDADE'];
												$produtoCarrinho = fillProdutos($carrinho['PRODUTO']);
												$imagem = is_file('../admin/uploads/produtos/'.$produtoCarrinho['PROCODIGO'].'/thumbnail/'.$produtoCarrinho['PROIMAGEMPRINCIPAL']) ? 'admin/uploads/produtos/'.$produtoCarrinho['PROCODIGO'].'/thumbnail/'.$produtoCarrinho['PROIMAGEMPRINCIPAL'] : 'admin/uploads/produtos/'.$produtoCarrinho['PROCODIGO'].'/'.$produtoCarrinho['PROIMAGEMPRINCIPAL'];
												$link = "/produtos/".gerarUrlAmigavel($produtoCarrinho['MARMARCA'])."/".gerarUrlAmigavel($produtoCarrinho['CATCATEGORIA'])."/{$produtoCarrinho[PROCODIGO]}/".gerarUrlAmigavel($produtoCarrinho['PRONOME'])."/";
												echo "<div class=\"product-itm\" data-token=\"{$carrinho[TOKEN]}\">\n
														<button class=\"btn-rm\" onClick=\"removeToCart('{$carrinho[TOKEN]}', this)\"></button>\n

														<img src=\"{$imagem}\" class=\"pic\">\n

														<div class=\"text\">\n
															<div class=\"product-ttl\"><a href=\"{$link}\">{$produtoCarrinho[PRONOME]}</a></div>\n

															<div class=\"category\">{$produtoCarrinho[MARMARCA]}</div>\n

															<div class=\"description\">{$produtoCarrinho[PROPREDESCRICAO]}</div>\n

															<div class=\"units\">{$carrinho['QUANTIDADE']} ";
															echo ($carrinho['QUANTIDADE'] > 1) ? ' Unidades' : ' Unidade';
															echo "</div>\n
														</div>\n
													</div>";
											}
										}else{
											echo "<div class=\"cart-empty\">
													Ainda não há produtos no carrinho! <br>\n
													Clique em <span class=\"btn-buy btn-buy-sm\">Comprar</span> para adicionar produtos aqui.
												  </div>\n";
										}
									}else{
										echo "<div class=\"cart-empty\">
												Ainda não há produtos no carrinho! <br>\n
												Clique em <span class=\"btn-buy btn-buy-sm\">Comprar</span> para adicionar produtos aqui.
											  </div>\n";
									}
									$visibilidade = (count($_SESSION['CARRINHO']) > 0) ? '' : 'style="display:none;"';
								?>
							
								</div><!-- product lst -->

								<div class="resume" <?= $visibilidade ?>>
									<div class="itms-added"><?php echo count($_SESSION['CARRINHO']); echo (count($_SESSION['CARRINHO']) > 1) ? ' itens adicionados' : ' item adicionado' ?></div>

									<div class="total-price">Total <?= formatar_moeda($valorTotal); ?></div>

									<a href="/checkout/carrinho/" class="btn-view-cart btn-md-red-sq">VER CARRINHO</a>
								</div>
							</div><!-- modal shopping cart -->
						</li>
					</ul><!-- user area -->
				</div><!-- actions -->
			</div><!-- ctn -->
		</div><!-- main header top -->

		<div class="main-header-bottom">
			<div class="ctn">
				<a href="index.php" class="logo">
					<img src="img/logo.png" alt="Logotipo">
				</a><!-- logo -->

				<button class="btn-menu">Menu</button>


				<div class="main-search">
					<form action="/produtos/pesquisar/" method="POST" class="form-1">
						<input type="text" placeholder="Procure por produtos" required class="notAsterisk" name="pesquisa" autocomplete="off">
						<button class="btn-main-search" type="submit"></button>
					</form>
				</div><!-- main search -->
			</div><!-- ctn -->
			<nav class="main-nav">
				<div class="ctn">
					<ul class="nav-tab">
						<li data-tab="home"><a href="/home/">Home</a></li>
						<?php
							require_once 'admin/transaction/transactionMarcas.php';
							require_once 'admin/transaction/transactionCategorias.php';
							$marcas = comboMarcas();
							foreach ($marcas as $marca) {
								echo "<li data-tab=\"marca-{$marca[MARCODIGO]}\">{$marca[MARMARCA]}</li>\n";
							}
						?>
					</ul><!-- nav tab -->
					<?php
						foreach($marcas as $marca){
							$imagem = file_exists('admin/uploads/marcas/thumbnail/'.$marca['MARIMAGEM']) ? 'admin/uploads/marcas/thumbnail/'.$marca['MARIMAGEM'] : 'admin/uploads/marcas/'.$marca['MARIMAGEM'];

							$imagem = file_exists($imagem) ? "<img src=\"{$imagem}\" alt=\"{$marca[MARMARCA]}\" class=\"nav-tab-content-photo\" title=\"{$marca[MARMARCA]}\">" : null;
							$categorias = categoriasByMarca($marca['MARCODIGO']);
								echo "<div id=\"marca-{$marca[MARCODIGO]}\" class=\"nav-tab-content\">\n
										\t<div class=\"ttl\">{$marca[MARMARCA]}</div>\n
										\t<ul class=\"nav-tab-content-itms\">\n";
							if($categorias['quantidadeRegistros'] > 0){
								foreach($categorias['dados'] as $categoria){
									echo "\t\t<li><a href=\"/marcas/{$marca[MARCODIGO]}/".gerarUrlAmigavel($marca['MARMARCA'])."/{$categoria[CATCODIGO]}/".gerarUrlAmigavel($categoria['CATCATEGORIA'])."\">{$categoria[CATCATEGORIA]}</a></li>\n";
								}
							}else{
								echo "\t\t<li>Não há categorias para esta marca!</li>\n";
							}
								echo "\t</ul>\n
										\t{$imagem}\n
										</div>\n";
						}
					?>
					
				</div><!-- ctn -->
			</nav><!-- main nav -->
		</div><!-- main heaer bottom -->
	</header><!-- main header -->