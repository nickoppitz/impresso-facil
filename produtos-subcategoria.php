<?php 
$namePage = "produtos-subcategoria";
include("header.php"); ?>

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Agendas</h1>

			<ul class="breadcrumb">
				<li><a href="/marcas/">Linhas de Produtos</a></li>
				<?php $infoMarca = fillMarcas($parametros[1]); ?>
				<li><a href="/marcas/<?= $infoMarca['MARCODIGO'] ?>/<?= $infoMarca['MARMARCA'] ?>"><?= $infoMarca['MARMARCA'] ?></a></li>
				<?php $infoCategoria = fillCategorias($parametros[3]); ?>
				<li><?= $infoCategoria['CATCATEGORIA'] ?></li>
			</ul><!-- breadcrumb -->
			
			<ul class="products-catalog">
				<!-- products catalog -->
			</ul>

			<img src="img/loader.svg" class="loader">
		</div><!-- ctn -->
	</main>
<script type="text/javascript">
	var marca = <?= $parametros[1]; ?>;
	var categoria = <?= $parametros[3]; ?>;
</script>
<?php include("footer.php"); ?>