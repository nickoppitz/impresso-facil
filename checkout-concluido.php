<?php 
	$namePage = "checkout-concluido";
	include("header.php"); 
	require_once 'admin/transaction/transactionPedidos.php';
	require_once 'admin/transaction/transactionPedidosxProdutos.php';
	require_once 'admin/transaction/transactionEnderecos.php';
	require_once 'admin/transaction/transactionClientes.php';
	require_once 'admin/transaction/transactionConfig.php';
	require_once 'admin/transaction/transactionTransportadoras.php';
	require_once 'admin/transaction/transactionFretesCorreios.php';
	$pedido 	= fillPedidos($parametros[2]);
	$cliente 	= fillClientes($pedido['CLICODIGO']);
	$endereco 	= fillEnderecosByCliente($pedido['CLICODIGO']);
	$bancario 	= getDadosBancarios();
	switch ($pedido['PEDFORMAPAGAMENTO']) {
		case 0:
			$formaPagamento = "Boleto Itaú";
			break;
		case 1:
			$formaPagamento = "Depósito em conta Itaú";
			break;
	}
?>

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Checkout</h1>
			<ul class="step-by-step">
				<li>
					<div class="itm">
						<div class="number cart"></div>
						<div class="description">Carrinho</div>
					</div><!-- itm -->				
				</li>

				<li>
					<div class="itm">
						<div class="number payment"></div>
						<div class="description">Pagamento</div>
					</div><!-- itm -->
				</li>

				<li>
					<div class="itm isActive">
						<div class="number finish"></div>
						<div class="description">Concluído</div>
					</div><!-- itm -->
				</li>
			</ul><!-- step-by-step -->

			<div class="success table">
				<div class="table-header">
					<div class="ttl ttl-sm-black-2">COMPRA EFETUADA COM SUCESSO</div>
				</div>

				<div class="table-content">
					<div class="order-number">O número do seu pedido é: <b><?= str_pad($pedido['PEDCODIGO'], 11, '0', STR_PAD_LEFT); ?></b></div>
					<div class="order-success">Seu pedido foi realizado com sucesso <br>
					Em breve você receberá um e-mail de confirmação com todas as informações da sua compra.
					</div><!-- secondary-txt -->
				</div>
			</div>

			<table class="table-product table">
				<thead class="header table-header">
					<tr class="lst-table-product">
						<th class="itm-product"><div class="ttl ttl-sm-black-2">PRODUTO</div></th>
						<th class="itm-provider"><div class="ttl ttl-sm-black-2">MARCA</div></th>
						<th class="itm-amount"><div class="ttl ttl-sm-black-2">QUANTIDADE</div></th>
						<th class="itm-value"><div class="ttl ttl-sm-black-2">VALOR UNITÁRIO</div></th>
					</tr>
				</thead>
				<tbody class="content table-content">
					<?php 
						$gridProdutosPedido = gridProdutosPedido($pedido['PEDCODIGO']);
						foreach($gridProdutosPedido['dados'] as $produtoPedido){
							echo "<tr class=\"lst-table-product\">\n
								<td class=\"itm-product\" data-ttl-itm=\"PRODUTO\">{$produtoPedido[PRONOME]}</td>\n
								<td class=\"itm-provider\" data-ttl-itm=\"MARCA\">{$produtoPedido[MARMARCA]}</td>\n
								<td class=\"itm-amount\" data-ttl-itm=\"QUANTIDADE\">{$produtoPedido[PXPQUANTIDADE]}</td>\n
								<td class=\"itm-value\" data-ttl-itm=\"VALOR UNITÁRIO\">".formatar_moeda($produtoPedido['PXPVALOR'])."</td>\n
							</tr>\n";
						}
					?>
				</tbody>
			</table>
			
			<div class="purchase-value">
				<div class="value-subtotal">Subtotal: <?= formatar_moeda($pedido['PEDVALORPRODUTOS']) ?></div>
				<div class="value-freight">+ Frete: <?= formatar_moeda($pedido['PEDVALORFRETE']) ?></div>
				<div class="value-total">TOTAL: <?= formatar_moeda($pedido['PEDVALORFRETE']+$pedido['PEDVALORPRODUTOS']); ?></div>
			</div>

			<div class="payment table">
				<div class="table-header">
					<div class="ttl ttl-sm-black-2">PAGAMENTO</div>
				</div><!-- header-completed-payment-->

				<div class="payment-content table-content">
					<div class="row">
						<div class="half">
							<div>FORMA DE PAGAMENTO: <span><?= $formaPagamento ?></span></div>
						</div>

						<div class="half">
							<?php 
								if ($pedido['PEDFORMAPAGAMENTO'] == 0) {
									echo "<a class=\"btn-md-red btn-print\" target=\"_blank\" href=\"/checkout/boleto/{$parametros[2]}/\">Imprimir Boleto</a>";							
								}
								if ($pedido['PEDFORMAPAGAMENTO'] == 1) {
									echo "<div>Agência: <span>".$bancario['CFGAGENCIA']."</span></div>";
									echo "<div>Conta: <span>".$bancario['CFGCONTA']."</span></div>";
									echo "<div>Razão social: <span>".$bancario['CFGCEDENTE']."</span></div>";
									echo "<div>CNPJ: <span>".$bancario['CFGCNPJ']."</span></div>";
									echo "<div><br></div>";
									echo "<div>Enviar um E-mail com o comprovante de depósito para: <span>".getConfig('CFGEMAILRECEBIMENTO')."</span></div>";
								}
							?>							
						</div>
					</div>
				</div>
			</div><!-- completed-payment -->

			<div class="informations table">
				<div class="table-header">
					<div class="ttl ttl-sm-black-2">INFORMAÇÕES</div>
				</div><!-- header-completed-informations-->

				<div class="table-content">
					<div class="waiting">AGUARDANDO PAGAMENTO</div>

					<div class="row">
						<div class="half">
							<div>Destinatário <span><?= $cliente['CLINOME']; ?></span></div>
							<div>Endereço de entrega <span><?= $endereco['ENDLOGRADOURO'].', '.$endereco['ENDNUMERO'].' - '.$endereco['ENDBAIRRO'].', '.$endereco['CIDDESCRICAO'].' - '.$endereco['ESTSIGLA'].' - CEP:'.$endereco['ENDCEP'] ?></span></div>
							<?php
								switch($pedido['PEDFORMAENVIO']){
									case 'CORREIOS':
										$envio = fillFretesCorreios($pedido['PEDCODIGOENVIO']);
										$formaEnvio = $envio['FCOTIPO'];
										break;
									case 'TRANSPORTADORA':
										$envio = fillTransportadoras($pedido['PEDCODIGOENVIO']);
										$formaEnvio = $envio['TRANOME'];
										break;
									case 'GRATIS':
										$formaEnvio = 'Frete grátis';
										break;
								}
							?>
							<div>Opção de entrega <span><?= $formaEnvio ?></span></div>
							<div>Forma de Pagamento <span><?= $formaPagamento ?></span></div>
						</div>
						<div class="half">
							<ul class="completed-informations-list">
								<div>Valor total <span><?= formatar_moeda($pedido['PEDVALORPRODUTOS']) ?></span></div>
								<div>Frete <span><?= formatar_moeda($pedido['PEDVALORFRETE']) ?></span></div>
								<div>Total da Compra <span><?= formatar_moeda($pedido['PEDVALORFRETE']+$pedido['PEDVALORPRODUTOS']); ?></span></li>
							</ul><!-- completed-informations-list -->
						</div>
					</div>
				</div>
			</div><!-- completed-informations -->
			<div class="btn-store-return">	
				<a class="btn-md-red " href="/home/">Voltar a loja</a>
			</div>
		</div><!-- ctn -->
	</main>

<?php include("footer.php"); ?>