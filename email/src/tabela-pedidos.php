<table class="ttl-red"><tr><td>Compras</td></tr></table>
<table class="table-data" cellpadding="0" cellspacing="0">
	<tr class="ttl">
		<td>Produto</td>
		<td>Quantidade</td>
		<td>Preço unitário</td>
		<td>Preço total</td>
	</tr><!-- ttl -->
	<?php 
        $listProdutos = gridProdutosPedido($pedido);
        foreach($listProdutos['dados'] as $produto):
            $imagem = is_file('admin/uploads/produtos/'.$produto['PROCODIGO'].'/thumbnail/'.$produto['PROIMAGEMPRINCIPAL']) 
                ? 'admin/uploads/produtos/'.$produto['PROCODIGO'].'/thumbnail/'.$produto['PROIMAGEMPRINCIPAL'] 
                : 'admin/uploads/produtos/'.$produto['PROCODIGO'].'/'.$produto['PROIMAGEMPRINCIPAL'];
    ?>
		<tr class="data">
			<td><?= $produto['PRONOME']; ?></td>
			<td><?= $produto['PXPQUANTIDADE']; ?></td>
			<td><?= formatar_moeda($produto['PXPVALOR']) ?></td>
			<td><?= formatar_moeda($produto['PXPVALOR']*$produto['PXPQUANTIDADE']) ?></td>
		</tr><!-- data -->

	<?php endforeach; ?>
	<tr class="data">
		<td colspan="4">
			<b>Arquivos anexados:</b> <br><br>
			<img src="<?= cSUrlSiteEmpresa ?>/email/img/attachment.png"> arquivo-anexado-01.pdf <br>
			<img src="<?= cSUrlSiteEmpresa ?>/email/img/attachment.png"> arquivo-anexado-02.pdf <br>
			<img src="<?= cSUrlSiteEmpresa ?>/email/img/attachment.png"> arquivo-anexado-03.pdf <br>
		</td>
	</tr>
	<tr class="total">
		<td colspan="4">
			<span class="red">Subtotal</span> <?= formatar_moeda($dados['PEDVALORPRODUTOS']) ?><br>
			<span class="red">+ Frete</span> <?= formatar_moeda($dados['PEDVALORFRETE']) ?><br>
			<span class="total-final"><span class="red">Total</span> 
			<?= formatar_moeda($dados['PEDVALORPRODUTOS']+$dados['PEDVALORFRETE']) ?></span>
		</td>
	</tr><!-- total -->
</table><!-- table-data -->
<br>

<table class="ttl-red"><tr><td>Dados de entrega</td></tr></table>
<table class="table-data" cellpadding="0" cellspacing="0">
	<tr class="ttl">
		<td>Nome</td>
		<td>Endereço</td>
		<td>CEP</td>
		<td>Prazo</td>
	</tr><!-- ttl -->

	<tr class="data">
		<td><?= $cliente['CLINOME']; ?></td>
		<td><?= $endereco['ENDLOGRADOURO']; ?>, <?= $endereco['ENDNUMERO']; ?> - <?= $endereco['ENDBAIRRO']; ?>, 
		<?= $endereco['CIDDESCRICAO']; ?> - <?= $endereco['ESTSIGLA']; ?></td>
		<td><?= $endereco['ENDCEP']; ?></td>
		<td>Até <?= $dados['PEDPRAZOENTREGA'] ?> Dias úteis Após o embarque.</td>
	</tr><!-- data -->
</table><!-- table-data -->
<br>

<table class="ttl-red"><tr><td>Forma de pagamento</td></tr></table>
<table class="table-data" cellpadding="0" cellspacing="0">
	<tr class="data">
		<td><img src="<?= cSUrlSiteEmpresa ?>/email/img/itau.png" class="logo-banco"></td>
		<td><?= $formaPagamento; ?></td>
	</tr><!-- data -->
</table><!-- table-data -->
<br>