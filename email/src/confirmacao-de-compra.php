<?php
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionEnderecos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionClientes.php";

	function emailPedidoRealizado($pedido){
	    $dados      = fillPedidos($pedido);
	    $endereco   = fillEnderecosByCliente($dados['CLICODIGO']);
	    $cliente    = fillClientes($dados['CLICODIGO']);

	    $formaPagamento = ($dados['PEDFORMAPAGAMENTO'] == 0) ? 'Boleto Itaú' : 'Depósito em conta Itaú' ;
	    ob_start();
?>
		<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

	<table class="ctn">

		<tr class="header">
			<td>
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png">
			</td>
			<td class="infos">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png">
			</td>
		</tr><!-- header -->

			<tr class="content">
				<td colspan="2">
					<div class="txt">
						<img src="<?= cSUrlSiteEmpresa ?>/email/img/confirmacao-de-compra.png" class="pedido-status">
						<br><br>
						Olá sr(a). <?= $cliente['CLINOME']; ?>, recebemos a confirmação de que você já recebeu o seu pedido.
						<br><br>
						Obrigado por comprar conosco. <br>

						Qualquer dúvida estamos a disposição.
						<br><br>
					</div><!-- txt -->
					<table class="ttl-red"><tr><td>Compras</td></tr></table>
					<table class="table-data" cellpadding="0" cellspacing="0">
						<tr class="ttl">
							<td>Produto</td>
							<td>Quantidade</td>
							<td>Preço unitário</td>
							<td>Preço total</td>
						</tr><!-- ttl -->
						<?php 
	                        $listProdutos = gridProdutosPedido($pedido);
	                        foreach($listProdutos['dados'] as $produto):
	                            $imagem = is_file('admin/uploads/produtos/'.$produto['PROCODIGO'].'/thumbnail/'.$produto['PROIMAGEMPRINCIPAL']) 
	                                ? 'admin/uploads/produtos/'.$produto['PROCODIGO'].'/thumbnail/'.$produto['PROIMAGEMPRINCIPAL'] 
	                                : 'admin/uploads/produtos/'.$produto['PROCODIGO'].'/'.$produto['PROIMAGEMPRINCIPAL'];
	                    ?>
						<tr class="data">
							<td><?= $produto['PRONOME']; ?></td>
							<td><?= $produto['PXPQUANTIDADE']; ?></td>
							<td><?= formatar_moeda($produto['PXPVALOR']) ?></td>
							<td><?= formatar_moeda($produto['PXPVALOR']*$produto['PXPQUANTIDADE']) ?></td>
						</tr><!-- data -->
						<?php endforeach; ?>
						<tr class="total">
							<td colspan="4">
								<span class="red">Subtotal</span> <?= formatar_moeda($dados['PEDVALORPRODUTOS']) ?><br>
								<span class="red">+ Frete</span> <?= formatar_moeda($dados['PEDVALORFRETE']) ?><br>
								<span class="total-final"><span class="red">Total</span> 
								<?= formatar_moeda($dados['PEDVALORPRODUTOS']+$dados['PEDVALORFRETE']) ?></span>
							</td>
						</tr><!-- total -->
					</table><!-- table-data -->
					<br>

					<table class="ttl-red"><tr><td>Dados de entrega</td></tr></table>
					<table class="table-data" cellpadding="0" cellspacing="0">
						<tr class="ttl">
							<td>Nome</td>
							<td>Endereço</td>
							<td>CEP</td>
							<td>Prazo</td>
						</tr><!-- ttl -->

						<tr class="data">
							<td><?= $cliente['CLINOME']; ?></td>
							<td><?= $endereco['ENDLOGRADOURO']; ?>, <?= $endereco['ENDNUMERO']; ?> - <?= $endereco['ENDBAIRRO']; ?>, 
							<?= $endereco['CIDDESCRICAO']; ?> - <?= $endereco['ESTSIGLA']; ?></td>
							<td><?= $endereco['ENDCEP']; ?></td>
							<td>Até <?= $dados['PEDPRAZOENTREGA'] ?> Dias úteis Após o embarque.</td>
						</tr><!-- data -->
					</table><!-- table-data -->
					<br>

					<table class="ttl-red"><tr><td>Forma de pagamento</td></tr></table>
					<table class="table-data" cellpadding="0" cellspacing="0">
						<tr class="data">
							<td><img src="<?= cSUrlSiteEmpresa ?>/email/img/itau.png"></td>
							<td><?= $formaPagamento; ?></td>
						</tr><!-- data -->
					</table><!-- table-data -->
					<br>
					
				</td>
			</tr><!-- content -->

			</table><!-- ctn -->

</body>
</html>
<?php
	    $informacoes = ob_get_contents();
        ob_get_clean();
        return $informacoes;
	}
?>