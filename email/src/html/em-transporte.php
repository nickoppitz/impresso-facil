<?php
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionEnderecos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionClientes.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxProdutos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxAnexos.php";

    function enviarEmaiPedidoEmTransporte($pedido){
	    //$pedido                  = $dados['vIPEDCODIGO'];

	    $dados      = fillPedidos($pedido);
	    $endereco   = fillEnderecosByCliente($dados['CLICODIGO']);
	    $cliente    = fillClientes($dados['CLICODIGO']);

	    $formaPagamento = ($dados['PEDFORMAPAGAMENTO'] == 0) ? 'Boleto Itaú' : 'Depósito em conta Itaú' ;
	        
	    $formaEnvio              = $dados['PEDFORMAENVIO'];
	    $codigoEnvio             = $dados['vIPEDCODIGOENVIO'];
	    $codigoRastreamento      = $dados['vSPEDCODIGORASTREAMENTO'];
	    $observacoesRastreamento = $dados['vSPEDOBSERVACOESENVIO'];
        ob_start();
?>

		@@include("header.php")

			<tr class="content">
				<td colspan="2">
					<div class="txt">
						<img src="<?= cSUrlSiteEmpresa ?>/email/img/em-transporte.png" class="pedido-status">
						<br><br>
						Olá Sr(a). <strong><?= $cliente['CLINOME']; ?></strong> seu pedido já foi expedido, abaixo segue informações para rastreamento de sua entrega.<br>
						<strong>Nº do pedido: </strong><?= str_pad($pedido, 11, '0', STR_PAD_LEFT); ?>.
						<br>
						<strong>Forma de Envio: </strong><?= $formaEnvio; ?>
						<br>
						<strong>Rastreamento: </strong>
						<?php if($formaEnvio != 'CORREIOS'): ?>
							<?= $observacoesRastreamento ?>
						<?php endif; ?>

						<?php if ($formaEnvio == 'CORREIOS' && !empty($codigoRastreamento)): ?>							
							Para acompanhar o seu pedido <a href="http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=<?= $codigoRastreamento ?>" target="_blank" class="veja-mais">clique aqui</a>.

						<?php endif; ?>
						<br><br>
						Caso tenha alguma dúvida estamos à disposição através de nossos canais de atendimento.
					</div><!-- txt -->

					@@include("tabela-pedidos.php")
					
				</td>
			</tr><!-- content -->

		@@include("footer.php")

<?php
        $informacoes = ob_get_contents();
        ob_get_clean();
        return $informacoes;
    }
?>