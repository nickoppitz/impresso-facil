<?php
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionEnderecos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionClientes.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxProdutos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxAnexos.php";

	function enviarEmailCancelamentoLoja($pedido){
	    $dados      = fillPedidos($pedido);
	    $endereco   = fillEnderecosByCliente($dados['CLICODIGO']);
	    $cliente    = fillClientes($dados['CLICODIGO']);

	    $formaPagamento = ($dados['PEDFORMAPAGAMENTO'] == 0) ? 'Boleto Itaú' : 'Depósito em conta Itaú' ;
	    ob_start();
?>
		@@include("header.php")

			<tr class="content">
				<td colspan="2">
					<div class="txt">
						<div class="ttl-black">Cancelamento de compra</div>
						Olá sr(a). Administrador, o seu cliente Sr(a). <strong><?= $cliente['CLINOME']; ?></strong> cancelou o pedido Nº <strong><?= str_pad($pedido, 11, '0', STR_PAD_LEFT); ?></strong>.					
					</div><!-- txt -->

					@@include("tabela-pedidos.php")
				
				</td>
			</tr><!-- content -->

		@@include("footer.php")
<?php
	    $informacoes = ob_get_contents();

	    ob_get_clean();
	    return $informacoes;
	}
?>