<?php
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionEnderecos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionClientes.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxProdutos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxAnexos.php";

    function emailPedidoRealizado($pedido){
        $dados      = fillPedidos($pedido);
        $endereco   = fillEnderecosByCliente($dados['CLICODIGO']);
        $cliente    = fillClientes($dados['CLICODIGO']);

        $formaPagamento = ($dados['PEDFORMAPAGAMENTO'] == 0) ? 'Boleto Itaú' : 'Depósito em conta Itaú' ;
        ob_start();
?>
        @@include("header.php")
             <tr class="content">
                <td colspan="2">
                    <div class="txt">
                        <img src="<?= cSUrlSiteEmpresa ?>/email/img/pedido-realizado.png" class="pedido-status">
                        <br><br>
                        <div class="ctn">
                            <div class="order-number">Recebemimento do pedido Nº 
                                <span class="number" style="color:#EA1F26;"><?= str_pad($dados['PEDCODIGO'], 11, '0', STR_PAD_LEFT); ?></span>
                            </div>
                            <a class="see-detail" href="<?= cSUrlSiteEmpresa ?>/meus-pedidos/<?= str_pad($dados['PEDCODIGO'], 11, '0', STR_PAD_LEFT); ?>/" target="_blank">
                                    Veja mais detalhes do pedido
                            </a>
                            <hr>
                            <div class="order-status">
                                <span class="number">1</span>
                                <b>1 - Pedido realizado</b>
    						</div>
                        </div>
                        <div class="delivery">
                            <div class="ctn">
                                <div class="delivery-ttl">
                                    <img src="<?= cSUrlSiteEmpresa ?>/email/img/delivery.png"> Prazo e entrega:
                                </div>
                                Até <?= $dados['PEDPRAZOENTREGA'] ?> Dias úteis Após o embarque.
                            </div>
                        </div>
<?php
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionEnderecos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionClientes.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxProdutos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxAnexos.php";

    function emailPedidoRealizado($pedido){
        $dados      = fillPedidos($pedido);
        $endereco   = fillEnderecosByCliente($dados['CLICODIGO']);
        $cliente    = fillClientes($dados['CLICODIGO']);

        $formaPagamento = ($dados['PEDFORMAPAGAMENTO'] == 0) ? 'Boleto Itaú' : 'Depósito em conta Itaú' ;
        ob_start();
?>
        @@include("header.php")

            <tr class="content">
                <td colspan="2">
                    <div class="txt">
                        <img src="<?= cSUrlSiteEmpresa ?>/email/img/pedido-realizado.png" class="pedido-status">
                        <br><br>
                        Olá sr(a). Administrador, o seu cliente Sr(a). <strong><?= $cliente['CLINOME']; ?></strong> realizou uma compra na loja.<br>
                        <strong>Nº do pedido: </strong><?= str_pad($pedido, 11, '0', STR_PAD_LEFT); ?>.<br>
                    </div><!-- txt -->
                    
                    @@include("tabela-pedidos.php")
                </td>
            </tr><!-- content -->

        @@include("footer.php")
<?php
        $informacoes = ob_get_contents();

        ob_get_clean();

        return array(
            'conteudo' => $informacoes,
            'email' => $cliente['CLIEMAIL']
        );
    }
?>