<?php 
    function minhaSenha($nome, $senha){ 
        ob_start();
?>
        @@include("header.php")

            <tr class="content">
                <td colspan="2">
                    <div class="txt">
                        <div class="ttl-black">Solicitação de nova senha</div>
                        Olá Sr(a). <strong><?= $nome ?> sua senha foi recuperada com sucesso:<br><br>
                        <strong>SENHA: </strong><?= $senha ?> .<br>
                        Caso tenha alguma dúvida estamos à disposição através de nossos canais de atendimento.
                    </div><!-- txt -->
                </td>
            </tr><!-- content -->

        @@include("footer.php")
<?php 
        $texto = ob_get_contents();

        ob_get_clean();
        return $texto;
    }
?>