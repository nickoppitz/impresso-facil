<?php    
    function novoCadastroLoja($nome, $data){
        ob_start();
?>
		@@include("header.php")

			<tr class="content">
				<td colspan="2">
					<div class="txt">
						<div class="ttl-black">Novo cadastro em sua loja</div>					

						Olá sr(a). Administrador, foi efetuado o novo cadastro de <strong><?= $nome ?></strong> em sua loja em <?= formatar_data_hora($data)?>.
					</div><!-- txt -->
				</td>
			</tr><!-- content -->
		@@include("footer.php")
<?php
    	$texto = ob_get_contents();

	    ob_get_clean();
	    return $texto;
    }
?>