<?php 
  	function dadosAcesso($nome, $cpfCnpj, $senha){   	  	
  		ob_start();
?>
		@@include("header.php")

			<tr class="content">
				<td colspan="2">
					<div class="txt">
						<div class="ttl-black">Novo cadastro</div>
						Bem-vindo sr(a). <?= $nome ?>.
						<br><br>
						Sua conta está pronta para você começar suas compras na Loja Algo Mais. Seguem abaixo seus dados para acesso:
						<br><br>
						<b>Cpf/Cnpj:</b> <?= $cpfCnpj ?><br>
						<b>Senha:</b> <?= $senha ?>						
					</div><!-- txt -->
				</td>
			</tr><!-- content -->
			
		@@include("footer.php")
<?php

		$texto = ob_get_contents();
		ob_get_clean();
		return $texto;
	}
?>