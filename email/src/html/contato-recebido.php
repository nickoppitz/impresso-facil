<?php 
  	function fillEmail($dadosEmail){   	  	
  		ob_start();
?>
		@@include("header.php")
			<tr class="content">
				<td colspan="2">
					<div class="txt">
						Olá Sr(a). Administrador, recebemos um novo contato via website
					</div><!-- txt -->
					<table class="ttl-red"><tr><td>Contato Recebido</td></tr></table>
					<table class="table-data">					
					<?php foreach($dadosEmail['fields'] as $key => $value): ?>
						<tr class="data">
							<td><b><?= $key; ?>: </b></td>
							<td><?= $value; ?></td>
						</tr><!-- data -->
					<?php endforeach; ?>
					</table><!-- table-data -->
				</td>
			</tr><!-- content -->
		@@include("footer.php")
<?php

		$texto = ob_get_contents();
		ob_get_clean();
		return $texto;
	}
?>