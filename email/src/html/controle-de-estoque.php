<?php
    function emailEstoqueBaixo($produto){
        $dados = fillProdutos($produto);
    	ob_start();
?>
		@@include("header.php")

			<tr class="content">
				<td colspan="2">
					<div class="txt">
						<div class="ttl-black">Controle de estoque</div>

						Olá sr(a). <strong>Administrador</strong>, restam apenas <b><?= $dados['PROESTOQUE'] ?></b> items do produto <b><?= $dados['PRONOME'] ?></b>.
						
					</div><!-- txt -->
				</td>
			</tr><!-- content -->
			
		@@include("footer.php")
<?php
	    $texto = ob_get_contents();

	    ob_get_clean();
	    return $texto;
    }
?>
