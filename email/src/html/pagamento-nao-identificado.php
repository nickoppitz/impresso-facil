<?php   
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionEnderecos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionClientes.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxProdutos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxAnexos.php";

    function pagamentoNaoIdentificado($pedido, $nome){
    	$dados      = fillPedidos($pedido);
	    $endereco   = fillEnderecosByCliente($dados['CLICODIGO']);
	    $cliente    = fillClientes($dados['CLICODIGO']);

	    $formaPagamento = ($dados['PEDFORMAPAGAMENTO'] == 0) ? 'Boleto Itaú' : 'Depósito em conta Itaú' ;
        ob_start();
?>
		@@include("header.php")

			<tr class="content">

				<td colspan="2">
					<div class="txt">
						<img src="<?= cSUrlSiteEmpresa ?>/email/img/pedido-realizado.png" class="pedido-status">

						<br><br>					
						<div class="ttl-black">Pagamento não identificado</div>
						Olá Sr(a). <strong><?= $nome ?></strong> , ainda não identificamos o pagamento do seu pedido Nº <strong><?= str_pad($pedido, 11, '0', STR_PAD_LEFT); ?></strong>.
					</div><!-- txt -->

					@@include("tabela-pedidos.php")
				</td>
			</tr><!-- content -->

		@@include("footer.php")

<?php
    	$texto = ob_get_contents();

	    ob_get_clean();
	    return $texto;
    }
    foreach(getPedidosNaoPagos() as $pedido){
      envioEmail('PAGAMENTO NÃO IDENTIFICADO - GRÁFICA ALGO MAIS', pagamentoNaoIdentificado($pedido['PEDCODIGO'], $pedido['NOMECLIENTE']), $pedido['CLIEMAIL']);
    }
?>