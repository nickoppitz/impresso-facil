<?php 
    require_once('../admin/includes/constantes.php');
    require_once '../admin/libs/phpmailer/email.php';    

    $amigoQueIndicou    = ($_POST['nome'] != '') ? filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING) :$_SESSION['CLINOME'];
    $nomeAmigoIndicado  = filter_input(INPUT_POST, 'nomeAmigo', FILTER_SANITIZE_STRING);
    $emailAmigoIndicado = filter_input(INPUT_POST, 'emailAmigo', FILTER_SANITIZE_EMAIL);
    $url                = filter_input(INPUT_POST, 'url', FILTER_SANITIZE_URL);
    $mensagem           = $_POST['mensagem'];
    $titulo             = $amigoQueIndicou." indicou um produto para você!";
    
    ob_start();
?>
	@@include("header.php")
		<tr class="content">
			<td colspan="2">
				<div class="txt">
					<div class="ttl-black"><?= $amigoQueIndicou ?>, indicou um produto para você</div>					

					Olá <?= $nomeAmigoIndicado ?>, <?= $amigoQueIndicou ?> indicou um produto para você na Loja Algo Mais!
					<br><br>

					<b>E lhe deixou a seguinte mensagem: </b>
					<br><?= $mensagem; ?>
					<br><br>

					Para conferir <a href="<?= $url ?>" target="_blank" class="veja-mais">clique aqui se já for cadastrado</a> ou 
					<a href="<?= cSUrlSiteEmpresa ?>/cadastro/" target="_blank" class="veja-mais">clique aqui se ainda não for cadastrado</b>.		
				</div><!-- txt -->
			</td>
		</tr><!-- content -->

	@@include("footer.php")
<?php
    $dados = ob_get_contents();

    ob_get_clean();

    echo json_encode(envioEmail($titulo, $dados, $emailAmigoIndicado));
?>