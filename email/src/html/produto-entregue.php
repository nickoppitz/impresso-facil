<?php
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionEnderecos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionClientes.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxProdutos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxAnexos.php";

	function enviarEmailConfirmacaoEnvio($pedido){
	    $dados      = fillPedidos($pedido);
	    $endereco   = fillEnderecosByCliente($dados['CLICODIGO']);
	    $cliente    = fillClientes($dados['CLICODIGO']);

	    $formaPagamento = ($dados['PEDFORMAPAGAMENTO'] == 0) ? 'Boleto Itaú' : 'Depósito em conta Itaú' ;
	    ob_start();
?>
		@@include("header.php")

			<tr class="content">
				<td colspan="2">
					<div class="txt">
						<img src="<?= cSUrlSiteEmpresa ?>/email/img/confirmacao-de-compra.png" class="pedido-status">
						<br><br>
						Olá Sr(a). <strong><?= $cliente['CLINOME']; ?></strong> seu pedido foi entregue com sucesso.<br>
						Trabalhamos para proporcionar uma ótima experiência na sua compra.<br>						 
						Caso tenha identificado alguma desconformidade com seu pedido entre em contato conosco, estamos à disposição através de nossos canais de atendimento.
					</div><!-- txt -->
					
					@@include("tabela-pedidos.php")
				</td>
			</tr><!-- content -->

		@@include("footer.php")
<?php
	    $informacoes = ob_get_contents();
        ob_get_clean();
        return $informacoes;
	}
?>