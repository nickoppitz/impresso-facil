<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title></title>
		<link rel="stylesheet" href="../../css/client-email.css">
	</head>
	<body>
		<table width="100%">
			<thead>
				<tr>
					<th class="header">
						<div class="ctn">
							<div class="row">
								<div class="logo">
									<img src="../img/logo.png" width="117">
								</div>
								<div class="info">
									<a class="url-site" href="#" target="_blank">algomais.com.br</a>
									<div class="phone"><img src="../img/phone.png"> (51) 3222-2339</div>
								</div>
							</div>
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<div class="ctn">							
							<div class="order-number">Recebemos o seu pedido Nº <span class="number">9</span></div>
							<a class="see-detail" href="#" target="_blank">Veja mais detalhes do seu pedido</a>
							<hr>
							<div class="order-status">
								<span class="number">5</span>
								Confirmação de envio
							</div>
							<div class="comments">
								<p>
									Olá sr(a). Fulano da Silva Sauro, recebemos a confirmação de que você já recebeu o seu pedido. <br>
									Obrigado por comprar conosco. <br>
									Qualquer dúvida estamos a disposição.
								</p>
							</div>
						</div>		
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td>
						<div class="footer">
							<div class="ctn">
								<div class="footer-ttl">Atendimento ao consumidor</div>
								<p>(51) 3222-2339</p>
								<p>atendimento@algomais.com.br</p>
							</div>
						</div>
					</td>
				</tr>
			</tfoot>
		</table>
	</body>
</html>