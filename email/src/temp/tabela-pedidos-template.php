<table class="ttl-red"><tr><td>Compras</td></tr></table>

<table class="table-data">
	<tr class="ttl">
		<td>Produto</td>
		<td>Qtd</td>
		<td>Status</td>
		<td>Preço</td>
	</tr><!-- ttl -->

	<tr class="data">
		<td>Agenda Casa Brasileira 2017</td>
		<td>30</td>
		<td>Pedido realizado</td>
		<td>R$ 55,00</td>
	</tr><!-- data -->

	<tr class="data">
		<td>Livro 30x30</td>
		<td>3</td>
		<td>Pedido realizado</td>
		<td>R$ 55,00</td>
	</tr><!-- data -->

	<tr class="total">
		<td colspan="44">
			<div class="subtotal"><span class="red">Subtotal</span> R$ 75,00</div>
			<div class="frete"><span class="red">+ Frete</span> R$ 19,28</div>
			<div class="total-final"><span class="red">Total</span> R$ 94,28</div>
		</td>
	</tr><!-- total -->
</table><!-- table-data -->

<br>

<table class="ttl-red"><tr><td>Dados de entrega</td></tr></table>

<table class="table-data">
	<tr class="ttl">
		<td>Nome</td>
		<td>Endereço</td>
		<td>Prazo</td>
	</tr><!-- ttl -->

	<tr class="data">
		<td>Nome Do Cliente</td>
		<td>Avenida Farrapos, 146 - Floresta, Porto Alegre - RS 
CEP: 90220-000</td>
		<td>Até 5 Dias úteis Após o embarque.</td>
	</tr><!-- data -->
</table><!-- table-data -->

<br>

<table class="ttl-red"><tr><td>Forma de pagamento</td></tr></table>

<table class="table-data">
	<tr class="data">
		<td><img src="<?= cSUrlSiteEmpresa ?>/email/img/banco-do-brasil.jpg"></td>
		<td>Depósito do Banco do Brasil</td>
	</tr><!-- data -->
</table><!-- table-data -->

<br>