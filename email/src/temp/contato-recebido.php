<?php 
  	function fillEmail($dadosEmail){   	  	
  		ob_start();
?>
		<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

	<table class="ctn">

		<tr class="header">
			<td>
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png">
			</td>
			<td class="infos">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png">
			</td>
		</tr><!-- header -->
			<tr class="content">
				<td colspan="2">
					<div class="txt">
						Olá Sr(a). Administrador, recebemos um novo contato via website
					</div><!-- txt -->
					<table class="ttl-red"><tr><td>Contato Recebido</td></tr></table>
					<table class="table-data">					
					<?php foreach($dadosEmail['fields'] as $key => $value): ?>
						<tr class="data">
							<td><b><?= $key; ?>: </b></td>
							<td><?= $value; ?></td>
						</tr><!-- data -->
					<?php endforeach; ?>
					</table><!-- table-data -->
				</td>
			</tr><!-- content -->
			</table><!-- ctn -->

</body>
</html>
<?php

		$texto = ob_get_contents();
		ob_get_clean();
		return $texto;
	}
?>