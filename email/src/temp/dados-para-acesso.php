<?php 
  	function dadosAcesso($nome, $cpfCnpj, $senha){   	  	
  		ob_start();
?>
		<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

	<table class="ctn">

		<tr class="header">
			<td>
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png">
			</td>
			<td class="infos">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png">
			</td>
		</tr><!-- header -->

			<tr class="content">
				<td colspan="2">
					<div class="txt">
						<div class="ttl-black">Novo cadastro</div>
						Bem-vindo sr(a). <?= $nome ?>.
						<br><br>
						Sua conta está pronta para você começar suas compras na Loja Algo Mais. Seguem abaixo seus dados para acesso:
						<br><br>
						<b>Cpf/Cnpj:</b> <?= $cpfCnpj ?><br>
						<b>Senha:</b> <?= $senha ?>						
					</div><!-- txt -->
				</td>
			</tr><!-- content -->
			
			</table><!-- ctn -->

</body>
</html>
<?php

		$texto = ob_get_contents();
		ob_get_clean();
		return $texto;
	}
?>