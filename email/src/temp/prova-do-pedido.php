<?php    
	function enviarEmailProva($pedido, $cliente){
    	ob_start();
?>
		<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

	<table class="ctn">

		<tr class="header">
			<td>
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png">
			</td>
			<td class="infos">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png">
			</td>
		</tr><!-- header -->

		<tr class="content">
			<td colspan="2">
				<div class="txt">
					Olá Sr(a). <strong><?= $cliente; ?></strong> <a href="<?= cSUrlSiteEmpresa; ?>/meus-pedidos/<?= str_pad($pedido, 11, '0', STR_PAD_LEFT); ?>/ " class="clique-aqui">clique aqui</a> para visualizar as provas virtuais do seu pedido.<br><br>

					Favor verificar e autorizar o inicio da produção.<br><br>
					ATENÇÃO: O prazo de entrega passa a contar a partir da aprovação da PROVA VIRTUAL.<br><br>

					Caso tenha alguma dúvida estaremos à disposição através de nossos canais de atendimento.<br>
				</div><!-- txt -->
			</td>
		</tr><!-- content -->

			</table><!-- ctn -->

</body>
</html>
<?php
		$dados = ob_get_contents();
        ob_get_clean();
        return $dados;
	}
?>