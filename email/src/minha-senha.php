<?php 
    function minhaSenha($nome, $senha){ 
        ob_start();
?>
        <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

	<table class="ctn">

		<tr class="header">
			<td>
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png">
			</td>
			<td class="infos">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png">
			</td>
		</tr><!-- header -->

            <tr class="content">
                <td colspan="2">
                    <div class="txt">
                        <div class="ttl-black">Solicitação de nova senha</div>                  

                        Olá <?= $nome ?>, Estamos enviando sua senha conforme solicitado.
                        <br><br>

                        <b>Sua nova senha é:</b> <?= $senha ?>
                    </div><!-- txt -->
                </td>
            </tr><!-- content -->

        	</table><!-- ctn -->

</body>
</html>
<?php 
        $texto = ob_get_contents();

        ob_get_clean();
        return $texto;
    }
?>