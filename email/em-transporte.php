<?php
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionEnderecos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionClientes.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxProdutos.php";
    require_once "{$_SERVER[DOCUMENT_ROOT]}/admin/transaction/transactionPedidosxAnexos.php";

    function enviarEmaiPedidoEmTransporte($pedido){
	    //$pedido                  = $dados['vIPEDCODIGO'];

	    $dados      = fillPedidos($pedido);
	    $endereco   = fillEnderecosByCliente($dados['CLICODIGO']);
	    $cliente    = fillClientes($dados['CLICODIGO']);

	    $formaPagamento = ($dados['PEDFORMAPAGAMENTO'] == 0) ? 'Boleto Itaú' : 'Depósito em conta Itaú' ;
	        
	    $formaEnvio              = $dados['PEDFORMAENVIO'];
	    $codigoEnvio             = $dados['vIPEDCODIGOENVIO'];
	    $codigoRastreamento      = $dados['vSPEDCODIGORASTREAMENTO'];
	    $observacoesRastreamento = $dados['vSPEDOBSERVACOESENVIO'];
        ob_start();
?>

		<!DOCTYPE html>
<html style="font-family: Verdana;">
<head style="font-family: Verdana;">
	<meta charset="utf-8" style="font-family: Verdana;">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" style="font-family: Verdana;">
	<title style="font-family: Verdana;"></title>
	
</head>
<body style="font-family: Verdana; text-align: center;">

	<table class="ctn" style="border-spacing: 0; color: #545454; font-family: Verdana; margin: auto; text-align: left; width: 570px;">

		<tr class="header" style="font-family: Verdana;">
			<td style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; padding: 20px 0;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png" style="font-family: Verdana;">
			</td>
			<td class="infos" style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; line-height: 25px; padding: 20px 0; text-align: right;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png" style="font-family: Verdana;">
			</td>
		</tr><!-- header -->

			<tr class="content" style="font-family: Verdana;">
				<td colspan="2" style="font-family: Verdana; padding: 20px 0;">
					<div class="txt" style="display: block; font-family: Verdana; line-height: 25px; margin: 40px 0; text-align: left;">
						<img src="<?= cSUrlSiteEmpresa ?>/email/img/em-transporte.png" class="pedido-status" style="display: block; font-family: Verdana; margin: 0 auto 40px;">
						<br style="font-family: Verdana;"><br style="font-family: Verdana;">
						Olá Sr(a). <strong style="font-family: Verdana;"><?= $cliente['CLINOME']; ?></strong> seu pedido já foi expedido, abaixo segue informações para rastreamento de sua entrega.<br style="font-family: Verdana;">
						<strong style="font-family: Verdana;">Nº do pedido: </strong><?= str_pad($pedido, 11, '0', STR_PAD_LEFT); ?>.
						<br style="font-family: Verdana;">
						<strong style="font-family: Verdana;">Forma de Envio: </strong><?= $formaEnvio; ?>
						<br style="font-family: Verdana;">
						<strong style="font-family: Verdana;">Rastreamento: </strong>
						<?php if($formaEnvio != 'CORREIOS'): ?>
							<?= $observacoesRastreamento ?>
						<?php endif; ?>

						<?php if ($formaEnvio == 'CORREIOS' && !empty($codigoRastreamento)): ?>							
							Para acompanhar o seu pedido <a href="http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=<?= $codigoRastreamento ?>" target="_blank" class="veja-mais" style="color: #0676B7; font-family: Verdana;">clique aqui</a>.

						<?php endif; ?>
						<br style="font-family: Verdana;"><br style="font-family: Verdana;">
						Caso tenha alguma dúvida estamos à disposição através de nossos canais de atendimento.
					</div><!-- txt -->

						<table class="ttl-red" style="background-color: #EA2026; color: #fff; font-family: Verdana; font-size: 16px; font-weight: bold; padding: 10px; width: 100%;"><tr style="font-family: Verdana;"><td style="font-family: Verdana;">Compras</td></tr></table>
	<table class="table-data" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; font-family: Verdana; margin-bottom: 40px; width: 100%;">
		<tr class="ttl" style="font-family: Verdana;">
			<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Produto</td>
			<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Quantidade</td>
			<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Preço unitário</td>
			<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Preço total</td>
		</tr><!-- ttl -->
		<?php 
	        $listProdutos = gridProdutosPedido($pedido);
	        foreach($listProdutos['dados'] as $produto):
	            $imagem = is_file('admin/uploads/produtos/'.$produto['PROCODIGO'].'/thumbnail/'.$produto['PROIMAGEMPRINCIPAL']) 
	                ? 'admin/uploads/produtos/'.$produto['PROCODIGO'].'/thumbnail/'.$produto['PROIMAGEMPRINCIPAL'] 
	                : 'admin/uploads/produtos/'.$produto['PROCODIGO'].'/'.$produto['PROIMAGEMPRINCIPAL'];
	    ?>
			<tr class="data" style="font-family: Verdana;">
				<td class="nome-produto" style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px 2px; width: 201px;">
					<table style="font-family: Verdana;">
						<tr style="font-family: Verdana;">
							<td style="font-family: Verdana;">
								<img src="<?= cSUrlSiteEmpresa ?>/<?= $imagem; ?>" class="pic" width="100px" style="font-family: Verdana;">
							</td>
							<td style="font-family: Verdana;">&nbsp; <?= $produto['PRONOME']; ?></td>
						</tr>
					</table>
				</td>
				<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><?= $produto['PXPQUANTIDADE']; ?></td>
				<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><?= formatar_moeda($produto['PXPVALOR']); ?></td>
				<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><?= formatar_moeda($produto['PXPVALOR']*$produto['PXPQUANTIDADE']); ?></td>
			</tr><!-- data -->
		<?php 
			$listAnexos = gridPedidoAnexos($pedido, $produto['PROCODIGO']);
			$k = 0;
			foreach ($listAnexos['dados'] as $anexo):
				if ($k == 0) {
		?>
    				<tr class="data" style="font-family: Verdana;">
    					<td colspan="4" style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">
    						<b style="font-family: Verdana;">&nbsp; Arquivos anexados: </b><br style="font-family: Verdana;">
    	<?php   } 	?>	
    						&nbsp; <img src="<?= cSUrlSiteEmpresa ?>/email/img/attachment.png" class="icon-attachment" style="font-family: Verdana; margin-right: 5px;">&nbsp; <?= $anexo['PXAANEXO'];?><br style="font-family: Verdana;">
		<?php  $k++;
			endforeach;
				if ($k > 0) {
		?>			
    					</td>
    				</tr>
    	<?php
    			}
			endforeach;
		?>
		<tr class="total" style="font-family: Verdana;">
			<td colspan="4" style="color: #333; font-family: Verdana; font-size: 12px; line-height: 20px; padding-top: 10px; text-align: right;">
				<span class="red" style="color: #EA2026; font-family: Verdana;">Subtotal</span> <?= formatar_moeda($dados['PEDVALORPRODUTOS']) ?><br style="font-family: Verdana;">
				<span class="red" style="color: #EA2026; font-family: Verdana;">+ Frete</span> <?= formatar_moeda($dados['PEDVALORFRETE']) ?><br style="font-family: Verdana;">
				<span class="total-final" style="font-family: Verdana; font-size: 16px;"><span class="red" style="color: #EA2026; font-family: Verdana;">Total</span> 
				<?= formatar_moeda($dados['PEDVALORPRODUTOS']+$dados['PEDVALORFRETE']) ?></span>
			</td>
		</tr><!-- total -->
	</table><!-- table-data -->
	<br style="font-family: Verdana;">

	<table class="ttl-red" style="background-color: #EA2026; color: #fff; font-family: Verdana; font-size: 16px; font-weight: bold; padding: 10px; width: 100%;"><tr style="font-family: Verdana;"><td style="font-family: Verdana;">Dados de entrega</td></tr></table>
	<table class="table-data" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; font-family: Verdana; margin-bottom: 40px; width: 100%;">
		<tr class="ttl" style="font-family: Verdana;">
			<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Nome</td>
			<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Endereço</td>
			<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">CEP</td>
			<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Prazo</td>
		</tr><!-- ttl -->

		<tr class="data" style="font-family: Verdana;">
			<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><?= $cliente['CLINOME']; ?></td>
			<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><?= $endereco['ENDLOGRADOURO']; ?>, <?= $endereco['ENDNUMERO']; ?> - <?= $endereco['ENDBAIRRO']; ?>, 
			<?= $endereco['CIDDESCRICAO']; ?> - <?= $endereco['ESTSIGLA']; ?></td>
			<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><?= $endereco['ENDCEP']; ?></td>
			<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Até <?= $dados['PEDPRAZOENTREGA'] ?> Dias úteis Após o embarque.</td>
		</tr><!-- data -->
	</table><!-- table-data -->
	<br style="font-family: Verdana;">

	<table class="ttl-red" style="background-color: #EA2026; color: #fff; font-family: Verdana; font-size: 16px; font-weight: bold; padding: 10px; width: 100%;"><tr style="font-family: Verdana;"><td style="font-family: Verdana;">Forma de pagamento</td></tr></table>
	<table class="table-data" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; font-family: Verdana; margin-bottom: 40px; width: 100%;">
		<tr class="data" style="font-family: Verdana;">
			<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><img src="<?= cSUrlSiteEmpresa ?>/email/img/itau.png" class="logo-banco" width="43" style="font-family: Verdana; width: 43px;"></td>
			<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><?= $formaPagamento; ?></td>
		</tr><!-- data -->
	</table><!-- table-data -->
	<br style="font-family: Verdana;">
					
				</td>
			</tr><!-- content -->

			</table><!-- ctn -->

</body>
</html>

<?php
        $informacoes = ob_get_contents();
        ob_get_clean();
        return $informacoes;
    }
?>