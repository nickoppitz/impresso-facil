<?php 
  	function fillEmail($dadosEmail){   	  	
  		ob_start();
?>
		<!DOCTYPE html>
<html style="font-family: Verdana;">
<head style="font-family: Verdana;">
	<meta charset="utf-8" style="font-family: Verdana;">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" style="font-family: Verdana;">
	<title style="font-family: Verdana;"></title>
	
</head>
<body style="font-family: Verdana; text-align: center;">

	<table class="ctn" style="border-spacing: 0; color: #545454; font-family: Verdana; margin: auto; text-align: left; width: 570px;">

		<tr class="header" style="font-family: Verdana;">
			<td style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; padding: 20px 0;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png" style="font-family: Verdana;">
			</td>
			<td class="infos" style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; line-height: 25px; padding: 20px 0; text-align: right;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png" style="font-family: Verdana;">
			</td>
		</tr><!-- header -->
			<tr class="content" style="font-family: Verdana;">
				<td colspan="2" style="font-family: Verdana; padding: 20px 0;">
					<div class="txt" style="display: block; font-family: Verdana; line-height: 25px; margin: 40px 0; text-align: left;">
						Olá Sr(a). Administrador, recebemos um novo contato via website
					</div><!-- txt -->
					<table class="ttl-red" style="background-color: #EA2026; color: #fff; font-family: Verdana; font-size: 16px; font-weight: bold; padding: 10px; width: 100%;"><tr style="font-family: Verdana;"><td style="font-family: Verdana;">Contato Recebido</td></tr></table>
					<table class="table-data" style="border-collapse: collapse; border-spacing: 0; font-family: Verdana; margin-bottom: 40px; width: 100%;">					
					<?php foreach($dadosEmail['fields'] as $key => $value): ?>
						<tr class="data" style="font-family: Verdana;">
							<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><b style="font-family: Verdana;"><?= $key; ?>: </b></td>
							<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><?= $value; ?></td>
						</tr><!-- data -->
					<?php endforeach; ?>
					</table><!-- table-data -->
				</td>
			</tr><!-- content -->
			</table><!-- ctn -->

</body>
</html>
<?php

		$texto = ob_get_contents();
		ob_get_clean();
		return $texto;
	}
?>