<tfoot>
    <tr>
        <td>
            <div class="footer" style="padding:40px 0;background-color:#222222;color:#ECECEC;font-size:16px;text-align:center;">
                <div class="ctn" style="max-width:1170px;margin-left:auto;margin-right:auto;padding-left:15px;padding-right:15px;">
                    <div class="footer-ttl" style="color:#fff;font-size:18px;font-weight:600;padding-bottom:5px;border-bottom:3px solid #ED262B;display:table;margin:auto;">    <?= cSSegmentoAtendimento ?>
                    </div>
                    <p><a href="tel: <?= cSTelefone1 ?>" style="color:#fff;"><?= cSTelefone1 ?></a></p>
                    <p><a href="mailto: <?= getConfig('CFGEMAILRECEBIMENTO'); ?>" style="color:#fff;"><?= getConfig('CFGEMAILRECEBIMENTO'); ?></a></p>
                </div>
            </div>
        </td>
    </tr>
</tfoot>