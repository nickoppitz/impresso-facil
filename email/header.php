<thead>
    <tr>
        <th class="header" style="text-align:left;padding:10px;border-bottom:1px solid #eee;margin-bottom:10px;background-color:#fff;">
            <div class="ctn" style="max-width:1170px;margin-left:auto;margin-right:auto;padding-left:15px;padding-right:15px;">
                <div class="row">
                    <div class="logo" style="float:left;padding-top:15px;">
                        <img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png" width="147">
                    </div>
                    <div class="info" style="float:right;text-align:right;padding-top:5px;">
                        <a class="url-site" href="<?= cSUrlSiteEmpresa ?>" target="_blank" style="color:#555;font-weight:400;margin:10px 0;"><?= cSSiteEmpresa ?></a>
                        <div class="phone" style="color:#555;font-weight:400;margin:10px 0;">
                            <img src="<?= cSUrlSiteEmpresa ?>/email/img/phone.png"> <?= cSTelefone1 ?>
                        </div>
                    </div>
                </div>
            </div>
        </th>
    </tr>
</thead>