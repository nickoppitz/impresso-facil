<!DOCTYPE html>
<html style="font-family: Verdana;">
<head style="font-family: Verdana;">
	<meta charset="utf-8" style="font-family: Verdana;">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" style="font-family: Verdana;">
	<title style="font-family: Verdana;"></title>
	
</head>
<body style="font-family: Verdana; text-align: center;">

	<table class="ctn" style="border-spacing: 0; color: #545454; font-family: Verdana; margin: auto; text-align: left; width: 570px;">

		<tr class="header" style="font-family: Verdana;">
			<td style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; padding: 20px 0;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png" style="font-family: Verdana;">
			</td>
			<td class="infos" style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; line-height: 25px; padding: 20px 0; text-align: right;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png" style="font-family: Verdana;">
			</td>
		</tr><!-- header -->

		<tr class="content" style="font-family: Verdana;">

			<td colspan="2" style="font-family: Verdana; padding: 20px 0;">
				<div class="txt" style="display: block; font-family: Verdana; line-height: 25px; margin: 40px 0; text-align: left;">
					<img src="<?= cSUrlSiteEmpresa ?>/email/img/confirmacao-de-compra.png" class="pedido-status" style="display: block; font-family: Verdana; margin: 0 auto 40px;">

					<br style="font-family: Verdana;"><br style="font-family: Verdana;">

					Olá sr(a). Nome Do Cliente, recebemos a confirmação de que você já recebeu o seu pedido.

					<br style="font-family: Verdana;"><br style="font-family: Verdana;">

					Obrigado por comprar conosco. <br style="font-family: Verdana;">

					Qualquer dúvida estamos a disposição.

					<br style="font-family: Verdana;"><br style="font-family: Verdana;">

					</div><!-- txt -->

				<table class="ttl-red" style="background-color: #EA2026; color: #fff; font-family: Verdana; font-size: 16px; font-weight: bold; padding: 10px; width: 100%;"><tr style="font-family: Verdana;"><td style="font-family: Verdana;">Compras</td></tr></table>

				<table class="table-data" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; font-family: Verdana; margin-bottom: 40px; width: 100%;">
					<tr class="ttl" style="font-family: Verdana;">
						<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Produto</td>
						<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Qtd</td>
						<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Status</td>
						<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Preço</td>
					</tr><!-- ttl -->

					<tr class="data" style="font-family: Verdana;">
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Agenda Casa Brasileira 2017</td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">30</td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Pedido realizado</td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">R$ 55,00</td>
					</tr><!-- data -->

					<tr class="data" style="font-family: Verdana;">
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Livro 30x30</td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">3</td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Pedido realizado</td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">R$ 55,00</td>
					</tr><!-- data -->

					<tr class="total" style="font-family: Verdana;">
						<td colspan="4" style="color: #333; font-family: Verdana; font-size: 12px; line-height: 20px; padding-top: 10px; text-align: right;">
							<span class="red" style="color: #EA2026; font-family: Verdana;">Subtotal</span> R$ 75,00 <br style="font-family: Verdana;">
							<span class="red" style="color: #EA2026; font-family: Verdana;">+ Frete</span> R$ 19,28 <br style="font-family: Verdana;">
							<span class="total-final" style="font-family: Verdana; font-size: 16px;"><span class="red" style="color: #EA2026; font-family: Verdana;">Total</span> R$ 94,28</span>
						</td>
					</tr><!-- total -->
				</table><!-- table-data -->

				<br style="font-family: Verdana;">

				<table class="ttl-red" style="background-color: #EA2026; color: #fff; font-family: Verdana; font-size: 16px; font-weight: bold; padding: 10px; width: 100%;"><tr style="font-family: Verdana;"><td style="font-family: Verdana;">Dados de entrega</td></tr></table>

				<table class="table-data" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; font-family: Verdana; margin-bottom: 40px; width: 100%;">
					<tr class="ttl" style="font-family: Verdana;">
						<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Nome</td>
						<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Endereço</td>
						<td style="color: #EA2026; font-family: Verdana; font-size: 14px; font-weight: bold; height: 1; margin: 0; padding: 0; padding-bottom: 5px; padding-top: 5px;">Prazo</td>
					</tr><!-- ttl -->

					<tr class="data" style="font-family: Verdana;">
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Nome Do Cliente</td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Avenida Farrapos, 146 - Floresta, Porto Alegre - RS 
CEP: 90220-000</td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Até 5 Dias úteis Após o embarque.</td>
					</tr><!-- data -->
				</table><!-- table-data -->

				<br style="font-family: Verdana;">

				<table class="ttl-red" style="background-color: #EA2026; color: #fff; font-family: Verdana; font-size: 16px; font-weight: bold; padding: 10px; width: 100%;"><tr style="font-family: Verdana;"><td style="font-family: Verdana;">Forma de pagamento</td></tr></table>

				<table class="table-data" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; font-family: Verdana; margin-bottom: 40px; width: 100%;">
					<tr class="data" style="font-family: Verdana;">
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;"><img src="<?= cSUrlSiteEmpresa ?>/email/img/banco-do-brasil.jpg" style="font-family: Verdana;"></td>
						<td style="border: 1px solid #eee; font-family: Verdana; font-size: 12px; line-height: 25px; padding: 20px;">Depósito do Banco do Brasil</td>
					</tr><!-- data -->
				</table><!-- table-data -->

				<br style="font-family: Verdana;">
				
			</td>
		</tr><!-- content -->

	</table><!-- ctn -->

</body>
</html>