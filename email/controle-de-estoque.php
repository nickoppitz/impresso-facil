<?php
    function emailEstoqueBaixo($produto){
        $dados = fillProdutos($produto);
    	ob_start();
?>
		<!DOCTYPE html>
<html style="font-family: Verdana;">
<head style="font-family: Verdana;">
	<meta charset="utf-8" style="font-family: Verdana;">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" style="font-family: Verdana;">
	<title style="font-family: Verdana;"></title>
	
</head>
<body style="font-family: Verdana; text-align: center;">

	<table class="ctn" style="border-spacing: 0; color: #545454; font-family: Verdana; margin: auto; text-align: left; width: 570px;">

		<tr class="header" style="font-family: Verdana;">
			<td style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; padding: 20px 0;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png" style="font-family: Verdana;">
			</td>
			<td class="infos" style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; line-height: 25px; padding: 20px 0; text-align: right;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png" style="font-family: Verdana;">
			</td>
		</tr><!-- header -->

			<tr class="content" style="font-family: Verdana;">
				<td colspan="2" style="font-family: Verdana; padding: 20px 0;">
					<div class="txt" style="display: block; font-family: Verdana; line-height: 25px; margin: 40px 0; text-align: left;">
						<div class="ttl-black" style="font-family: Verdana; font-size: 18px; font-weight: bold; padding: 20px 0; text-transform: uppercase;">Controle de estoque</div>

						Olá sr(a). <strong style="font-family: Verdana;">Administrador</strong>, restam apenas <b style="font-family: Verdana;"><?= $dados['PROESTOQUE'] ?></b> items do produto <b style="font-family: Verdana;"><?= $dados['PRONOME'] ?></b>.
						
					</div><!-- txt -->
				</td>
			</tr><!-- content -->
			
			</table><!-- ctn -->

</body>
</html>
<?php
	    $texto = ob_get_contents();

	    ob_get_clean();
	    return $texto;
    }
?>
