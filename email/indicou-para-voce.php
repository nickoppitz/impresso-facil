<?php 
    require_once('../admin/includes/constantes.php');
    require_once '../admin/libs/phpmailer/email.php';    

    $amigoQueIndicou    = ($_POST['nome'] != '') ? filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING) :$_SESSION['CLINOME'];
    $nomeAmigoIndicado  = filter_input(INPUT_POST, 'nomeAmigo', FILTER_SANITIZE_STRING);
    $emailAmigoIndicado = filter_input(INPUT_POST, 'emailAmigo', FILTER_SANITIZE_EMAIL);
    $url                = filter_input(INPUT_POST, 'url', FILTER_SANITIZE_URL);
    $mensagem           = $_POST['mensagem'];
    $titulo             = $amigoQueIndicou." indicou um produto para você!";
    
    ob_start();
?>
	<!DOCTYPE html>
<html style="font-family: Verdana;">
<head style="font-family: Verdana;">
	<meta charset="utf-8" style="font-family: Verdana;">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" style="font-family: Verdana;">
	<title style="font-family: Verdana;"></title>
	
</head>
<body style="font-family: Verdana; text-align: center;">

	<table class="ctn" style="border-spacing: 0; color: #545454; font-family: Verdana; margin: auto; text-align: left; width: 570px;">

		<tr class="header" style="font-family: Verdana;">
			<td style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; padding: 20px 0;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png" style="font-family: Verdana;">
			</td>
			<td class="infos" style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; line-height: 25px; padding: 20px 0; text-align: right;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png" style="font-family: Verdana;">
			</td>
		</tr><!-- header -->
		<tr class="content" style="font-family: Verdana;">
			<td colspan="2" style="font-family: Verdana; padding: 20px 0;">
				<div class="txt" style="display: block; font-family: Verdana; line-height: 25px; margin: 40px 0; text-align: left;">
					<div class="ttl-black" style="font-family: Verdana; font-size: 18px; font-weight: bold; padding: 20px 0; text-transform: uppercase;"><?= $amigoQueIndicou ?>, indicou um produto para você</div>					

					Olá <?= $nomeAmigoIndicado ?>, <?= $amigoQueIndicou ?> indicou um produto para você na Loja Algo Mais!
					<br style="font-family: Verdana;"><br style="font-family: Verdana;">

					<b style="font-family: Verdana;">E lhe deixou a seguinte mensagem: </b>
					<br style="font-family: Verdana;"><?= $mensagem; ?>
					<br style="font-family: Verdana;"><br style="font-family: Verdana;">

					Para conferir <a href="<?= $url ?>" target="_blank" class="veja-mais" style="color: #0676B7; font-family: Verdana;">clique aqui se já for cadastrado</a> ou 
					<a href="<?= cSUrlSiteEmpresa ?>/cadastro/" target="_blank" class="veja-mais" style="color: #0676B7; font-family: Verdana;">clique aqui se ainda não for cadastrado.		
				</a></div><!-- txt -->
			</td>
		</tr><!-- content -->

		</table><!-- ctn -->

</body>
</html>
<?php
    $dados = ob_get_contents();

    ob_get_clean();

    echo json_encode(envioEmail($titulo, $dados, $emailAmigoIndicado));
?>