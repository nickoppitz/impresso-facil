<?php    
	function enviarEmailProva($pedido, $cliente){
    	ob_start();
?>
		<!DOCTYPE html>
<html style="font-family: Verdana;">
<head style="font-family: Verdana;">
	<meta charset="utf-8" style="font-family: Verdana;">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" style="font-family: Verdana;">
	<title style="font-family: Verdana;"></title>
	
</head>
<body style="font-family: Verdana; text-align: center;">

	<table class="ctn" style="border-spacing: 0; color: #545454; font-family: Verdana; margin: auto; text-align: left; width: 570px;">

		<tr class="header" style="font-family: Verdana;">
			<td style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; padding: 20px 0;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/logo.png" style="font-family: Verdana;">
			</td>
			<td class="infos" style="border-bottom: 1px solid #ddd; border-collapse: collapse; font-family: Verdana; line-height: 25px; padding: 20px 0; text-align: right;">
				<img src="<?= cSUrlSiteEmpresa ?>/email/img/assinatura.png" style="font-family: Verdana;">
			</td>
		</tr><!-- header -->

		<tr class="content" style="font-family: Verdana;">
			<td colspan="2" style="font-family: Verdana; padding: 20px 0;">
				<div class="txt" style="display: block; font-family: Verdana; line-height: 25px; margin: 40px 0; text-align: left;">
					Olá Sr(a). <strong style="font-family: Verdana;"><?= $cliente; ?></strong> <a href="<?= cSUrlSiteEmpresa; ?>/meus-pedidos/<?= str_pad($pedido, 11, '0', STR_PAD_LEFT); ?>/ " class="clique-aqui" style="-moz-border-radius: 5px; -webkit-border-radius: 5px; background-color: #EA2026; border-radius: 5px; color: #fff; font-family: Verdana; font-weight: bold; padding: 5px 8px; text-decoration: none; text-transform: uppercase;">clique aqui</a> para visualizar as provas virtuais do seu pedido.<br style="font-family: Verdana;"><br style="font-family: Verdana;">

					Favor verificar e autorizar o inicio da produção.<br style="font-family: Verdana;"><br style="font-family: Verdana;">
					ATENÇÃO: O prazo de entrega passa a contar a partir da aprovação da PROVA VIRTUAL.<br style="font-family: Verdana;"><br style="font-family: Verdana;">

					Caso tenha alguma dúvida estaremos à disposição através de nossos canais de atendimento.<br style="font-family: Verdana;">
				</div><!-- txt -->
			</td>
		</tr><!-- content -->

			</table><!-- ctn -->

</body>
</html>
<?php
		$dados = ob_get_contents();
        ob_get_clean();
        return $dados;
	}
?>