var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();


// *******************************************
// Pega os splits(.js)  dentro de /gulp-tasks/
// *******************************************
function getTask(task) {
    return require('./gulp-tasks/' + task)(gulp, plugins);
}


// *************************************************************************
// Pega um split(.js) especiífico e uma task específica do respectivo split. 
// Exemplo: gulp-tasks/ui e gulp-tasks/email
// *************************************************************************
gulp.task('ui', getTask('ui'));
gulp.task('email-default', getTask('email'));
gulp.task('email-deploy-default', getTask('email'));


// *******************************************************************
// Cria uma task específica chamando as tasks escolhidas anteriormente
// *******************************************************************
gulp.task('default', ['ui']);
gulp.task('email', ['email-default']);
gulp.task('email-deploy', ['email-deploy-default']);

