<?php 
$namePage = "checkout-pagamento";
include("header.php"); 
if(!is_numeric($_SESSION['CLICODIGO'])){
	require_once 'modal-login-lg.php';
	require_once 'footer.php'; 
	die();
}
require_once 'admin/transaction/transactionEnderecos.php';
$endereco = fillEnderecosByCliente($_SESSION['CLICODIGO']);
require_once 'admin/transaction/transactionPedidosxAnexos.php';

if(!empty($_FILES['vFPXAANEXO'])){
	$anexos = reArrayFiles($_FILES['vFPXAANEXO']);
	foreach($anexos as $i => $anexo){
		if(is_file($anexo['tmp_name'])){
			$nomeArquivo = removerAcentoEspacoCaracter($anexo['name']);

			foreach ($_SESSION['CARRINHO'] as $carrinho) {
				if ($carrinho['PRODUTO'] == $_POST['vIPROCODIGO'][$i])
					$vSTOKEN = $carrinho['TOKEN'];
			}

			$diretorio = $_SERVER['DOCUMENT_ROOT']."/anexos/{$vSTOKEN}";
			if(!is_dir($diretorio))	mkdir($diretorio, 0755, true);

			uploadArquivo($anexo, $diretorio, $nomeArquivo);

			$arrAnexo = array(
				'vSPXAANEXO'    => $nomeArquivo,
				'vIPROCODIGO'   => $_POST['vIPROCODIGO'][$i],
				'vSPXACARRINHO' => $vSTOKEN,
				'vIPXATAMANHO'  => $anexo['size'],
				'vSPXATIPO'     => $anexo['type'],
			);
			insertPedidoAnexos($arrAnexo);
		}
	}
}
?>
	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Checkout</h1>
			<ul class="step-by-step">
				<li>
					<a href="/checkout/carrinho/" class="itm">
						<div class="number cart"></div>
						<div class="description">Carrinho</div>
					</a><!-- itm -->				
				</li>

				<li>
					<div class="itm isActive">
						<div class="number payment"></div>
						<div class="description">Pagamento</div>
					</div><!-- itm -->
				</li>

				<li>
					<div class="itm">
						<div class="number finish"></div>
						<div class="description">Concluído</div>
					</div><!-- itm -->
				</li>
			</ul><!-- step-by-step -->

			<div class="row">
				<div class="half">
					<div class="resume table">
						<div class="table-header">
							<div class="ttl ttl-sm-black-2">RESUMO DO PEDIDO</div>
						</div><!-- header-table -->
						<?php 
							$valorProdutos = 0;
							foreach ($_SESSION['CARRINHO'] as $carrinho) {
								$valorProdutos += ($carrinho['VALORUNITARIO']*$carrinho['QUANTIDADE']);
							}
						?>
						<div class="table-content">
							<table class="resume">
								<tr class="row">
									<td class="itm-resume">PRODUTOS</td>
									<td class="price-resume"><?= formatar_moeda($valorProdutos) ?></td>
								</tr>
								<tr class="row">
									<td class="itm-resume">FRETE</td>
									<td class="price-resume freight">À calcular</td>
								</tr>
								<tr class="row">
									<td class="itm-resume total">TOTAL A PAGAR</td>
									<td class="price-resume total">À calcular</td>
								</tr>
							</table>
						</div><!-- table-content -->
					</div><!-- resume table -->
				</div><!-- half -->
				<div class="half">
					<div class="table">
						<div class="table-header">
							<div class="ttl ttl-sm-black-2">ENDEREÇO DE ENTREGA</div>
						</div>

						<div class="table-content">
							<div class="row">
								<ul class="full">
									<li><?= $_SESSION['CLINOME'] ?> <a href="/meus-dados/">(alterar destinatário)</a></li>
									<li><?= $endereco['ENDLOGRADOURO'].', '.$endereco['ENDNUMERO'].' - '.$endereco['ENDBAIRRO'].', '.$endereco['CIDDESCRICAO'].' - '.$endereco['ESTSIGLA']; ?></li>
									<li>CEP: <?= $endereco['ENDCEP'] ?></li>
									<li><a href="/meus-enderecos/">Alterar endereço de entrega</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div><!-- half -->
			</div><!-- row -->

			<div class="delivery-option table">
				<div class="table-header">
					<div class="ttl ttl-sm-black-2">OPÇÕES DE ENTREGA</div>
				</div><!-- header-table -->

				<div class="table-content">
					<p>Escolha como gostaria de receber seu pedido</p>
					<form class="form-1">
						<ul class="lst-delivery">
							<?php 
								require_once 'php/calcularFreteCarrinho.php';
								foreach(calcularFreteCarrinho($endereco['ENDCEP'], $valorProdutos) as $i => $frete){
									$prazoFrete = ($frete['prazo'] > 1) ? $frete['prazo'].' Dias úteis' : $frete['prazo'].' Dia útil';
									echo "<li class=\"itm-delivery\" data-tab=\"delivery-option-0{$i}\">
											<input type=\"radio\" name=\"modalidadeFrete\" id=\"modalidadeFrete0{$i}\" value=\"{$i}\"";
										if($_POST['freteModalidade'] == $i) echo 'checked';
										echo ">
											<label class=\"radio\" for=\"modalidadeFrete0{$i}\">
												{$frete[modalidade]}
												<div class=\"days\" data-frete=\"{$frete[valor]}\" data-total=\"{$frete[total]}\" data-tipoFrete=\"{$frete[TIPOFRETE]}\" data-codigoFrete=\"{$frete[CODIGO]}\" data-prazo=\"{$frete[prazo]}\">{$prazoFrete}
												</div>
												<div class=\"price\">{$frete[valor]}</div>
											</label>
										</li>";
								}
							?>
						</ul>
					</form>
				</div>
			</div>

			<div class="payment-forms table">
				<div class="table-header">
					<div class="ttl ttl-sm-black-2">FORMA DE PAGAMENTO</div>			
				</div><!-- header-table -->

				<div class="table-content row">
					<div class="options">
						<p>Escolha sua forma de pagamento<br><br></p>

						<form>
							<ul class="tabs">
								<li class="tab-link" data-tab="payment-form-01" data-payment="0">
									<input type="radio" name="payment-form" id="boleto" value="boleto">
									<label for="boleto">
										<img src="img/itau.png" class="pic"> 
										<div class="txt">Boleto Itaú</div>
									</label>
								</li>
								<li class="tab-link" data-tab="payment-form-02" data-payment="1">
									<input type="radio" name="payment-form" id="deposito" value="deposito">
									<label for="deposito">
										<img src="img/itau.png" class="pic"> 
										<div class="txt">Depósito em conta Itaú</div>
									</label>
								</li>
							</ul><!-- tabs -->
						</form>
					</div><!-- options -->

					<div class="options-info">
						<div class="tab-content" id="payment-form-01">
							<div class="itm-options-info">
								<div class="name">Boleto Itaú</div>
								<div class="value">Valor total: À Calcular</div>
								<div class="detail">
									<img src="img/itau.png" class="pic">
									<div class="txt">O pagamento deverá ser realizado no próximo dia útil</div>
								</div>
							</div><!-- itm-options-info -->
						</div><!-- tab-content -->

						<div class="tab-content" id="payment-form-02">
							<div class="itm-options-info">
								<div class="name">Depósito em conta Itaú</div>
								<div class="value">Valor total: À Calcular</div>
								<div class="detail">
									<img src="img/itau.png" class="pic">
									<div class="txt">As informações para depósito serão exibidas ao término do pedido</div>
								</div>
							</div><!-- itm-options-info -->
						</div><!-- tab-content -->
					</div><!-- options-info -->
				</div><!-- table content -->
			</div><!-- payment-forms table -->

			<form action="/checkout/finalizar/" method="POST" id="formPagamento" class="form-1">
				<div class="table">
					<div class="table-header">
						<div class="ttl ttl-sm-black-2">SUAS OBSERVAÇÕES</div>			
					</div><!-- header-table -->

					<div class="table-content row">
						<fieldset class="full">
							<textarea name="vSPEDOBSERVACOESCOMPRADOR" id="vSPEDOBSERVACOESCOMPRADOR" rows="10"></textarea>
							<label class="form-1-placeholder" for="vSPEDOBSERVACOESCOMPRADOR">Caso queira nos deixar alguma observação</label>
						</fieldset>
					</div><!-- table content -->
				</div>
				<input type="hidden" name="vIPEDFORMAPAGAMENTO">
				<input type="hidden" name="vSPEDFORMAENVIO" value="">
				<input type="hidden" name="vIPEDCODIGOENVIO" value="">
				<input type="hidden" name="vIPEDPRAZOENTREGA" value="">
				<input type="hidden" name="vSPEDVALORPRODUTOS" value="<?= $valorProdutos ?>">
				<input type="hidden" name="vSPEDVALORFRETE" value="">
				<button type="submit" class="btn-continue btn-md-red">CONTINUAR</a>
			</form>
		</div><!-- ctn -->
	</main>
<?php 
	require_once 'footer.php'; 
?>
<script>
	chageTipoFrete(<?= ($_POST['freteModalidade'] != '') ? $_POST['freteModalidade'] : 0 ?>);
</script>