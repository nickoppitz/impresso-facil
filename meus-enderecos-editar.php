<?php 
	$namePage = "meus-enderecos-editar";
	include("header.php"); 
	require_once 'admin/transaction/transactionEnderecos.php';
	require_once 'admin/transaction/transactionEstados.php'; 
	$fill = fillEnderecosByCliente($_SESSION['CLICODIGO']);
?>
	<main>
		<div class="ctn">
			
			<h1 class="ttl-md-black">Editar endereço cadastrado</h1>
			<div class="panel">
				<!-- panel nav -->
				<?php include("panel-nav.php"); ?>
				<!-- panel nav -->

				<div class="panel-data-list">
					<form class="form-1 form-1-grey" name="form-register" method="POST" action="/meus-enderecos/alterar/">
						
						<div class="row">
							<fieldset class="full">
								<input type="text" id="vSENDCEP" name="vSENDCEP" value="<?= $fill['ENDCEP']; ?>" required>
								<label class="form-1-placeholder" for="vSENDCEP">CEP:</label>
							</fieldset>
						</div><!-- row -->
							
						<div class="row">
							<fieldset class="full">
								<input type="text" id="vSENDLOGRADOURO" name="vSENDLOGRADOURO" value="<?= $fill['ENDLOGRADOURO']; ?>" required>
								<label class="form-1-placeholder" for="vSENDLOGRADOURO">Endereço:</label>
							</fieldset>
						</div><!-- row -->

						<div class="row">
							<fieldset class="half">
								<input type="text" id="vIENDNUMERO" name="vIENDNUMERO" value="<?= $fill['ENDNUMERO']; ?>" required>
								<label class="form-1-placeholder" for="vIENDNUMERO">Número:</label>
							</fieldset>

							<fieldset class="half">
								<input type="text" id="vSENDCOMPLEMENTO" name="vSENDCOMPLEMENTO" value="<?= $fill['ENDCOMPLEMENTO']; ?>">
								<label class="form-1-placeholder" for="vSENDCOMPLEMENTO">Complemento</label>
							</fieldset>
						</div><!-- row -->

						<div class="row">
							<fieldset class="full">
								<input type="text" id="vSENDBAIRRO" name="vSENDBAIRRO" value="<?= $fill['ENDBAIRRO']; ?>" required>
								<label class="form-1-placeholder" for="vSENDBAIRRO">Bairro:</label>
							</fieldset>
						</div><!-- row -->	

						<div class="row">
							<fieldset class="half">
								<label class="select-arrow"></label>
								<select id="vIESTCODIGO" name="vIESTCODIGO" required>
								<?php 	foreach(comboEstados() as $estado){
											if($estado['ESTCODIGO'] == $fill['ESTCODIGO'])
												echo "<option value=\"{$estado[ESTCODIGO]}\" selected>{$estado[ESTDESCRICAO]}</option>\n";
											else	
												echo "<option value=\"{$estado[ESTCODIGO]}\">{$estado[ESTDESCRICAO]}</option>\n";
										}
								?>	
								</select>
								<label class="form-1-placeholder" for="vIESTCODIGO">Estado</label>
							</fieldset>

							<fieldset class="half">
								<label class="select-arrow"></label>
								<select id="vICIDCODIGO" name="vICIDCODIGO" required>
									<option disabled selected></option>
								</select>
								<label class="form-1-placeholder" for="vICIDCODIGO">Cidade</label>
							</fieldset>
						</div><!-- row -->

						<div class="ttl-sm-red">Confirmar senha</div>

						<div class="row">	
							<fieldset class="full">
								<input type="password" id="vSCLISENHACONFIRMACAO" name="vSCLISENHACONFIRMACAO" required minlength="8">
								<label class="form-1-placeholder" for="vSCLISENHACONFIRMACAO">Senha atual</label>
							</fieldset>
						</div><!-- row -->
						<input type="hidden" name="vIENDCODIGO" value="<?= $fill['ENDCODIGO'] ?>">
						<input type="hidden" name="vICLICODIGO" value="<?= $fill['CLICODIGO'] ?>">
						<footer class="form-1-footer">
							<button type="submit" class="btn-md-red">SALVAR</button>
						</footer>
					</form><!-- form-1 -->
				</div><!-- panel-data-list -->
			</div><!-- panel -->			
		</div><!-- ctn -->
	</main>
<?php 
	if(!isset($_SESSION['CLICODIGO']) || !is_numeric($_SESSION['CLICODIGO'])){
		include("modal-login-lg.php");
	}
?>
<?php include("footer.php"); ?>
<?php if(!empty($fill['CLICODIGO'])): ?>
	<script type="text/javascript">
		buscarCidades(<?= $fill['ESTCODIGO'] ?>, <?= $fill['CIDCODIGO'] ?>);
	</script>
<?php endif; ?>