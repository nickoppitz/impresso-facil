<?php
$url = $_GET['url'];
$url = rtrim($url, '/');
$parametros = explode('/', $url);

$arquivo = $parametros[0];
if($arquivo != ''){
	if($arquivo == 'home'){
		require_once 'index.php';
	}elseif($arquivo == 'marcas' && !isset($parametros[1])){
		require_once 'produtos-marca.php';
	}elseif($arquivo == 'marcas' && isset($parametros[1]) && !isset($parametros[3])){
		require_once 'produtos-categoria.php';
	}elseif($arquivo == 'marcas' && isset($parametros[3])){
		require_once 'produtos-subcategoria.php';
	}elseif($arquivo == 'login'){
		require_once 'php/login.php';
	}elseif($arquivo == 'logout'){
		require_once 'php/logout.php';
	}elseif($arquivo == 'meus-dados' && $parametros[1] == 'alterar'){
		require_once 'php/alterarDadosCliente.php';
	}elseif($arquivo == 'meus-enderecos' && $parametros[1] == 'alterar'){
		require_once 'php/alterarEnderecoCliente.php';
	}elseif($arquivo == 'produtos' && is_numeric($parametros[3])){
		require_once 'produtos-detalhe.php';
	}elseif($arquivo == 'produtos' && $parametros[1] == 'pesquisar'){
		require_once 'produtos-pesquisa.php';
	}elseif($arquivo == 'checkout'){
		if($parametros[1] == 'carrinho'){
			require_once 'checkout-carrinho.php';
		}elseif($parametros[1] == 'pagamento'){
			require_once 'checkout-pagamento.php';
		}elseif($parametros[1] == 'finalizar'){
			require_once 'php/finalizarCompra.php';
		}elseif($parametros[1] == 'concluido'){
			require_once 'checkout-concluido.php';
		}elseif($parametros[1] == 'boleto'){
			require_once 'php/boleto.php';
		}
	}elseif($arquivo == 'contato' && $parametros[1] == 'enviar'){
		require_once 'php/valida-contato.php';
	}elseif($arquivo == 'meus-enderecos'){
		require_once 'meus-enderecos-editar.php';
	}elseif($arquivo == 'esqueci-minha-senha' && $parametros[1] == 'recuperar'){
		require_once 'php/recuperarSenha.php';
	}elseif($arquivo == 'esqueci-minha-senha'){
		require_once 'minha-senha.php';
	}elseif($arquivo == 'cadastro' && $parametros[1] == 'salvar'){
		require_once 'php/cadastrarCliente.php';
	}elseif($arquivo == 'pagina' && $parametros[1] != '' && is_numeric($parametros[2])){
		require_once 'paginaDinamica.php';
	}
	else{
		if(is_file($arquivo.'.php'))
			require_once $arquivo.'.php';
		else
			require_once 'index.php';
	}
}else
	require_once 'index.php';