//Marcara para telefone (Incluíndo 9º dígito)
    jQuery("input.telefone")
        .mask("(00) 0000-00009")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-9999");  
            } else {  
                element.mask("(99) 9999-99990");  
            }  
        });

var escritorio = new google.maps.LatLng(-30.011512, -51.206829);

function initialize(){
    var mapProp = {
        center: escritorio,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        //desabilitando scroll zoom do mouse
        scrollwheel: false,
        draggable: false,

        //setando a posição dos controles
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.BOTTOM_CENTER
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        scaleControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        }
    };

    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    var markerEscritorio = new google.maps.Marker({
        position:escritorio,
        icon: "img/pin-map.png"
    });

    markerEscritorio.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);

var gCallbackSuccess = function(val){
    if(val != '')
        $("label.error[for=hiddenRecaptcha]").hide();
};
var gCallbackExpired = function(val){
    if($("label.error[for=hiddenRecaptcha]").length < 1){
        $("input[name=hiddenRecaptcha]").before('<label class="error" for="hiddenRecaptcha">É necessário confirmar que você não é um robô</span>');
    }else{
        $("label.error[for=hiddenRecaptcha]").show();
    }
};