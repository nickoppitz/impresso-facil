$(document).ready(function() {
	$("#vSENDCEP").on('focusout', function(){
		$.ajax({
			url: 'admin/includes/buscarEndereco.php',
			type: 'GET',
			dataType: 'json',
			data: {
				cep: $(this).val()
			},
			success: function(result){
				$("#vSENDLOGRADOURO").val(result.logradouro);
				$("#vSENDBAIRRO").val(result.bairro);
				$("#vIESTCODIGO").val(result.estadoCodigo);
				buscarCidades(result.estadoCodigo, result.cidadeCodigo);
				$("#vSENDNUMERO").focus();
			},
			error: function(){
				sweetAlert("Oops...", "Ocorreu um erro inesperado!", "error");
			}
		});
	});
	
});


function buscarCidades(estado, cidade){
	if(estado != null && estado != undefined){
		$.ajax({
			url: 'admin/includes/comboCidades.php',
			type: 'GET',
			data: {
				estado: estado,
				cidade: cidade
			},
			success: function(result){
				$("#vICIDCODIGO").removeAttr('disabled');
				$("#vICIDCODIGO").html(result);	
				if($("#vICIDCODIGO:selected").val() != null) $("#vICIDCODIGO").addClass('isActive');			
			},
			error: function(){
				sweetAlert("Oops...", "Ocorreu um erro inesperado!", "error");
			}
		});
	}
}
