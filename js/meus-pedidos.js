jQuery(document).ready(function($) {
	$("[data-producao]").on('click', function(){
		if(!$(this).hasClass('btn-disabled')){
			var producao = $(this).data('producao');
			if($(this).hasClass('btn-approved'))
				var situacao = 'A';
			else if($(this).hasClass('btn-disapproved'))
				var situacao = 'R';

			$.ajax({
				url: 'admin/transaction/transactionPedidosxProducao.php',
				type: 'POST',
				dataType: 'json',
				data: {
					method: 'changePosicao',
					vIPPRCODIGO: producao,
					vSPPRSITUACAO: situacao
				},
				success: function(result){
					if(result){
						if(situacao == 'A'){
							$("[data-producao="+producao+"].btn-approved").text('Aprovado');
							$("[data-producao="+producao+"].btn-approved").addClass('btn-disabled');
							$("[data-producao="+producao+"].btn-disapproved").remove();
							$("[data-producao="+producao+"]").removeAttr('data-producao');
						}else if(situacao == 'R'){
							$("[data-producao="+producao+"].btn-disapproved").text('Reprovado');
							$("[data-producao="+producao+"].btn-disapproved").addClass('btn-disabled');
							$("[data-producao="+producao+"].btn-approved").remove();
							$("[data-producao="+producao+"]").removeAttr('data-producao');
						}
						
						var resposta = (situacao == 'A') ? 'aprovada' : 'reprovada';

						sweetAlert("", "Prova "+resposta+" com sucesso!", "success");
					}else{
						sweetAlert("Oops...", "Ocorreu um erro ao enviar o email!", "error");
					}
				},
				error: function(){
					sweetAlert("Oops...", "Ocorreu um erro inesperado!", "error");
				}
			});
		}
	});
});