$(function() {
    $("[data-amount]").on("click", function() {
        var $button = $(this);
        var $input = $button.parent().find("input");
        var $price = $button.parent().find(".value").find(".price");
        var token = $input.attr('data-token');
        var amount = $input.val();
        $.ajax({
            url: 'php/alterarCarrinho.php',
            type: 'GET',
            dataType: 'json',
            data: {
                token: token,
                quantidade : amount
            },
            success: function(result){
                subtotal = result.totalFloat;
                $(".modal-shopping-cart div.resume div.total-price").text('TOTAL: '+result.valorTotal);
                var description = (amount > 1) ? ' Unidades' : ' Unidade';
                $(".product-itm[data-token="+token+"] .units").text(amount+description);
                $(".itm-cart-product[data-token="+token+"] .value .value-unit").text('('+result.valorProduto+' unid.)');
                $price.text(result.totalProduto);

            },
            error: function(erro){
                sweetAlert("Oops...", "Ocorreu um erro inesperado", "error");
            }
        });
    });
});
var subtotalMoeda = '';
$(".cep").mask("99999-999");
$("#formFrete").on('submit', function(event) {
    event.preventDefault();
    var cep = $("#cep").val();
    calcularFrete(cep, subtotal, subtotalMoeda);
});

$(".btn-remove").on('click', function(event) {
    event.preventDefault();
    var token = $(this).data('token');
    $.ajax({
        url: 'php/removerDoCarrinho.php',
        type: 'GET',
        dataType: 'json',
        data: {
            token: token
        },
        success: function(result){
            if(result[0]){
                sweetAlert("Produto removido do carrinho", "", "success");
                $(".modal-shopping-cart .product-itm[data-token="+token+"]").remove();
                $(".itm-cart-product[data-token="+token+"]").remove();
                updateValuesOfCart(result[1]);
                $(".value-subtotal").text('Subtotal: '+result[1]);
                subtotalMoeda = result[1];
                verificarCarrinho();
                calcularFrete($("#cep").val(), '', result[1]);
            }else   
            sweetAlert("Oops...", "Houve um erro ao remover o produto do carrinho", "error");
        },
        error: function(){
            sweetAlert("Oops...", "Ocorreu um erro inesperado", "error");
        }
    });
});

function verificarCarrinho(){
    if($(".itm-cart-product").length < 1){
        $(".list-cart-product").html("<div class=\"cart-empty\">Ainda não há produtos no carrinho! <br>\nClique em <span class=\"btn-buy btn-buy-sm\">Comprar</span> para adicionar produtos aqui.</div>\n");
    }
}

function calcularFrete(cep, subtotal, subtotalMoeda){
    if(cep != ''){
        $.ajax({
            url: 'php/calcularFreteCarrinho.php',
            type: 'GET',
            dataType: 'json',
            cache: false,
            data: {
                cep: cep,
                subtotal: subtotal,
                subtotalMoeda: subtotalMoeda
            },
            beforeSend: function(){
                $(".loader").css('display', 'table');
                $(".result").hide();
                $("div.result > form.form-1 > .info-value > .row.frete").html('');
                $("#formFrete [type=submit]").prop('disabled', true);
            },
            success: function(dados){
                for(var i = 0; i < dados.length; i++){
                    $("div.result > form.form-1 > .info-value > .row.frete").append(gerarRadioFrete(dados[i], i));
                }

                $(".value-freight").text('+ Frete:  '+dados[0].valor);
                $(".value-total").text('TOTAL: '+dados[0].total);
                $(".cart-product-value > .value").text('TOTAL '+dados[0].total);

                $("input[type=radio][name=modalidade]").on('change', function(event) {
                    event.preventDefault();
                    var modalidade = $(this).val();
                    var freteValor = $(this).attr('data-freteValor');
                    var total = $(this).attr('data-total');

                    $("input[type=hidden][name=freteModalidade]").val(modalidade);

                    $(".value-freight").text('+ Frete:  '+freteValor);
                    $(".value-total").text('TOTAL: '+total);
                    $(".cart-product-value > .value").text('TOTAL '+total);
                });
                $.ajax({
                    url: 'admin/includes/buscarEndereco.php',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        cep: cep
                    },
                    beforeSend: function(){
                        $(".result > .result-address").hide();
                    },
                    success: function(endereco){
                        if(endereco.logradouro != '')
                            $(".result > .result-address").html(endereco.logradouro+' - '+endereco.bairro+'<br/>'+endereco.cidade+' - '+endereco.uf).show();

                    }
                });
                $(".loader").hide();
                $(".result").show();
                $("#formFrete [type=submit]").prop('disabled', false);
            },
            error: function(erro){
                sweetAlert("Oops...", "Ocorreu um erro inesperado", "error");
                $("#formFrete [type=submit]").prop('disabled', false);
            }
        });
    }
}

function gerarRadioFrete(frete, contador){
    var textoPrazo = (frete.prazo > 1) ? ' Dias úteis' : ' Dia útil';
    var txt = "<fieldset class=\"third\">"+
                "<input type=\"radio\" name=\"modalidade\" id=\"modalidade_"+contador+"\" value=\""+contador+"\"";
                if(contador == 0) txt += ' checked ';
                txt +="data-freteValor=\""+frete.valor+"\" data-total=\""+frete.total+"\">"+
                "<label class=\"radio\" for=\"modalidade_"+contador+"\">"+
                    frete.modalidade+
                    "<div class=\"days\">"+frete.prazo+textoPrazo+"</div>"+
                    "<div class=\"price\">"+frete.valor+"</div>"+
                "</label>"+
            "</fieldset>";
    return txt;
}

$("form#formPagamento").one("submit", enviarFormPagamento);

function enviarFormPagamento(event) {
    event.preventDefault(); 
    $("form#formPagamento").submit();
}