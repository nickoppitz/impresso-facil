var paginacao = new Paginacao();

paginacao.setSeletor('ul.products-catalog');
paginacao.setUrl('php/produtosIndex.php');
paginacao.setPesquisa(pesquisa);
paginacao.paginacaoContinua();

paginacao.setCallback('test');
function test(){
	if($("ul.products-catalog li").length < 1) $("ul.products-catalog").html("<h2 class='not-found'>Nenhum resultado encontrado para \""+pesquisa+"\"</h2>");
}
var pagina = 1;
$(window).scroll(function(){
	if($(window).scrollTop() == $(document).height() - $(window).height()){
		paginacao.setPagina(pagina++);
		paginacao.paginacaoContinua();
	}
});