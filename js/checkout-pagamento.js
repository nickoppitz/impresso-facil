function chageTipoFrete(tipoFrete){
	var valorFrete = $('label[for=modalidadeFrete0'+tipoFrete+']').find('.days').attr('data-frete');
	var valorTotal = $('label[for=modalidadeFrete0'+tipoFrete+']').find('.days').attr('data-total');

	var modalidade = $('label[for=modalidadeFrete0'+tipoFrete+']').find('.days').attr('data-tipoFrete');
	var codigoFrete = $('label[for=modalidadeFrete0'+tipoFrete+']').find('.days').attr('data-codigoFrete');
	var prazo = $('label[for=modalidadeFrete0'+tipoFrete+']').find('.days').attr('data-prazo');

	if(valorFrete != '' && valorTotal != ''){
		$(".resume .price-resume.freight").text(valorFrete);
		$("input[name=vSPEDVALORFRETE]").val(valorFrete);
		$("input[name=vSPEDFORMAENVIO]").val(modalidade);
		$("input[name=vIPEDCODIGOENVIO]").val(codigoFrete);
		$("input[name=vIPEDPRAZOENTREGA]").val(prazo);
		$(".resume .price-resume.total").text(valorTotal);
		$(".itm-options-info .value").text('Valor total: '+valorTotal);
	}else{
		$(".resume .price-resume.freight").text('À calcular');
		$(".resume .price-resume.total").text('À calcular');
		$("input[name=vIPEDFORMAENVIO]").val('');
	}
}
// *************************
// Checkout-pagamento
// *************************
//Tab delivery options
$(document).ready(function(){

	$('.lst-delivery .itm-delivery').click(function(){
		var tab_id = $(this).attr('data-tab');
		var tipoFrete = $(this).find('input[type=radio]:checked').val();
		$('.lst-delivery .itm-delivery').removeClass('isActive');
		$('.content-delivery').removeClass('isActive');

		$(this).addClass('isActive');


		chageTipoFrete(tipoFrete);
	});



	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');
		var paymentMethod = $(this).attr('data-payment');
		$("input[name=vIPEDFORMAPAGAMENTO]").val(paymentMethod);

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});

	$("#formPagamento").on('submit', function(event){
		var modalidade = $("input[name=vIPEDFORMAENVIO]").val();
		var formPagamento = $("input[name=vIPEDFORMAPAGAMENTO]").val();
		var erro = false;
		if(modalidade == ''){
			erro = true;
			swal({
				title: "Oops!",
				text: "Você esqueceu de escolher a modalidade de envio!",
				type: "warning"
			},
			function(){
				$('html, body').animate({
					scrollTop: $(".delivery-option.table").offset().top
				}, 1000);
			});
		}else if(formPagamento == ''){
			erro = true;
			swal({
				title: "Oops!",
				text: "Você esqueceu de escolher a forma de pagamento!",
				type: "warning"
			},
			function(){
				$('html, body').animate({
					scrollTop: $(".payment-forms.table").offset().top
				}, 1000);
			});
		}

		if(!erro){
			return true;
		}
		
		event.preventDefault();
		return false;
	});
});

$("form#formPagamento").on('submit', function(event){
    $("form#formPagamento [type=submit]").prop('disabled', true);
});