$(".cep").mask("99999-999");
$("#formFrete").on('submit', function(event) {
	event.preventDefault();
	var cep = $("#cep").val();
	var quantidade = $("#quantidade").val();
	if(cep != ''){
		$.ajax({
			url: 'php/calcularFrete.php',
			type: 'GET',
			dataType: 'json',
			data: {
				produto: produto,
				quantidade: quantidade,
				cep: cep
			},
			beforeSend: function(){
				$(".loader").css('display', 'table');
				$(".result").hide();
				$(".result > .value > .itens").html('');
				$("#formFrete [type=submit]").prop('disabled', true);
			},
			success: function(frete){
				for(var i = 0; i < frete.length; i++){
					$(".result > .value > .itens").append(viewFrete(frete[i]));
				}
				$.ajax({
					url: 'admin/includes/buscarEndereco.php',
					type: 'GET',
					dataType: 'json',
					data: {
						cep: cep
					},
					beforeSend: function(){
						$(".result > .address").hide();
					},
					success: function(endereco){
						if(endereco.logradouro != '')
							$(".result > .address").html(endereco.logradouro+' - '+endereco.bairro+'<br/>'+endereco.cidade+' - '+endereco.uf).show();

					}
				});
				$(".loader").hide();
				$("#formFrete [type=submit]").prop('disabled', false);
				$(".result").show();
			},
			error: function(){
				$(".loader").hide();
				$("#formFrete [type=submit]").prop('disabled', false);
				sweetAlert("Oops...", "Ocorreu um erro inesperado", "error");
			}
		});
	}
});

function viewFrete(frete){
	var frasePrazo = (frete.prazo > 1) ? ' Dias úteis' : ' Dia útil';
	return "<div class=\"itm-value shippingType\">"+frete.modalidade+"</div>"+
			"<div class=\"itm-value shippingValue\">"+frete.valor+"</div>"+
			"<div class=\"itm-value shippingTime\">"+frete.prazo+frasePrazo+"</div>";
}

$("form[name=send-doubt]").on('submit', function(event){
	event.preventDefault();
	if($("form[name=send-doubt]").is(':valid')){
		$.ajax({
			url: 'php/enviarDuvidaProduto.php',
			type: 'POST',
			dataType: 'json',
			data: {
				vSCONNOME     : $("input[name=vSCONNOME]").val(),
				vSCONTELEFONE : $("input[name=vSCONTELEFONE]").val(),
				vSCONEMAIL    : $("input[name=vSCONEMAIL]").val(),
				vSCONMENSAGEM : $("textarea[name=vSCONMENSAGEM]").val(),
				produtoNome   : produtoNome,

			},
			beforeSend: function(){
				$("form[name=send-doubt] [type=submit]").prop('disabled', true);
			},
			success: function(result){
				if(result)
					sweetAlert("Dúvida enviada com sucesso", "Em breve lhe responderemos por E-mail", "success");
				else	
					sweetAlert("Oops...", "Houve um erro ao enviar a sua dúvida", "error");
				$(".modal-doubt").hide();
				$('form[name=send-doubt] input, form[name=send-doubt] textarea').val("");
				$("form[name=send-doubt] [type=submit]").prop('disabled', false);
			},
			error: function(){
				sweetAlert("Oops...", "Ocorreu um erro inesperado", "error");
				$("form[name=send-doubt] [type=submit]").prop('disabled', false);
			}
		});
	}
});

$("#buyProduct, #buyProductFastMode").on('click', function(event){
	event.preventDefault();
	var quantidade = $("#quantidade").val();
	if(quantidade != '' && produto != ''){
		$.ajax({
			url: 'php/adiconarAoCarrinho.php',
			type: 'GET',
			dataType: 'json',
			data: {
				produto : produto,
				quantidade : quantidade,
				valorUnitario: valorUnitario
			},
			beforeSend: function(){
				$("#buyProduct, #buyProductFastMode").prop('disabled', true);
			},
			success: function(result){
				if(result[0]){
					appendToCart(generateItemCart(result[1]));
					updateValuesOfCart(result[2]);
					swal({
						title: "Produto adicionado ao carrinho", 
						text: result[3], 
						type: "success"
					},function(){
						if($(event.target).attr('id') == 'buyProductFastMode') location.href = '/checkout/carrinho/';
					});
				}else	
					sweetAlert("Oops...", result[3], "error");
				$("#buyProduct, #buyProductFastMode").prop('disabled', false);
			},
			error: function(){
				sweetAlert("Oops...", "Ocorreu um erro inesperado", "error");
				$("#buyProduct, #buyProductFastMode").prop('disabled', false);
			}
		});
	}
});

function generateItemCart(product){
	var item = "<div class=\"product-itm\" data-token=\""+product.TOKEN+"\">"+
					"<button class=\"btn-rm\" onClick=\"removeToCart('"+product.TOKEN+"', this)\"></button>"+
					"<img src=\""+product.PROIMAGEM+"\" class=\"pic\">"+
					"<div class=\"text\">"+
						"<div class=\"product-ttl\"><a href=\""+product.PROLINK+"\">"+product.PRONOME+"</a></div>"+
						"<div class=\"category\">"+product.PROMARCA+"</div>"+
						"<div class=\"description\">"+product.PROPREDESCRICAO+"</div>"+
						"<div class=\"units\">"+product.QUANTIDADE;
					item += (product.QUANTIDADE > 1) ? ' Unidades' : ' Unidade';
				item +=	"</div>"+
					"</div>"+
				"</div>";
	return item;
}

function appendToCart(product){
	if($(".modal-shopping-cart #mCSB_1_container .product-itm").length >= 1)
		$(".modal-shopping-cart #mCSB_1_container").append(product);
	else
		$(".modal-shopping-cart #mCSB_1_container").html(product);
}

$("form[name=indicacao]").on('submit', function(event){
	event.preventDefault();
	var nome 	    = $("input[name=my-name]").val();
	var nomeAmigo 	= $("input[name=friend-name]").val();
	var emailAmigo 	= $("input[name=friend-email]").val();
	var mensagem 	= $("textarea[name=message-indicate]").val();
	
	if($("form[name=indicacao]").is(':valid')){
		$.ajax({
			url: 'email/indicou-para-voce.php',
			type: 'POST',
			dataType: 'json',
			data: {
				nomeAmigo : nomeAmigo,
				emailAmigo : emailAmigo,
				mensagem : mensagem,
				url: urlProduto,
				nome: nome
			},
			beforeSend: function(){
				$("form[name=indicacao] [type=submit]").prop('disabled', true);
			},
			success: function(result){
				if(result)
					sweetAlert("Sua mensagem foi enviada com sucesso", "Obrigado por nos indicar!", "success");
				else	
					sweetAlert("Oops...", "Houve um erro ao enviar a sua dúvida", "error");
				$(".modal-indicate").hide();
				$('form[name=indicacao] input, form[name=indicacao] textarea').val("");
				$("form[name=indicacao] [type=submit]").prop('disabled', false);
			},
			error: function(){
				sweetAlert("Oops...", "Ocorreu um erro inesperado", "error");
				$("form[name=indicacao] [type=submit]").prop('disabled', false);
			}
		});
	}
});
jQuery("input.telefone")
.mask("(00) 0000-00009")
.focusout(function (event) {  
    var target, phone, element;  
    target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
    phone = target.value.replace(/\D/g, '');
    element = $(target);  
    element.unmask();  
    if(phone.length > 10) {  
        element.mask("(99) 99999-9999");  
    } else {  
        element.mask("(99) 9999-99990");  
    }  
});