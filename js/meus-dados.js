$(document).ready(function() {
	changeTipoPessoa($("select#vSCLITIPOPESSOA").val());
	if($("#vSCLIINSCRICAOESTADUAL").val() == ''){
		$("#vSCLIINSCRICAOESTADUAL").prop('disabled', true);
		$("#checkIsencaoIE").prop('checked', true);
	}
	$("select#vSCLITIPOPESSOA").on('change', function(){
		changeTipoPessoa($(this).val());
	});

	$("#checkIsencaoIE").on('change', function(event){
		if($(this).is(':checked')){
			$("#vSCLIINSCRICAOESTADUAL").prop('disabled', true);
			$("#vSCLIINSCRICAOESTADUAL").val('');
		}else{
			$("#vSCLIINSCRICAOESTADUAL").prop('disabled', false);
		}
	});

	$("#vIESTCODIGO").on('changed.bs.select', function(){
		buscarCidades($(this).val());
	});

	$("#vSENDCEP").on('focusout', function(){
		$.ajax({
			url: '../includes/buscarEndereco.php',
			type: 'GET',
			dataType: 'json',
			data: {
				cep: $(this).val()
			},
			success: function(result){
				$("#vSENDLOGRADOURO").val(result.logradouro);
				$("#vSENDBAIRRO").val(result.bairro);
				$("#vIESTCODIGO").val(result.estadoCodigo);
				$("#vIESTCODIGO").selectpicker('val', result.estadoCodigo);
				$("#vIESTCODIGO").selectpicker('refresh');
				buscarCidades(result.estadoCodigo, result.cidadeCodigo);
				$("#vSENDNUMERO").focus();
			},
			error: function(){
				sweetAlert("Oops...", "Ocorreu um erro inesperado!", "error");
			}
		});
	});
});

function buscarCidades(estado, cidade){
	if(estado != null && estado != undefined){
		$.ajax({
			url: '../includes/comboCidades.php',
			type: 'GET',
			data: {
				estado: estado,
				cidade: cidade
			},
			success: function(result){
				$("#vICIDCODIGO").removeAttr('disabled');
				$("#vICIDCODIGO").html(result);
				$("#vICIDCODIGO").selectpicker('refresh');
			},
			error: function(){
				sweetAlert("Oops...", "Ocorreu um erro inesperado!", "error");
			}
		});
	}
}

function changeTipoPessoa(tipoPessoa){
	if(tipoPessoa == 'J'){
		$(".pessoaFisica").hide();
		$(".pessoaFisica input, .pessoaFisica select")
		.val('')
		.prop('disabled', true);

		$(".pessoaJuridica").show();
		$(".pessoaJuridica input, .pessoaJuridica select").prop('disabled', false);

		$(".inputNome > label").text('Responsável');

		$(".inputCpfCnpj > label").text('CNPJ');
		$(".inputCpfCnpj > input")
		.removeClass('cpf')
		.addClass('cnpj')
		.unmask()
		.mask("99.999.999/9999-99");
		$(".pessoaJuridica input, .pessoaJuridica select").prop('required', true);
		$(".pessoaFisica input, .pessoaFisica select").prop('required', true);
	}else if(tipoPessoa == 'F'){
		$(".pessoaJuridica").hide();
		$(".pessoaJuridica input, .pessoaJuridica select")
		.val('')
		.prop('disabled', true);

		$(".pessoaFisica").show();
		$(".pessoaFisica input, .pessoaFisica select").prop('disabled', false);

		$(".inputNome > label").text('Nome');

		$(".inputCpfCnpj > label").text('CPF');
		$(".inputCpfCnpj > input")
		.removeClass('cnpj')
		.addClass('cpf')
		.unmask()
		.mask("999.999.999-99");
		$(".pessoaJuridica input, .pessoaJuridica select").prop('required', true);
		$(".pessoaFisica input, .pessoaFisica select").prop('required', true);
	}
}

$("#vSCLICPFCNPJ").on('focusout', function(){
	verificarDocumentoCliente($(this), $("input[name=vICLICODIGO]").val());
});

function verificarDocumentoCliente(element, codigo){
	$.ajax({
		url: 'admin/includes/verificarDocumentoCliente.php',
		type: 'GET',
		dataType: 'json',
		data: {
			documento: element.val(),
			codigo: codigo
		},
		success: function(result){
			if(!result){
				sweetAlert("Oops...", "O CPF ou CNPJ informado já foi cadastrado!", "warning");
				element.val('');
				element.focus();
			}
		},
		error: function(){
			sweetAlert("Oops...", "Ocorreu um erro inesperado!", "error");
		}
	});
}

//DatePicker
    jQuery(".datepicker").mask("99/99/9999");
//Marcara para telefone (Incluíndo 9º dígito)
    jQuery("input.telefone")
        .mask("(00) 0000-00009")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-9999");  
            } else {  
                element.mask("(99) 9999-99990");  
            }  
        });