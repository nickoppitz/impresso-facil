function buscarCidades(estado, cidade){
	if(estado != null && estado != undefined){
		$.ajax({
			url: 'admin/includes/comboCidades.php',
			type: 'GET',
			data: {
				estado: estado,
				cidade: cidade
			},
			success: function(result){
				$("#vICIDCODIGO").removeAttr('disabled');
				$("#vICIDCODIGO").html(result);
				$("#vICIDCODIGO").selectpicker('refresh');
			},
			error: function(){
				sweetAlert("Oops...", "Ocorreu um erro inesperado!", "error");
			}
		});
	}
}
