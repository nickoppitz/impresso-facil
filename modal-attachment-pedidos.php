<?php	
	foreach(listPedidosByCliente($_SESSION['CLICODIGO']) as $pedido): 
		$produtos = gridProdutosPedido($pedido['PEDCODIGO']);
		foreach($produtos['dados'] as $produto):
		?>
			<form action="/meus-pedidos/<?= str_pad($pedido['PEDCODIGO'], 11, "0", STR_PAD_LEFT) ?>/" method="POST" id="formPagamento" enctype="multipart/form-data">
				<div class="modal-attachment" id="modal-attachment-<?=$pedido['PEDCODIGO']?>-<?=$produto['PROCODIGO']?>">
					<div class="modal-inner">
						<div class="btn-close" data-dismiss="modal"></div>
						
						<div class="attachment-lst">
							<div class="anexar">
								<div class="title-anexar">ANEXAR ARQUIVOS</div>
								<div class="anexo">
									<label class="label-upload">Adicionar um arquivo</label>
									<input type="file" class="input-upload" id="vFPXAANEXO" name="vFPXAANEXO[]" />
									<input type="hidden" class="hidden-upload" name="vIPROCODIGO[]" id="vIPROCODIGO[]" value="<?=$produto['PROCODIGO']?>" />								
									<input type="hidden" class="hidden-upload" name="vIPEDCODIGO" id="vIPEDCODIGO" value="<?=$pedido['PEDCODIGO']?>" />
									<div class="status-upload"><b>Escolha um arquivo</b></div>								
									<div class="remover"></div>
								</div><!-- anexo -->
							</div><!-- anexar -->
							<div class="anexar-mais-arquivos" id="<?=$pedido['PEDCODIGO']?>"></div>

						</div><!-- attachment-lst -->
						<button class="btn-md-red btn-concluido">Concluído</button>
					</div>
					<div class="modal-overlay"></div>
				</div>
			</form>
			<?php
		endforeach;
	endforeach;
?>