<?php 
$namePage = "home";
include("header.php"); 
require_once 'admin/transaction/transactionBanners.php';
?>

	<div class="slider-products">
		<?php require_once 'php/sliders.php';?>
	
	</div><!-- slider products -->
	
	<div class="banner-welcome">
		<div class="ctn">
			<div class="message"><b>SEJA BEM-VINDO!</b> Aproveite os produtos criados para você!</div>
		</div><!-- ctn -->
	</div><!-- banner welcome -->

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Produtos em destaque</h1>
			
			<ul class="products-catalog">
				<!-- products catalog -->
			</ul>

			<img src="img/loader.svg" class="loader">
		</div><!-- ctn -->
	</main>
<?php 
 if(!isset($_SESSION['CLICODIGO']) || !is_numeric($_SESSION['CLICODIGO'])){
		include("modal-login-lg.php");
 }
 ?>	
<?php include("footer.php"); ?>