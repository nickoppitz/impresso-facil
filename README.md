#Automatizador de tarefas (Gulp)
1) Para rodar a aplicação em Gulp você vai precisar ter instalado em sua máquina:
- [Node.js](https://nodejs.org/en/download/)
- [Ruby](https://www.ruby-lang.org/pt/downloads/)
- [Gulp (instalado globalmente)](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)

2) Caso você necessite fazer alterações em arquivos de formatação(.scss) instale antes globalmente:
- [node-sass](https://www.npmjs.com/package/node-sass)

3) Após a instalação entre na pasta do projeto através do terminal e execute o seguinte comando:
- $ npm-install

4) Pronto! Agora você já pode rodar a aplicação.

##Rodando aplicação para desenvolvimento da UI do site
Utilize o comando a seguir:
- $ gulp

##Rodando aplicação para desenvolvimento da UI dos e-mails
Utilize o comando a seguir:
- $ gulp email

##Rodando aplicação para compilação inline e deploy para o FTP da UI dos e-mails
Utilize o comando a seguir:
- $ gulp email-deploy


# Componentes HTML / CSS
Para facilitar a vida ;)

## Grid para formulários e afins
Sempre que **for** usar um colunas diferentes tem que criar uma row nova.

### Full (Um inteiro)
Exemplo:

```HTML
<div class="row">
  <div class="full">
     Uma coluna de 12/12 :)
  </div>
</div>
```

### Half (Um meio)
Exemplo:

```HTML
<div class="row">
  <div class="half">
     Uma coluna de 6/12
  </div>
  <div class="half">
    Outra de 6/12
  </div>
</div>
```

### Third (Um terço)
Exemplo:

```HTML
<div class="row">
  <div class="third">
     Uma coluna de 4/12
  </div>
  <div class="third">
     Outra coluna de 4/12
  </div>
  <div class="third">
     E mais uma coluna de 4/12 :)
  </div>
</div>
```

## Form-1
É carregado através da class *form-1*

### Class
```HTML
<form class="form-1"></form>
```

### Campos e placeholder
Sempre que **for** adicionar um novo campo com placeholder você precisará colocá-lo dentro de um fieldset. O placeholder será uma label com a class="form-1-paceholder" e sempre virá após o seu respectivo input/select/textarea com o atributo **FOR** recebendo o valor equivalente ao valor do **ID** de seu input/select/textarea.

Exemplo:

```HTML
<form class="form-1">
  <fieldset>
    <input type="text" id="nome-do-campo" name="nome-do-campo" value="">
    <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
  </fieldset>
</form>
```

**Form com fundo cinza**
```HTML
<form class="form-1 form-1-grey">
  <fieldset>
    <input type="text" id="nome-do-campo" name="nome-do-campo" value="">
    <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
  </fieldset>
</form>
```

### Campos com Select
O select sempre receberá uma label antecessora com a class="select-arrow" que é o elemento de auxilio para o estilo de seu icon de dropdown e uma label sucessora que será o placeholder com a class="form-1-placeholder" e o **FOR** sincado com o campo. Seu primeiro option sera vazio e com os atributo disabled e selected.

Exemplo:

```HTML
<fieldset>
  <label class="select-arrow"></label>
  <select name="nome-do-campo" id="nome-do-campo">
    <option disabled selected></option>
    <option value="valor-1">Valor 1</option>
    <option value="valor-1">Valor 2</option>
  </select>
  <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
</fieldset>
```
### Campos com Radio
Basta adicionar o mesmo nome do **ID** do radio ao do **FOR** de sua label respectiva, e adicionar nessa label a *class="radio"*.

Exemplo:

```HTML
<input type="radio" id="nome-do-campo-01" name="nome-da-lista" value="valor-do-campo-01">
<label class="radio" for="nome-do-campo-01">Valor do campo 01</label>

<input type="radio" id="nome-do-campo-02" name="nome-da-lista" value="valor-do-campo-02">
<label class="radio" for="nome-do-campo-02">Valor do campo 02</label>
```

### Campos Checkbox
Basta adicionar o mesmo nome do **ID** do checkbox ao do **FOR** de sua label respectiva, e adicionar nessa label a *class="checkbox"*.

```HTML
<input type="checkbox" id="nome-do-campo-01" name="nome-da-lista" value="nome-do-campo-01">
<label class="checkbox" for="nome-do-campo-01">Nome do campo 01</label>

<input type="checkbox" id="nome-do-campo-02" name="nome-da-lista" value="nome-do-campo-02">
<label class="checkbox" for="nome-do-campo-02">Nome do campo 02</label>
```


### Campos com Addon
Basta adicionar dentro do mesmo fieldset do campo e abaixo desse um checkbox e sua label respectiva com a *class="form-1-addon"*.

Exemplo:

```HTML
<fieldset>
  <input type="text" id="nome-do-campo" name="nome-do-campo">
  <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>

  <input type="checkbox" id="addon-do-campo" name="addon-do-campo">
  <label class="form-1-addon" for="addon-do-campo">Addon do campo</label>
</fieldset>
```

### Campos obrigatórios
Basta acrescentar o atributo required por último na tag que o **jQuery Validate** o verificará automaticamente.

Exemplo:

```HTML
<select required></select>

<input type="text" required>

<input type="radio" required>

<textarea required></textarea>
```
e etc...

Isso fará com que esse script acrescente a marcação visual para o usuário de que esse campo é obrigatório 
```javascript
//Inserção de asterísco quando algum campo é required
$(':input[required]:visible').before("<div class='required'></div>");
```

E com que o **jQuery Validate** valide o form:
```javascript
//Validação
$("form").each(function(){
    $(this).validate();
});
```

### Campos com grid
Basta colocá-los dentro de uma div com a class="row" e acrescentar nas respectivas fieldsets a class de colunas com a divisão desejada.

Exemplo de 1 campo por linha:
```HTML
<form class="form-1">
  <div class="row">
    <fieldset class="full">
      <input type="text" id="nome-do-campo" name="nome-do-campo" value="">
      <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
    </fieldset>
  </div>
</form>
```

Exemplo de 2 campos por linha:
```HTML
<form class="form-1">
  <div class="row">
    <fieldset class="half">
      <input type="text" id="nome-do-campo" name="nome-do-campo" value="">
      <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
    </fieldset>
    
    <fieldset class="half">
      <input type="text" id="nome-do-campo" name="nome-do-campo" value="">
      <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
    </fieldset>
  </div>
</form>
```

Exemplo de 3 campos por linha:
```HTML
<form class="form-1">
  <div class="row">
    <fieldset class="third">
      <input type="text" id="nome-do-campo" name="nome-do-campo" value="">
      <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
    </fieldset>
    
    <fieldset class="third">
      <input type="text" id="nome-do-campo" name="nome-do-campo" value="">
      <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
    </fieldset>

    <fieldset class="third">
      <input type="text" id="nome-do-campo" name="nome-do-campo" value="">
      <label class="form-1-placeholder" for="nome-do-campo">Nome do campo</label>
    </fieldset>
  </div>
</form>
```

## Tables
Componente auxiliar para tables feitas em tags *div* ou em tags *tables*. Aplicado na página meus-pedidos.

Exemplo:
```HTML
<div class="table">
  <div class="table-header">
    <div class="row">
      <div class="half">
        <h1 class="ttl-md-black-2">Título 1</h1>
      </div>
      <div class="half">
        <h2 class="ttl-md-black-2">Título 2</h2>
      </div>
    </div>
  </div>
  <div class="table-content">
    <div class="row">
      <div class="half">
        Conteúdo 1
      </div>
      <div class="half">
        Conteúdo 2
      </div>
    </div>
  </div>
</div>
```

**Table com fundo cinza**

Basta adicionar a class *table-grey*.
```HTML
<div class="table table-grey">
  <div class="table-header"></div>
  <div class="table-content"></div>
</div>
```

## Buttons

###Botão médio com background vermelho e fonte branca.(btn-md-red)
```HTML
<button type="submit" class="btn-md-red"> CADASTRAR </button>
```
OU
```HTML
<div>  
  <a class="btn-md-red" href="/cadastrar/"> CADASTRAR </a>
</div>
```

###Botão médio com background preto e fonte branca.(btn-md-black)
```HTML
<button class="btn-md-black" type="submit">CALCULAR FRETE</button>
```

###Botão médio com formato quadrado, background vermelho e fonte branca 
```HTML###
<button class="btn-md-red-sq" type="submit">ENTRAR</button>
```

###Botão do modal de login no header
```HTML
<button class="btn-login"></button>
```

###Botão para fechar modals, fundo vermelho e icone branco
```HTML
<button class="btn-close"></button>
```

###Botão do modal de carrinho no header
```HTML
<button class="btn-shopping-cart"></button>
```

###Botão do menu no header
```HTML
<button class="btn-menu">Menu</button>
```

###Botão no header para pesquisar. Com icone branco e background vermelho
```HTML
<button class="btn-main-search" type="submit"></button>
```

###Botão de largura grande.fonte branca e background vermelho
```HTML
<a href="/marcas/" class=" btn-lg-red"></a>
```

###Botão de largura média.Fonte branca background azul
```HTML
<button class="btn-md-blue">RECEBER SENHA POR E-MAIL</button>
```

###Botão de largura pequena.background vermelho e fonte branca.
```HTML
<button class="btn-buy-sm">COMPRAR</button><!-- btn -->
```

###Botão de largura pequena.Fonte branca e background verde
```HTML
<a class="btn-sm-green">CAIXA RÁPIDO</a>
```
## Titles

###Fonte com cor preta e tamanho grande
```HTML
<div class="ttl-lg-black-2">Faça seu login</div>
```

###Fonte com cor preta e tamanho médio.
```HTML
<h1 class="ttl-md-black">Cadastre-se</h1>
```

###Fonte com cor preta e tamanho médio
```HTML
<div class="ttl-md-black-2">RESUMO DO PEDIDO</div>
```

###Fonte com cor preta e tamanho pequeno.
```HTML
<div class="ttl-sm-black-2">COMPRA EFETUADA COM SUCESSO</div>
```

OU

```HTML
<h3 class="ttl-sm-black-2">COMPRA EFETUADA COM SUCESSO</h3>
```

###Fonte com cor vermelha e tamanho grande
```HTML
<h1 class="ttl-lg-red-2"></h1>
```


###Fonte com cor vermelha e tamanho pequeno.
```HTML
<h3 class="ttl-sm-red"><div class="ttl-number">1</div> Dados gerais </h3>
```

###Fonte com cor branca e tamanho médio.
```HTML
<h2 class="ttl-md-white">Informações</h2>
```


