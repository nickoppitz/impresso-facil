<?php 
$namePage = "produtos-detalhe";
include("header.php"); 
require_once 'admin/includes/constantes.php';
require_once 'admin/transaction/transactionProdutos.php';
require_once 'admin/transaction/transactionPromocoes.php';
$produto = fillProdutos($parametros[3]);
if($produto['PROESTOQUE'] < $produto['PROQUANTIDADEMINIMAPORVENDA']) $produto['PROSITUACAO'] = 2;
switch ($produto['PROSITUACAO']){
		case 0:
			//Disponivel para venda
			$botao = "COMPRAR";
			$classeBotao = 'btn-buy-sm';
			$preco = (empty($produto['PROVALORANTIGO'])) ? "<h3 class=\"price-promotion\">".$produto['PROVALOR']."</h3>" : "De R$ <span class=\"price-canceled\">".$produto['PROVALORANTIGO']."</span> por <h3 class=\"price-promotion\">R$ ".$produto['PROVALOR']."</h3>";
			break;
		case 1:
			//Preço sob consulta
			$botao = "VER MAIS";
			$classeBotao = 'btn-view-more';
			$preco = "<h3 class=\"price\">Preço sob consulta</h3>";
			break;
		case 2:
			$botao = "VER MAIS";
			$classeBotao = 'btn-view-more';
			$preco = (empty($produto['PROVALORANTIGO'])) ? "<h3 class=\"price-promotion\">".$produto['PROVALOR']."</h3>" : "De R$ <span class=\"price-canceled\">".$produto['PROVALORANTIGO']."</span> por <h3 class=\"price-promotion\">R$ ".$produto['PROVALOR']."</h3>";
			//Ocultar opções de compra (exibir preços)
			break;
		case 3:
			$botao = "VER MAIS";
			$preco = "";
			$classeBotao = 'btn-view-more';
			//Ocultar preços e opções de compra
			break;
	}
?>

	<main>
		<div class="ctn">
			
			<ul class="breadcrumb">
				<li><a href="/marcas/">Linha de Produtos</a></li>
				<li><a href="/marcas/<?= $produto['MARCODIGO'] ?>/<?= $produto['MARMARCA'] ?>/"><?= $produto['MARMARCA'] ?></a></li>
				<li><a href="/marcas/<?= $produto['MARCODIGO'] ?>/<?= $produto['MARMARCA'] ?>/<?= $produto['CATCODIGO'] ?>/<?= $produto['CATCATEGORIA'] ?>/"><?= $produto['CATCATEGORIA'] ?></a></li>
				<li><?= $produto['PRONOME']; ?></li>
			</ul><!-- breadcrumb -->

			<article class="products-dtl-itm">
				<div class="pic">
					<?php
						$imagens = searchImagesGalleryWithThumb('admin/uploads/produtos/'.$produto['PROCODIGO']);
						foreach($imagens as $i => $imagem){
							if($imagem['filename'] == $produto['PROIMAGEMPRINCIPAL'])
								echo "<div id=\"pic-{$i}\" class=\"pic-main isActive\">\n
										<img src=\"{$imagem[file]}\">\n
									</div>\n";
							else
								echo "<div id=\"pic-{$i}\" class=\"pic-main\">\n
										<img src=\"{$imagem[file]}\">\n
									</div>\n";
						}
					?>
					<div class="products-dtl-carousel">
						<?php 
							foreach($imagens as $i => $imagem){
								if($imagem['filename'] == $produto['PROIMAGEMPRINCIPAL'])
									echo "<div data-tab=\"pic-{$i}\" class=\"itm isActive\">\n
											<img src=\"{$imagem[file]}\">\n
										</div>\n";
								else
									echo "<div data-tab=\"pic-{$i}\" class=\"itm\">\n
											<img src=\"{$imagem[file]}\">\n
										</div>\n";
							}
						?>
					</div><!-- products-dtl-carousel -->
				</div><!-- pic -->

				<div class="infos">
					<h1 class="product-ttl ttl-lg-red-2"><?= $produto['PRONOME']; ?></h1>

					<h2 class="category"><?= $produto['CATCATEGORIA']; ?></h2>

					<div class="description"><?= $produto['PROPREDESCRICAO']; ?></div><!-- description -->

					<div class="actions">
					
						<div class="price">
							<?= $preco ?>
							<?php if($produto['PROTIPOFRETE'] == 1) echo "<div class=\"free-shipping\">Frete grátis</div>\n"; ?>
						</div>
							
				
						<?php if($produto['PROSITUACAO'] != 3 && $produto['PROSITUACAO'] != 1 && $produto['PROSITUACAO'] != 2): ?>
							<div class="amount">
								<div class="amount-box">
									<label class="form-1-placeholder" for="quantidade">Quantidade</label>
									<input type="number" name="quantidade" id="quantidade" value="<?= $produto['PROQUANTIDADEMINIMAPORVENDA'] ?>" min="<?= $produto['PROQUANTIDADEMINIMAPORVENDA'] ?>" max="<?= $produto['PROESTOQUE'] ?>" >
								</div><!-- amount box -->
								
								<div class="amount-max"><?= $produto['PROESTOQUE'] ?> unidades disponíveis</div>
								<div class="amount-min"><b>Quantidade mínima por compra:</b> <?= $produto['PROQUANTIDADEMINIMAPORVENDA'] ?></div>
							</div><!-- amount -->

							<div class="payment-data">
								<div class="payment-form">
									<div class="payment-form-ttl modal-details">Ver formas de pagamento</div>
								
									<div class="modal-payment-form">
										<div class="modal-header">
											<div class="ttl">Formas de pagamento</div>
											<button class="btn-close"></button>
										</div><!-- header -->

										<div class="itm-payment-form">
											<img src="img/itau.png" class="pic">

											<div class="info">
												<div class="ttl">Boleto Itaú</div>
												<div class="price">à vista <?= formatar_moeda($produto['PROVALOR']) ?></div>
											</div><!-- info -->
										</div><!-- itm -->

										<div class="itm-payment-form">
											<img src="img/itau.png" class="pic">

											<div class="info">
												<div class="ttl">Depósito em conta Itaú</div>
												<div class="price">à vista <?= formatar_moeda($produto['PROVALOR']) ?></div>
											</div><!-- info -->
										</div><!-- itm -->
									</div><!-- modal-payment-form -->
								</div><!-- payment-form -->


								<div class="calculate-freight">
									<div class="calculate-freight-ttl modal-details">Calcular frete</div>
								
									<div class="modal-calculate-freight">
										<div class="modal-header">
											<div class="ttl">Calcular frete</div>
											<button class="btn-close"></button>
										</div><!-- header -->

										<div class="cart-product-freight">
											<form id="formFrete" class="form-1">
												<fieldset class="full">
													<input type="text" id="cep" name="cep" class="cep" required autocomplete="off">
													<label class="form-1-placeholder" for="cep">Seu CEP</label>
												</fieldset>
												
												<button class="btn-calculate" type="submit">CALCULAR FRETE</button> <!-- btn-cart-product-cep -->
											</form>
											<img src="img/loader.svg" class="loader">
											<div class="result" style="display: none;">
												
												<div class="address"></div><!-- address -->

												<div class="value">
													<div class="itens"></div>
													<p class="note">Obs.: Previsão de entrega sujeita a alteração no fechamento do pedido em função da forma de pagamento.</p>
												</div><!-- value -->
											</div><!-- result -->
										</div><!-- cart-product-freight -->
										
									</div><!-- modal-calculate-freight -->
								</div><!-- calculate-freight -->
							</div><!-- payment-data -->

							<div class="buy">
								<button id="buyProduct" class="button-buy btn-buy-sm">COMPRAR</button><!-- btn -->
								<a class="btn-cashier btn-sm-green" id="buyProductFastMode" >CAIXA RÁPIDO</a>
							</div><!-- buy -->
						<?php endif; ?>
					</div><!-- actions -->

					<ul class="promotion">
						<?php
							foreach(promotionByProduct($produto['PROCODIGO']) as $promocao){
								$texto = ($promocao['PRMLIMITE'] > 1) ? 'unidades' : 'unidade';
								echo "<li>A partir de {$promocao[PRMLIMITE]} {$texto} deste produto, você ganhará ".formatar_moeda($promocao['PRMDESCONTO'], false)."% de desconto</li>";
							}
						?>
					</ul>

					<div class="actions">
						<div class="doubt">
							<div class="ttl">DÚVIDAS</div>
							<p>Tire dúvidas sobre esse produto</p>
						</div><!-- doubt -->

						<div class="indicate">
							<div class="ttl">INDIQUE</div>
							<p>Indique este produto para um amigo</p>
						</div><!-- indicate -->
					</div><!-- actions -->
				</div><!-- infos -->
			</article><!-- products-detail-itm -->
		</div><!-- ctn -->
	</main>	
	
	<section class="related-products">
		<div class="ctn">
			<h3 class="ttl-md-black-2">PRODUTOS RELACIONADOS</h3>

			<div class="related-products-carousel products-catalog">

			<?php include_once 'php/produtosRelacionados.php'; ?>

			</div><!-- related-products-carousel -->
		</div><!-- ctn -->
	</section><!-- related product -->

	<section class="product-info">
		<div class="ctn">
			<div class="ttl-md-black-2">INFORMAÇÕES DO PRODUTO</div>

			<article class="product-info-txt">
				<?= $produto['PRODESCRICAO'] ?>
			</article><!-- product-info-txt -->

			<div class="share">
				<div class="ttl">Compartilhe</div>
				<!-- Go to www.addthis.com/dashboard to customize your tools -->
				<div class="addthis-custom addthis_inline_share_toolbox_1109"></div>
			</div>
		</div><!-- ctn -->
	</section><!-- product-info -->

	<!-- modals -->
	<div class="modal-doubt">
		<div class="modal-inner">
			<button class="btn-close"></button>

			<div class="ttl-doubt ttl-lg-black-2">Tire suas dúvidas</div>

			<form name="send-doubt" class="form-1">
				<?php if(!is_numeric($_SESSION['CLICODIGO'])): ?>
					<div class="row">
						<fieldset class="full">
							<input type="text" name="vSCONNOME" id="vSCONNOME" required>
							<label class="form-1-placeholder" for="vSCONNOME">Seu nome</label>
						</fieldset>
					</div>
					<div class="row">
						<fieldset class="half">
							<input type="text" name="vSCONTELEFONE" id="vSCONTELEFONE" required class="telefone">
							<label class="form-1-placeholder" for="vSCONTELEFONE">Seu telefone</label>
						</fieldset>

						<fieldset class="half">
							<input type="email" name="vSCONEMAIL" id="vSCONEMAIL" required>
							<label class="form-1-placeholder" for="vSCONEMAIL">Seu E-mail</label>
						</fieldset>
					</div>
				<?php endif; ?>
				<fieldset class="full">
					<textarea name="vSCONMENSAGEM" id="vSCONMENSAGEM" required></textarea>
					<label class="form-1-placeholder" for="vSCONMENSAGEM">Escreva a sua mensagem</label>
				</fieldset>

				<div class="form-1-footer">
					<button type="submit" class="form-1-btn-submit btn-md-red">ENVIAR</button>
				</div>							
			</form>
		</div>
		<div class="modal-overlay"></div>
	</div><!-- modal-doubt -->

	<div class="modal-indicate">
		<div class="modal-inner">
			<button class="btn-close"></button>

			<div class="ttl-indicate ttl-lg-black-2">Indique para um amigo</div>

			<form name="indicacao" class="form-1">
				<?php if(!is_numeric($_SESSION['CLICODIGO'])): ?>
					<div class="row">
						<fieldset class="full">
							<input type="text" name="my-name" id="my-name" required>
							<label class="form-1-placeholder" for="my-name">Seu nome</label>
						</fieldset>
					</div>
				<?php endif; ?>
				<div class="row">
					<fieldset class="half">
						<input type="text" name="friend-name" id="friend-name" required>
						<label class="form-1-placeholder" for="friend-name">Nome do amigo</label>
					</fieldset>

					<fieldset class="half">
						<input type="email" name="friend-email" id="friend-email" required>
						<label class="form-1-placeholder" for="friend-email">E-mail do amigo</label>
					</fieldset>
				</div><!-- row -->

				<div class="row">	
					<fieldset class="full">
						<textarea name="message-indicate" id="message-indicate" required></textarea>
						<label class="form-1-placeholder" for="message-indicate">Escreva uma mensagem</label>
					</fieldset>
				</div>

				<div class="form-1-footer">
					<button type="submit" class="form-1-btn-submit btn-md-red">ENVIAR</button>
				</div>							
			</form>
		</div>
		<div class="modal-overlay"></div>
	</div><!-- modal-indicate -->
	<!-- modals -->
	<script type="text/javascript">
		var produto = <?= $produto['PROCODIGO']; ?>;
		var produtoNome = "<?= $produto['PRONOME']; ?>";
		var valorUnitario = "<?= $produto['PROVALOR']; ?>";
		var freteGratis = "<?= $produto['PROFRETEGRATIS'] ?>";
		var urlProduto = "<?= $_SERVER ['SCRIPT_URI'] ?>";
	</script>
<?php 
 if(!isset($_SESSION['CLICODIGO']) || !is_numeric($_SESSION['CLICODIGO'])){
		include("modal-login-lg.php");
 }
 ?>		
<?php include("footer.php"); ?>