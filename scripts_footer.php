<!-- libs -->
<script type="text/javascript" src="libs/jquery/jquery.min.js"></script>
<script type="text/javascript" src="libs/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="libs/owl.carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="admin/libs/paginacao/paginacao.min.js"></script>
<script type="text/javascript" src="admin/libs/jquery.mask/jquery.mask.min.js"></script>
<script type="text/javascript" src="admin/libs/jquery.validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="admin/libs/jquery.validate/jquery.validate.messages.js"></script>
<script type="text/javascript" src="admin/libs/jquery.validate/additional-methods.min.js"></script>
<script type="text/javascript" src="admin/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="admin/libs/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js"></script>
<!-- addthis -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56181e4836d34dc0"></script> 
<!-- addthis -->
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=<?= getConfig('CFGGOOGLEMAPSKEY'); ?>"></script> 
<!-- libs -->

<!-- scripts -->
<script>

// **************
// Meus-pedidos
// **************

<?php if($namePage == "meus-pedidos") {
    if(is_numeric($parametros[1])): ?>
        jQuery(document).ready(function($) {
            if($(".request-content.table-content[data-ref="+"<?= $parametros[1] ?>"+"]").length > 0){
                $('html, body').animate({
                    scrollTop: $(".request-content.table-content[data-ref="+"<?= $parametros[1] ?>"+"]").offset().top
                }, 1000);
                $(".request-content.table-content[data-ref="+"<?= $parametros[1] ?>"+"] .btn-dropdown").click();
            }
        });
    <?php endif;
 } ?>


//Btns para Show/Hide request detail quando click

//Btn-arrow
$(".btn-dropdown").click(function(){
    //icone angle down transforma em angle up
    $(this).toggleClass("isActive");

    //Mostra e recolhe request-detail
    $(this)
        .parent()
            .parent()
                .parent()
                        .children(".request-detail")
                            .slideToggle();
});

//Btn-close-detail
$(".btn-close-detail").click(function(){
    //Icone de angle up volta a ser angle down
    $(this)
        .parent()
        .parent()
            .children()
                .children()
                    .children(".btn-dropdown")
                        .removeClass("isActive");

    //Recolhe request-detail                        
    $(this)
        .parent()
            .slideUp();
});


//*************************
// Checkout-carrinho
//*************************

//Botões de aumentar e diminuir quantidade
$(function() {
    $(".amount-box input").after('<div data-amount="inc"></div>');
    $(".amount-box input").before('<div data-amount="dec"></div>');

    $("[data-amount]").on("click", function() {

        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.attr("data-amount") == "inc") {
            var maximum = ($button.parent().find('input').attr("max") != undefined) ? $button.parent().find('input').attr("max") : null;
            var newVal = parseFloat(oldValue) + 1;
            if(maximum != null){
                //Don't allow to increase above the value attributed to the max attribute
                if(newVal > maximum){
                    newVal = maximum;
                }
            }
        } else {
            var minimal = ($button.parent().find('input').attr("min") != undefined) ? $button.parent().find('input').attr("min") : 0;
            var newVal = parseFloat(oldValue) - 1;
            // Don't allow decrementing below zero
            if (newVal < minimal) {
                newVal = minimal;
            }
        }

        $button.parent().find("input").val(newVal);

    });

});



// ********************
// Produtos-detalhe
// ********************

//Show Hide Modal Doubt
$(".actions .doubt, .modal-doubt .btn-close, .modal-doubt .modal-overlay").click(function(){
    $(".modal-doubt").fadeToggle();
});

//Show Hide Modal Indicate
$(".actions .indicate, .modal-indicate .btn-close, .modal-indicate .modal-overlay").click(function(){
    $(".modal-indicate").fadeToggle();
});


//Show Hide Modal Payment Form
$(".payment-form-ttl, .modal-payment-form .btn-close").click(function(){
    $(".modal-payment-form").fadeToggle();
    $(".modal-calculate-freight").fadeOut();
    $(".payment-form-ttl").toggleClass("isActive");
    $(".calculate-freight-ttl").removeClass("isActive");

});

// Show Hide Modal Calculate Freight
$(".calculate-freight-ttl, .modal-calculate-freight .btn-close").click(function(){
    $(".modal-calculate-freight").fadeToggle();
    $(".modal-payment-form").fadeOut();
    $(".calculate-freight-ttl").toggleClass("isActive");
    $(".payment-form-ttl").removeClass("isActive");

});

// products dtl carousel
$(document).ready(function(){
    $(".products-dtl-carousel").owlCarousel({
        items: 4,
        margin: 10,
        // stagePadding: 10,
        
        navigation: true,
        navigationText: [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ]
    });
});

//Tab products-dtl-itm
$(document).ready(function(){
    $('.products-dtl-itm .itm').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('.products-dtl-itm .itm').removeClass('isActive');
        $('.products-dtl-itm .pic-main')
            .removeClass('isActive')
            .fadeOut(20);

        $(this).addClass('isActive');
        $("#"+tab_id)
            .fadeIn()
            .addClass('isActive');
    })
});

// related products carousel
$(document).ready(function(){
    $(".related-products-carousel").owlCarousel({
        items: 4,
        // margin: 10,
        // stagePadding: 10,
        
        navigation: true,
        navigationText: [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ]
    });
});



// **********************
// Produtos-marca
// **********************

//brand-logo
var brandLogoHeight = $('.brand-logo').width();
$('.brand-logo').css({'height':brandLogoHeight+'px'});



// ************************
// Home
// ************************

// slider products
$(document).ready(function(){
    $(".slider-products .itm").addClass("isActive");

    $(".slider-products").owlCarousel({
        navigation: false,

        pagination: false,

        singleItem: true,

        slideSpeed : 300,
        paginationSpeed : 400,
        autoPlay: true
    });
});




//Show Hide modal login LG
// $(".modal-login-lg .btn-close, .modal-login-lg .modal-overlay").click(function(){
//     $(".modal-login-lg").fadeToggle();
// });



// *******************
// Layout
// *******************

//Show Hide main nav
$(".btn-menu").click(function(){
    $(".main-nav").fadeToggle();
    $(".btn-menu").toggleClass("active");
});

//Tab main nav
$(document).ready(function(){
    $('.nav-tab li').not(':first-child').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('.nav-tab li').removeClass('current');
        $('.nav-tab-content')
            .removeClass('current')
            .fadeOut(20);

        $(this).addClass('current');
        $("#"+tab_id)
            .fadeIn()
            .addClass('current');
    })
});

//scroll lst-product header
(function($){
    $(window).on("load",function(){
        $(".modal-shopping-cart .product-lst").mCustomScrollbar({
        	theme:"dark"
        });
    });
})(jQuery);


$(".user-area .btn-login[type!=submit], .user-area .modal-login .btn-close").click(function(){
	$(".modal-login").fadeToggle();
	$(".modal-shopping-cart").fadeOut();

});

$(".user-area .btn-shopping-cart, .user-area .modal-shopping-cart .btn-close").click(function(){
	$(".modal-shopping-cart").fadeToggle();
	$(".modal-login").fadeOut();
});



// ************************
// # Modulos e Componentes
// ************************

// attachment-file
$('.file-attachment input[type=file]').change(function(e){

  $in=$(this);
  alert($in.val().replace(/C:\\fakepath\\/i, ''));	
  $in
    .parent()
    .children(".status-file")
    .html($in.val().replace(/C:\\fakepath\\/i, ''));

});

//scroll attachment-lst
(function($){
    $(window).on("load",function(){
        $(".attachment-lst").mCustomScrollbar({
            theme:"dark"
        });
    });
})(jQuery);


// Complemento no comportamento do input[file] estilizado
$('input[type=file]').change(function(e){

  $in=$(this);

  $in
    .parent()
    .children(".status-upload")
    .html($in.val().replace(/C:\\fakepath\\/i, ''));

});


// Incrementa numeração no for da label e no id do input de upload ao carregar pagina
$(".anexar .label-upload")
    .each(function(i) {
        $(this).attr('for', "input-upload-" + (i + 1));
    });

$(".anexar .input-upload")
    .each(function(i) {
        $(this).attr('id', "input-upload-" + (i + 1));
		$(".anexar .hidden-upload").attr('id', "hdn_oid_produto-" + (i + 1));
    });

$(".anexar .status-upload")
    .each(function(i) {
        $(this).attr('id', "status-upload-" + (i + 1));
    });

function countFiles(){ 
    var i = 0;
    $("input[type=file]").each(function(index, element) {
        i++;
    });
    $("#num_files").val(i);
}

// Add input div anexo com input file
$(".anexar-mais-arquivos").click(function(){
    var produto = $(this).siblings('.anexar').find(".anexo [name='vIPROCODIGO[]']").val();

    $(this).siblings(".anexar")
        .append("<div class='anexo'><label class='label-upload'>Adicionar um arquivo</label> <input type='file' class='input-upload' name=\"vFPXAANEXO[]\" id=\"vFPXAANEXO[]\"/><div class='status-upload'><b>Escolha um arquivo</b></div><div class='remover'></div></div><input type='hidden' class='hidden-upload' value='"+produto+"' name=\"vIPROCODIGO[]\" id=\"vIPROCODIGO[]\">");

    // Incrementa numeração no for da label e no id do input file da div anexo ao clicar em adicionar
    $(".anexar .label-upload")
        .each(function(i) {
            $(this).attr('for', "input-upload-" + (i + 1));
        })

    $(".anexar .input-upload")
        .each(function(i) {
            $(this).attr('id', "input-upload-" + (i + 1));
        });		
		
    $(".anexar .status-upload")
        .each(function(i) {
            $(this).attr('id', "status-upload-" + (i + 1));
        });
    $('.anexo')
        .on('click', '.remover', function(e) {
            $(this)
               .parent()
               .remove();
            countFiles();
        });

    $(function (){
        countFiles();
    });

    // Complemento no comportamento do input[file] estilizado
    $('input[type=file]').change(function(e){

      $in=$(this);

      $in
        .parent()
        .children(".status-upload")
        .html($in.val().replace(/C:\\fakepath\\/i, ''));

    });

});

// Modal attachments
$(".btn-attachment").click(function(){
    var idNumero = $(this).attr("id");
    $("#modal-attachment-" + idNumero).fadeIn();
});

$(".modal-attachment .btn-close, .modal-attachment .btn-concluido, .modal-attachment .modal-overlay").click(function(){
    $(".modal-attachment").fadeOut();
});

//***********************
// Tabs
//***********************
$(document).ready(function(){
    
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    });

});


// ***************
// Forms Global
// ***************

//Validação
$("form").each(function(){
    if($(this).attr('name') == 'formContato')
        $('form[name=formContato]').validate({
            ignore: '',
            rules: {
                vSCONNOME: { required: true },
                vSCONEMAIL: { required: true, email: true},
                vSCONASSUNTO: { required: true },
                vSCONTELEFONE: { required: true },
                vSCONMENSAGEM: { required: true },
                hiddenRecaptcha: {
                    required: function () {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            errorPlacement: function(error, element) {
                if(element.prop('name') == 'hiddenRecaptcha'){
                   element.before('<label class="error" for="hiddenRecaptcha">É necessário confirmar que você não é um robô</span>');
                }else{
                    element.before(error);
                }
            },
            submitHandler: function( form ){
                form.submit();
            }
        });
    else
        $(this).validate();
});

//Inserção de asterísco quando algum campo é required
$(':input[required]:visible').not('.notAsterisk').before("<div class='required'></div>");



// *********
// Form 1
// *********

//Add to js default
jQuery(document).ready(function($) {
    $(".cpfCnpj").keydown(function(){
        try {
            $(this).unmask();
        } catch (e) {}
        
        var tamanho = $(this).val().length;

        if(tamanho < 11){
            $(this).mask("999.999.999-99");
        } else {
            $(this).mask("99.999.999/9999-99");
        }                   
    });
    $(".cpfCnpj").blur(function(){
        try {
            $(this).unmask();
        } catch (e) {}
        
        var tamanho = $(this).val().length;
        
        if(tamanho <= 11){
            $(this).mask("999.999.999-99");
        } else {
            $(this).mask("99.999.999/9999-99");
        }                   
    });    
});

//Ativa o placeholder quando o field está preenchido
$(".form-1 input, .form-1 textarea, .form-1 select").blur(function(){
    if($(this).val() != null){
        if($(this).val().length > 0) {
            $(this).addClass("isActive");
        } else {
            $(this).removeClass("isActive");
        }
    }else{
        $(this).removeClass("isActive");    
    }
});

$( document ).ready(function() {
    $(".form-1 input, .form-1 textarea, .form-1 select").each(function(){
        if($(this).val() != null){
            if($(this).val().length > 0) {
                 $(this).addClass("isActive");
            } else {
                 $(this).removeClass("isActive");
            }
        }else{
            $(this).removeClass("isActive");    
        }
    });
});
// Verifica o campo e-mail para envio de newsletter
$( document ).ready(function() {
    $("form[name=formNewsletter]").on('submit', function(event){
        event.preventDefault();
        var email = document.getElementById('vSNEWEMAIL').value;
        if (email != '') {
            var validado = validarEmail(email);
            if (validado) {                
                $.ajax({
                    url: 'php/enviarNewsletter.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        vSNEWEMAIL: email
                    },
                    success: function(result){
                        var typeAlert = (result.statusInsercao) ? 'success' : 'warning';
                        sweetAlert("", result.mensagem,typeAlert);                        
                        $("#vSNEWEMAIL").val('');
                    },
                    error: function(){
                        sweetAlert("Oops...", "Ocorreu um erro inesperado!", "error");
                    }
                });
            }
            
        }

    })
});

function validarEmail(pEmail){
    if (pEmail.indexOf("@",0) == -1) {
        return false;
    }
    if (pEmail.indexOf(".",0) == -1) {
        return false;
    }
    return true;
}
function removeToCart(token, button){
    swal({
        title: "",
        text: "Você deseja remover o produto selecionado do carrinho?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false
    },
    function(){
        if(token != ''){
            $.ajax({
                url: 'php/removerDoCarrinho.php',
                type: 'GET',
                dataType: 'json',
                data: {
                    token: token
                },
                success: function(result){
                    if(result[0]){
                        sweetAlert("Produto removido do carrinho", "", "success");
                        $(button).parent().remove();
                        updateValuesOfCart(result[1]);
                    }else   
                        sweetAlert("Oops...", "Houve um erro ao remover o produto do carrinho", "error");
                },
                error: function(){
                    sweetAlert("Oops...", "Ocorreu um erro inesperado", "error");
                }
            });
        }
    });
}

function updateValuesOfCart(total){
    var countItems = $(".modal-shopping-cart #mCSB_1_container .product-itm").length;
    var description = (countItems > 1) ? ' itens adicionados' : 'item adicionado';
    if(countItems > 0){
        $(".modal-shopping-cart .resume .total-price").text('Total: '+total);
        $(".modal-shopping-cart .resume .itms-added").text(countItems + description);
        $(".modal-shopping-cart .resume").show();
    }
    else{
        $(".modal-shopping-cart .resume").hide();
        $(".modal-shopping-cart #mCSB_1_container").html("<div class=\"cart-empty\">Ainda não há produtos no carrinho! <br>\nClique em <span class=\"btn-buy btn-buy-sm\">Comprar</span> para adicionar produtos aqui.</div>\n");
    }
}
</script>

<?php 
    if(is_file('js/'.$namePage.'.js')) echo "<script type=\"text/javascript\" src=\"js/{$namePage}.js\"></script>";
?>
<!-- scripts -->
