
	<footer class="main-footer">
		<div class="main-footer-top">
			<div class="ctn">
				<nav class="nav-footer">
					<div class="ttl-md-white">A loja</div>

					<ul>
					    <li>
					    	<a href="/contato/">Contato</a>
					    </li>
					<?php
						require_once 'admin/transaction/transactionPaginasDinamicas.php';
						
						foreach(exibirPaginasMenu() as $pagina){
					?>  
					    <li>
					    	<a href="/pagina/<?= gerarUrlAmigavel($pagina['PAGTITULO']); ?>/<?= $pagina['PAGCODIGO']; ?>/"><?= $pagina['PAGTITULO']; ?></a>
					    </li>

					<?php
						}
					?>
					</ul>
				</nav><!-- nav footer -->

				<div class="service-customer">
					<div class="ttl-md-white">Atendimento</div>

					<p>
						Horário: 8:00 às 18:00 - Segunda à Sábado <br>
						(51) 3222-2339 | atendimento@algomais.art.br <br>
						Rua Santos Dumont, 1101 - Bairro São Geraldo - Porto Alegre/RS
					</p>	
				</div><!-- service customer -->

				<div class="newsletter">
					<div class="ttl-md-white">Receba nossas novidades</div>
					<form  class="form-1" name="formNewsletter" method="post">
						<fieldset>
							<input type="email" id="vSNEWEMAIL" name="vSNEWEMAIL" placeholder="Preencha seu e-mail" required class="notAsterisk">
							<button type="submit" id="btnNewsletter" class="btn-submit">CADASTRAR</button>
						</fieldset>
					</form>
				</div><!-- newsletter -->
			</div><!-- ctn -->
		</div><!-- main footer top -->
		<div class="main-footer-bottom">
			<div class="ctn">
				<div class="copyright">© 2016 Algo Mais - Todos os direitos reservados.</div>
				<ul class="signature">
					<li>
						<a href="http://www.massacriativa.com.br" target="_blank" title="Massa Criativa">
							<img src="img/massa-criativa.png" alt="Massa Criativa">
						</a>
					</li>
					<li>
						<a href="http://portal.teraware.com.br/" target="_blank" title="Teraware">
							<img src="img/teraware.png" alt="Teraware">
						</a>
					</li>
				</ul>
			</div>
		</div><!-- main footer bottom -->
	</footer><!-- main footer -->

	<?php include("scripts_footer.php"); ?>
</body>
</html>