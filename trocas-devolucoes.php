<?php 
$namePage = "trocas-devolucoes";
include("header.php"); ?>

	<main>
		<div class="ctn">


		<section class="institucional">
			<h1 class="ttl-md-black">Trocas e devoluções</h1>

			<div class="content">
				<p>

					<br>

					<b>Devolução do Produto</b> 
					<br> 

						Os itens disponíveis no site são produzidos conforme demanda, desta forma a devolução somente ocorrerá em caso de defeito.

					<br>
					<br>

						Se o produto apresentar defeito comunique nosso setor de atendimento ao cliente e solicite a troca, através do e-mail: atendimento@algomais.art.br

					<br>
					<br>

						Todas as instruções serão fornecidas pelo setor de atendimento ao cliente.
				</p>
			
			</div><!-- content -->
			
		</section><!-- institucional -->

		</div><!-- ctn -->
	</main>
<?php include("footer.php"); ?>