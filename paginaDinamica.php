<?php 
	$namePage = "politica-privacidade";
	include("header.php");
	require_once 'admin/transaction/transactionPaginasDinamicas.php';
	$fill = fillPaginasDinamicas($parametros[2]);
?>

	<main>
		<div class="ctn">


		<section class="institucional">
			<h1 class="ttl-md-black"><?= $fill['PAGTITULO']; ?></h1>

			<div class="content">
				<?= $fill['PAGTEXTO']; ?>		
			</div><!-- content -->
			
		</section><!-- institucional -->


		</div><!-- ctn -->
	</main>
<?php include("footer.php"); ?>