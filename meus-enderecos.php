<?php 
$namePage = "meus-enderecos";
include("header.php"); ?>

	<main>
		<div class="ctn">
			<h1 class="ttl-md-black">Checkout</h1>
			
			<div class="panel">
				<!-- panel nav -->
				<?php include("panel-nav.php"); ?>
				<!-- panel nav -->

				<div class="panel-data-list">
					<table class="table-address table">
						<thead class="header table-header">
							<tr class="lst-address">
								<th class="itm-address"><div class="ttl ttl-sm-black-2">ENDEREÇO</div></th>
								<th class="itm-neighborhood"><div class="ttl ttl-sm-black-2">BAIRRO</div></th>
								<th class="itm-city"><div class="ttl ttl-sm-black-2">CIDADE</div></th>
								<th class="itm-zip-code"><div class="ttl ttl-sm-black-2">CEP</div></th>
								<th class="itm-actions"><div class="ttl ttl sm-black-2"></div></th>
							</tr>
						</thead>
						<tbody class="content table-content">
							<tr class="lst-address">
								<td class="itm-address">
									Avenida Farrapos, 146 / 101
									<div class="address-main"><b>Endereço principal</b></div>
								</td>
								<td class="itm-neighborhood">Floresta</td>
								<td class="itm-city">Porto Alegre / RS</td>
								<td class="itm-zip-code">90220000</td>
								<td class="itm-actions">
									<a href="meus-enderecos-editar.php" class="edit">Editar</a> 
									<a href="#" class="remove">Excluir</a>
								</td>
							</tr>
						</tbody><!-- content table-content -->
					</table><!-- table-address table -->

					<button class="btn-md-red">ADICIONAR NOVO</button>
					
				</div><!-- panel-data-list -->
			</div><!-- panel -->
			
		</div><!-- ctn -->
	</main>
<?php include("footer.php"); ?>