<?php 
	$namePage = "contato";
	include("header.php"); ?>

	<main>
		<div class="ctn">		
			<h1 class="ttl-md-black">Contato</h1>
			<form class="send-contact form-1" name="formContato" action="/contato/enviar/" method="post">
				<div class="row">
					<fieldset class="half">
						<input type="text" id="vSCONNOME" name="vSCONNOME" required>
						<label class="form-1-placeholder" for="vSCONNOME">Nome</label>
					</fieldset>

					<fieldset class="half">
						<input type="email" id="vSCONEMAIL" name="vSCONEMAIL" required>
						<label class="form-1-placeholder" for="vSCONEMAIL" >E-mail</label>
					</fieldset>
				</div>
				<div class="row">
					<fieldset class="half">
						<input type="text" class="telefone" id="vSCONTELEFONE" name="vSCONTELEFONE" required>
						<label class="form-1-placeholder" for="vSCONTELEFONE">Seu telefone</label>
					</fieldset>

					<fieldset class="half">
						<input type="text" id="vSCONASSUNTO" name="vSCONASSUNTO" required>					
						<label class="form-1-placeholder" for="vSCONASSUNTO">Escolha um assunto</label>
					</fieldset>
				</div>
				<div class="row">
					<fieldset class="full">
						<textarea id="vSCONMENSAGEM" name="vSCONMENSAGEM" required></textarea>
						<label class="form-1-placeholder" for="vSCONMENSAGEM">Prencha sua mensagem</label>
					</fieldset>
				</div>
				<div class="row">
					<div class="half">
						<p class="required-message">Todos os campos são de preenchimento obrigatório.</p>
					</div><!-- half -->	
					<div class="send half">
						<fieldset class="recaptcha">
	                        <div class="g-recaptcha" data-sitekey="<?= getConfig('CFGRECAPTCHASITEKEY'); ?>" data-callback="gCallbackSuccess" data-expired-callback="gCallbackExpired"></div>
	                        <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha">
	                    </fieldset><!-- recaptcha -->	
						<button type="submit" class="btn-send btn-md-red">ENVIAR</button>
					</div><!-- send half -->		
				</div><!-- row-->	
			</form>		
		</div>
		<section class="contact-informations">
			<div class="row">
				<div class="detail half">	
					<h2 class="ttl ttl-md-white">Informações</h2>						
					<ul>
						<li>Rua Santos Dumont, 1101</li>
						<li>Bairro Floresta - Porto Alegre | RS</li>
						<li>(51) 3222.2339</li>
						<li><a href="mailto: atendimento@algomais.art.br">atendimento@algomais.art.br</a></li>
					</ul>
				</div>
				<div class="map half">			
					<div class="extra-map" id="googleMap" style="width: 100%; height: 334px;">
						
					</div>
				</div>
			</div>
		</section>
	</main>
	<?php include("footer.php"); ?>